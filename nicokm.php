<?php
    require('videoinfo_func.php');
    require('navbar/nicovideo_navbar.php');
?>
<?php
  $id = isset($_GET['nicoID']) ? $_GET['nicoID'] : null; //GETでIDを受け取る
  $before_after = isset($_GET['km']) ? $_GET['km'] : null; //GETでIDを受け取る
  $id = intval($id); //小数点などを整数で返す
  $video_id = video_IDsearch($id);
  $new_id = video_newone();
  $search_video = $video_id . $id;

  if($before_after == 0 || $before_after == null){
    $title = " - ニコニコ動画検索info 過去動画検索";
  }else{
    $title = " - ニコニコ動画検索info 未来動画検索";
  }

  if($video_id === 1){
    $title = "動画ID(".$id.")".$title;
  }else{
    $base_id = 'https://ext.nicovideo.jp/api/getthumbinfo/' . $search_video; //ニコニコAPI(動画情報取得)
    $xml = simplexml_load_file($base_id); //ニコニコAPIのxml形式のデータを読み込む(動画情報取得)
    $title = $xml->thumb->title.$title;
  }
  $meta_url = "https://nicosearch.info/nicokm.php?nicoID=".$id."&km=".$before_after;

?>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
      <?php echo "<title>".$title."</title>"; ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <?php echo '<meta property="og:url" content="'.$meta_url.'" />'; ?>
        <meta property="og:type" content="website" />
        <?php echo '<meta property="og:title" content="'.$title.'" />'; ?>
        <meta property="og:description" content="ニコニコ動画に投稿された動画のコメント,タグ,投稿者ランキング,新着動画,フォロー・フォロワー等の詳細な検索ができるサイトです。" />
        <meta property="og:site_name" content="ニコニコ動画検索info" />
        <meta property="og:image" content="https://nicosearch.info/image/site-title.png" />

        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content="@dhiki_pico" />
        <?php echo '<meta name="twitter:title" content="'.$title.'" />'; ?>
        <?php echo '<meta name="twitter:url" content="'.$meta_url.'" />'; ?>
        <meta name="twitter:description" content="ニコニコ動画に投稿された動画のコメント,タグ,投稿者ランキング,新着動画,フォロー・フォロワー等の詳細な検索ができるサイトです。" />
        <meta name="twitter:image" content="https://nicosearch.info/image/site-title.png" />

          <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
          <link href="./css/style.css" rel="stylesheet" media="screen">
            <style type="text/css">
            @font-face {
                font-family: 'GenJyuuGothic';
                src: url('fonts/genjyuugothic-20150607/GenJyuuGothic-Medium.ttf');
            }
            body{
                font-family: 'GenJyuuGothic',sans-serif;
                /* table-layout: fixed; */
                /* text-align: center; */
            }
            p{
              font-size: 20px;
            }
            img {
              zoom: 150%;
            }
            button {
              -webkit-tap-highlight-color:rgba(0,0,0,0);
            }
            </style>
            <script type="application/ld+json">
            {
              "@context" : "http://schema.org",
              "@type" : "WebPage",
              "name" : "ニコニコ動画検索info",
            "url" : "https://nicosearch.info/",
            "sameAs" : [
              "https://twitter.com/dhiki_pico",
              "https://www.instagram.com/nicosearchinfo/?hl=ja"
              ]
            }
            </script>
            <script>
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

              ga('create', 'UA-85902327-1', 'auto');
              ga('send', 'pageview');

            </script>
              <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
              <script src="js/bootstrap.min.js"></script>


              <script src="js/include.js"></script>
  </head>
  <body>

<a name="up"></a>


    <?php $select = 6; navbar($select); ?>

    <ul itemscope itemtype="https://schema.org/BreadcrumbList" class="breadcrumbs">
      <!-- 1つめ -->
      <li itemprop="itemListElement" itemscope
          itemtype="https://schema.org/ListItem" class="breadcrumb_1">
        <a itemprop="item" href="https://nicosearch.info/">
            <span itemprop="name">ニコニコ動画検索info トップ</span>
        </a>
        <meta itemprop="position" content="1" />
      </li>

      <!-- 2つめ -->
      <li itemprop="itemListElement" itemscope
          itemtype="https://schema.org/ListItem" class="breadcrumb_2">
        <a itemprop="item" href="https://nicosearch.info/nicokmsearch.php">
            <span itemprop="name">過去・未来動画検索</span>
        </a>
        <meta itemprop="position" content="2" />
      </li>

      <!-- 3つめ -->
      <li itemprop="itemListElement" itemscope
          itemtype="https://schema.org/ListItem" class="breadcrumb_3">
        <?php echo "<a itemprop='item' href='https://nicosearch.info/nicokm.php?nicoID=".$id."&km=".$before_after."'>"; ?>
        <?php 
          if($before_after == 0 || $before_after == null){
            $breadcrumb = "]からうpされた過去動画10件</span>";
          }else{
            $breadcrumb = "]からうpされた未来動画10件</span>";
          }

          if($video_id === 1){
            $breadcrumb = "<span itemprop='name'>[動画ID(".$id.")".$breadcrumb;
          }else{
            $breadcrumb = "<span itemprop='name'>[".$xml->thumb->title.$breadcrumb;
          }
          echo $breadcrumb;
        ?>
        </a>
        <meta itemprop="position" content="3" />
      </li>
    </ul>

<?php

      //動画IDが最新の動画よりも大きすぎる場合
        if($new_id-100 < $id){
          $new_id = $new_id - 100;
          echo "<div class='container' style='padding:20px'>";
          echo "<p>入力値 ".$id."</p>";
          echo "<p>".$new_id."までの動画IDを入力してください</p>";
          echo "</div>";
            die();
        }
      //動画IDがマイナスの値だった場合
        if($id < 0){
          echo "<div class='container' style='padding:20px'>";
          echo "<p>入力値 ".$id."</p>";
          echo "<p>マイナスの値は入力できません</p>";
          echo "</div>";
            die();
        }
        //最低でも動画IDが50以上(10番目に古い動画がsm50だから)
          if($id < 51){
            echo "<div class='container' style='padding:20px'>";
            echo "<p>入力値 ".$id."</p>";
            echo "<p>最低でも51以上の値を入力してください</p>";
            echo "</div>";
              die();
          }
    /*
      $all = 'http://flapi.nicovideo.jp/api/getheadline';
      $xml = simplexml_load_file($all);
      $video_total = str_replace(',','',$xml->total_video);
      $video_total = $video_total * 5; //トータルの動画総数からだいたい5倍の総数は見積もっておく
    */
?>

<div class="container" style="padding:20px">
  <center><a href=./nicokmsearch.php><button type='button' class='btn btn-default' data-toggle='popover'>入力ページに戻る</button></a>

<?php
if($video_id == 1){
  if($before_after != 1){
  echo "<center><p><style=font-size: 150%;>動画ID(".$id.")から10件前までの動画を検索します</style></p></center>";
}else{
  echo "<center><p><style=font-size: 150%;>動画ID(".$id.")から10件後までの動画を検索します</style></p></center>";
}
    //echo "<p>".$xml->response_headline->total_video."</p>";
}
if($video_id != 1){
  if($before_after != 1){
  echo "<center><p><style=font-size: 150%;>10件前までの動画を検索します</style></p></center>";
}else{
  echo "<center><p><style=font-size: 150%;>10件後までの動画を検索します</style></p></center>";
}
}
?>
<form class="form-inline" action="nicokm.php" method="get">
  <div class="input-group">
    <div class="input-group-addon">sm,nm,soに対応</div>
    <input type="number" class="form-control" name="nicoID" maxlength='12' placeholder="niconicoID">
  </div>
  <button type="submit" class="btn btn-primary">検索</button>
</form>

<div class="container" style="padding:20px">

  <script type="text/javascript" >
     include("navbar/videoinfo.html");
  </script>


<?php

if($video_id != 1){

  echo "<center><p><b>基準とする動画</b></p></center>";

  video_data($id,$video_id);
      echo "<div class='col-sm-12 col-xs-12' style='background:black;'></div>";
      echo "<div class='col-sm-12 col-xs-12' style='background:black;'></div>";
      echo "<div class='col-sm-12 col-xs-12' style='background:black;'></div>";
}
 ?>
<div></div>
<?php

echo "<div id='loading'  class='col-sm-12 col-xs-12' style='width:100%;margin:0;padding:0;height:100%;'>";
echo "<font size='6'>検索中...</font><center><img src='image/loading.gif' class='img-responsive' alt='Responsive image'></center>";
$video_count = 0;

   while($video_count < 10){
     ob_flush();       // PHPの出力バッファを強制出力します。
     flush();          // Webサーバの出力バッファを強制出力します。
     //echo ".";

     if($before_after != 1){
        $id = $id - 1;
        $now_video[$video_count] = $id;
      }
     else{
        $id = $id + 1;
        $now_video[$video_count] = $id;
      }

      $now_id[$video_count] = video_IDsearch($now_video[$video_count]);  //動画の種類(sm,so,nmを判断)

          if($now_id[$video_count] != 1)
            $video_count++;
    }
  echo "</div>";
 ?>
 <style>
#loading {
  display:none;
}
</style>
</div>
<?php
$video_count = 0;
while($video_count < 10){
video_data($now_video[$video_count],$now_id[$video_count]);
$video_count++;
}

$id_save = $id;
$sort = 100;
    echo "<center>";
    echo "<div class='col-sm-12 col-xs-12'><p>過去の動画を検索</p></div>";
    echo "<ul class='pagination btn-group-sm'>";
    for($i = 9; $i >= 1; $i--){
      $id = $id_save - 10 * $i;
      $sort = $sort - 10;
      echo "<li><a href=./nicokm.php?nicoID=".$id."&km=0>-".$sort."</a></li>";
    }
    echo "<li><a href=./nicokm.php?nicoID=".$id_save."&km=0>".$now_id[9].$id_save."から</li>";
  $sort = 0;
      for($i = 1; $i <= 9; $i++){
        $id = $id_save + 10 * $i;
        $sort = $sort + 10;
        echo "<li><a href=./nicokm.php?nicoID=".$id."&km=0>+".$sort."</a></li>";
      }
    echo "</ul>";
    echo "</center>";


    $sort = 100;
        echo "<center>";
        echo "<div class='col-sm-12 col-xs-12'><p>未来の動画を検索</p></div>";
        echo "<ul class='pagination btn-group-sm'>";
        for($i = 9; $i >= 1; $i--){
          $id = $id_save - 10 * $i;
          $sort = $sort - 10;
          echo "<li><a href=./nicokm.php?nicoID=".$id."&km=1>-".$sort."</a></li>";
        }
        echo "<li><a href=./nicokm.php?nicoID=".$id_save."&km=1>".$now_id[9].$id_save."から</li>";
    $sort = 0;
          for($i = 1; $i <= 9; $i++){
            $id = $id_save + 10 * $i;
            $sort = $sort + 10;
            echo "<li><a href=./nicokm.php?nicoID=".$id."&km=1>+".$sort."</a></li>";
          }
        echo "</ul>";
        echo "</center>";
?>
<center><a href="#up" class="btn btn-primary btn-lg " role="button">一番上へ</a></center>
  </div>
    </div>
<a name="down"></a>
</center>
  </body>
</html>
