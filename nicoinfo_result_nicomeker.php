<!DOCTYPE html>
<html>
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
<meta name='viewport'
content='width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no'>
<meta name='format-detection' content='telephone=no'>
<link media='only screen and (max-device-width:480px)'
href='smart.css' type='text/css' rel='stylesheet' />
<link media='screen and (min-device-width:481px)' href='design.css'
type='text/css' rel='stylesheet' />
<!--[if IE]>
<link href='design.css' type='text/css' rel='stylesheet' />
<![endif]-->
<link rel='stylesheet' href='css/top.css' type='text/css'>
 <head>
  <title>ニコニコ動画　動画詳細検索</title>
  <style type="text/css">
  div{
  border-bottom: medium black solid;
  }

  p{
  font-size: 50px;
}

.popup_mylist {
  padding: 0 3px;
  color: #66f;
  font-weight: bold;
  cursor: pointer;
}
.popup_adv {
  padding: 0 3px;
  color: #66f;
  font-weight: bold;
  cursor: pointer;
}
.popup_advok {
  padding: 0 3px;
  color: #66f;
  font-weight: bold;
  cursor: pointer;
}

.popup_tweet{
  padding: 0 3px;
  color: #66f;
  font-weight: bold;
  cursor: pointer;
}

.popup_total{
  padding: 0 3px;
  color: #66f;
  font-weight: bold;
  cursor: pointer;
}

  </style>
<script src = "http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script>
$(function() {
// 参考URL:https://remotestance.com/blog/1522/
//popup_mylist
  $(".popup_mylist").mouseover(function() {
    // 「?」にマウスが重なった場合の処理です。

    // ヘルプウィンドウのトップマージンを定義します。
    // 0だと「?」と上端が揃います。
    var marginTop = 0;

    // ヘルプウィンドウのレフトマージンを定義します。
    // 0だと「?」と左端が揃います。
    var marginLeft = 20;

    // ヘルプウィンドウ表示させる際のスピードを定義します。
    var speed = 300;

    // ヘルプウィンドウのオブジェクトを取得します。
    var popupObj_mylist = $(".popup_mylist");

    if (!popupObj_mylist.length) {
      // ウィンドウがなければ作成します。
      popupObj_mylist = $("<p/>").addClass("popup_mylist").appendTo($("body"));
    }

    // ウィンドウにメッセージを設定します。
    popupObj_mylist.text($(this).attr("data-message"));

    // ウィンドウのオフセットを計算します。
    var offsetTop = $(this).offset().top + marginTop;
    var offsetLeft = $(this).offset().left + marginLeft;

    // ウィンドウの位置を整え、表示します。
    popupObj_mylist.css({
      "top": offsetTop,
      "left": offsetLeft
    }).show(speed);

  }).mouseout(function() {
    // 「?」からマウスが離れた場合、テキストを空にしてウィンドウを隠します。
    $(".popup_mylist").text("?");
  });


//popup_adv
  $(".popup_adv").mouseover(function() {
    // 「?」にマウスが重なった場合の処理です。

    // ヘルプウィンドウのトップマージンを定義します。
    // 0だと「?」と上端が揃います。
    var marginTop = 0;

    // ヘルプウィンドウのレフトマージンを定義します。
    // 0だと「?」と左端が揃います。
    var marginLeft = 20;

    // ヘルプウィンドウ表示させる際のスピードを定義します。
    var speed = 300;

    // ヘルプウィンドウのオブジェクトを取得します。
    var popupObj_adv = $(".popup_adv");

    if (!popupObj_adv.length) {
      // ウィンドウがなければ作成します。
      popupObj_adv = $("<p/>").addClass("popup_adv").appendTo($("body"));
    }

    // ウィンドウにメッセージを設定します。
    popupObj_adv.text($(this).attr("data-message"));

    // ウィンドウのオフセットを計算します。
    var offsetTop = $(this).offset().top + marginTop;
    var offsetLeft = $(this).offset().left + marginLeft;

    // ウィンドウの位置を整え、表示します。
    popupObj_adv.css({
      "top": offsetTop,
      "left": offsetLeft
    }).show(speed);

  }).mouseout(function() {
    // 「?」からマウスが離れた場合、テキストを空にしてウィンドウを隠します。
    $(".popup_adv").text("?");
  });


  //popup_advok
  $(".popup_advok").mouseover(function() {
    // 「?」にマウスが重なった場合の処理です。

    // ヘルプウィンドウのトップマージンを定義します。
    // 0だと「?」と上端が揃います。
    var marginTop = 0;

    // ヘルプウィンドウのレフトマージンを定義します。
    // 0だと「?」と左端が揃います。
    var marginLeft = 20;

    // ヘルプウィンドウ表示させる際のスピードを定義します。
    var speed = 300;

    // ヘルプウィンドウのオブジェクトを取得します。
    var popupObj_advok = $(".popup_advok");

    if (!popupObj_advok.length) {
      // ウィンドウがなければ作成します。
      popupObj_advok = $("<p/>").addClass("popup_advok").appendTo($("body"));
    }

    // ウィンドウにメッセージを設定します。
    popupObj_advok.text($(this).attr("data-message"));

    // ウィンドウのオフセットを計算します。
    var offsetTop = $(this).offset().top + marginTop;
    var offsetLeft = $(this).offset().left + marginLeft;

    // ウィンドウの位置を整え、表示します。
    popupObj_advok.css({
      "top": offsetTop,
      "left": offsetLeft
    }).show(speed);

  }).mouseout(function() {
    // 「?」からマウスが離れた場合、テキストを空にしてウィンドウを隠します。
    $(".popup_advok").text("?");
  });


    //popup_total
  $(".popup_total").mouseover(function() {
    // 「?」にマウスが重なった場合の処理です。

    // ヘルプウィンドウのトップマージンを定義します。
    // 0だと「?」と上端が揃います。
    var marginTop = 0;

    // ヘルプウィンドウのレフトマージンを定義します。
    // 0だと「?」と左端が揃います。
    var marginLeft = 20;

    // ヘルプウィンドウ表示させる際のスピードを定義します。
    var speed = 300;

    // ヘルプウィンドウのオブジェクトを取得します。
    var popupObj_total = $(".popup_total");

    if (!popupObj_total.length) {
      // ウィンドウがなければ作成します。
      popupObj_total = $("<p/>").addClass("popup_total").appendTo($("body"));
    }

    // ウィンドウにメッセージを設定します。
    popupObj_total.text($(this).attr("data-message"));

    // ウィンドウのオフセットを計算します。
    var offsetTop = $(this).offset().top + marginTop;
    var offsetLeft = $(this).offset().left + marginLeft;

    // ウィンドウの位置を整え、表示します。
    popupObj_total.css({
      "top": offsetTop,
      "left": offsetLeft
    }).show(speed);

  }).mouseout(function() {
    // 「?」からマウスが離れた場合、テキストを空にしてウィンドウを隠します。
    $(".popup_total").text("?");
  });

      //popup_tweet
  $(".popup_tweet").mouseover(function() {
    // 「?」にマウスが重なった場合の処理です。

    // ヘルプウィンドウのトップマージンを定義します。
    // 0だと「?」と上端が揃います。
    var marginTop = 0;

    // ヘルプウィンドウのレフトマージンを定義します。
    // 0だと「?」と左端が揃います。
    var marginLeft = 20;

    // ヘルプウィンドウ表示させる際のスピードを定義します。
    var speed = 300;

    // ヘルプウィンドウのオブジェクトを取得します。
    var popupObj_tweet = $(".popup_tweet");

    if (!popupObj_tweet.length) {
      // ウィンドウがなければ作成します。
      popupObj_tweet = $("<p/>").addClass("popup_tweet").appendTo($("body"));
    }

    // ウィンドウにメッセージを設定します。
    popupObj_tweet.text($(this).attr("data-message"));

    // ウィンドウのオフセットを計算します。
    var offsetTop = $(this).offset().top + marginTop;
    var offsetLeft = $(this).offset().left + marginLeft;

    // ウィンドウの位置を整え、表示します。
    popupObj_tweet.css({
      "top": offsetTop,
      "left": offsetLeft
    }).show(speed);

  }).mouseout(function() {
    // 「?」からマウスが離れた場合、テキストを空にしてウィンドウを隠します。
    $(".popup_tweet").text("?");
  });

})
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-85902327-1', 'auto');
  ga('send', 'pageview');

</script>

<?php
$id = isset($_GET['nicoID']) ? $_GET['nicoID'] : null; //POSTで動画のIDの値の受け渡しで受け取る
$id_nosm = $id;
$id = "nm" . $id; //スマイルビデオ(一般な動画の場合でsm0000のような形)

$base = 'http://ext.nicovideo.jp/api/getthumbinfo/'; //ニコニコAPI(動画情報取得)
$base_id = 'http://ext.nicovideo.jp/api/getthumbinfo/' . $id; //ニコニコAPI(動画情報取得)
$xml = simplexml_load_file($base_id); //ニコニコAPIのxml形式のデータを読み込む(動画情報取得)
//firsrt_retriereは投稿された日時を取得する
$day = substr($xml->thumb->first_retrieve, 0, 10); //first_retrieveから日付だけを抽出
$time = substr($xml->thumb->first_retrieve, 11, 8); //first_retrieveから時間だけを抽出
//ニコニコAPI(動画の宣伝情報:json形式) --> $base_adv
$base_adv = 'http://uad-api.nicovideo.jp/sub/1.0/UadVideoService/findAdsInfoJsonp?idvideos=' . $id;
$json_adv = file_get_contents( $base_adv ); //APIの情報を読み取る
$json_adv = preg_replace('/.+?({.+}).+/','$1',$json_adv); //$json_advのJSON形式から余分なものを抜き取る --> {} の形になり、変数を読み込めるようになる
$obj_adv = json_decode( $json_adv ) ; //JSON用の形式をPHP用に読み込む


//動画IDからURLで関連動画、ツイートの状況を調べる
$twitter_retweet =  'https://twitter.com/search?q=' . $id;//Twitter
//$twitter_retweet_full =  'https://twitter.com/search?q=' . $id . '&since:' . $day; //全期間Twitter(Twitterの使用変更？で$twitter_retweetでも全機関のツイート全部出るみたい)
$yahoo_realtime = 'http://realtime.search.yahoo.co.jp/search?p=' . $id; //yahooリアルタイム検索
$youtube = 'https://www.youtube.com/results?search_query=' . $id; //Youtube関連動画検索
$niconico = 'http://www.nicovideo.jp/search/' . $id; //ニコニコ動画関連動画検索

//エラー出力
if( $xml->error->code == "DELETED"){
    echo"<a href='../nicoinfo.html'>入力画面に戻る</a><br>
    <a href='../index.html'>トップ</a><br>";
   die ("動画が消去、または非公開にされています");
}
if( $xml->error->code == "NOT_FOUND"){
    echo"<a href='../nicoinfo.html'>入力画面に戻る</a><br>
    <a href='../index.html'>トップ</a><br>";
   die ("動画が消去、または存在しません");
}
if( $id == null){
    echo"<a href='../nicoinfo.html'>入力画面に戻る</a><br>
    <a href='../index.html'>トップ</a><br>";
   die ("IDが入力されていません");
}

//総合ポイント計算するための補正値
$revise_point = ( $xml->thumb->view_counter + $xml->thumb->mylist_counter ) / ( $xml->thumb->view_counter + $xml->thumb->comment_num +$xml->thumb->mylist_counter );


?>

</head>
 <body>
  <center>
  <a href="../nicoinfo.html">入力ページに戻る</a><br>
  <a href="../nicomylist.php">マイリストへ</a><br>
  <a href="../index.html">トップ</a><br>
  <h1>動画リンク</h1>
 <!--<iframe width='350' height='200' src=http://ext.nicovideo.jp/thumb/<?= $id ?>></iframe><br>-->
  <script type="text/javascript" src="http://ext.nicovideo.jp/thumb_watch/<?= $xml->thumb->video_id ?>"></script><noscript>
  <a href="http://www.nicovideo.jp/watch/<?= $xml->thumb->video_id ?>"><?= $xml->thumb->title ?></a></noscript>

</center>
<center>
  <h1>動画情報</h1>
  </center>

<table border="1" cellpadding="5" width="630" align="left">

      <tr>
        <th colspan="2" style="font-size:big">
        基本情報
      </th>
      </tr>
      <tr>
        <td width="300"> 動画ID </td>
          <td width="330"><?= $xml->thumb->video_id ?> </td>
      </tr>
      <tr>
        <td> タイトル </td>
          <td><?= $xml->thumb->title ?> </td>
      </tr>
      <tr>
        <td> 投稿日時 </td>
          <td><?= $day ?>  <?= $time ?></td>
      </tr>
      <tr>
        <td> 説明 </td>
        <td><?= $xml->thumb->description ?></td>
      </tr>
      <tr>
        <td> サムネイル </td>
        <td> <img src="<?= $xml->thumb->thumbnail_url ?>">  </td>
      </tr>
      <tr>
        <td> 再生時間 </td>
        <td> <?=$xml->thumb->length ?> </td>
      </tr>
      <tr>
        <td> 再生数 </td>
        <td> <?=$xml->thumb->view_counter ?> </td>
      </tr>
      <tr>
        <td> コメント数 </td>
        <td> <?=$xml->thumb->comment_num ?> </td>
      </tr>
      <tr>
        <td> マイリスト数 </td>
        <td> <?=$xml->thumb->mylist_counter ?> </td>
      </tr>

      <tr>
        <td> タグ </td>
        <td> <?= $xml->thumb->tags->tag[0] ?><br>
             <?= $xml->thumb->tags->tag[1] ?><br>
             <?= $xml->thumb->tags->tag[2] ?><br>
             <?= $xml->thumb->tags->tag[3] ?><br>
             <?= $xml->thumb->tags->tag[4] ?><br>
             <?= $xml->thumb->tags->tag[5] ?><br>
             <?= $xml->thumb->tags->tag[6] ?><br>
             <?= $xml->thumb->tags->tag[7] ?><br>
             <?= $xml->thumb->tags->tag[8] ?><br>
             <?= $xml->thumb->tags->tag[9] ?><br>
             <?= $xml->thumb->tags->tag[10] ?><br>  </td>
      </tr>
      <tr>
        <td> 最新のコメント</td>
          <td><?= $xml->thumb->last_res_body ?> </td>
      </tr>
</table>

<table border="1" cellpadding="5" width="610" align="right">
      <tr>
        <th colspan="2" style="font-size:big">
        詳細情報
      </th>
      </tr>
       <tr>
        <td width="300">  動画形式 </td>
          <td width="310"><?= $xml->thumb->movie_type ?> </td>
      </tr>
      <tr>
        <td> マイリスト率 <span class="popup_mylist" data-message="「マイリスト/再生数」で出したもの  マイリスト率が高いほど良い動画というわけではありませんが、あくまでも参考程度でお願いします">?</span></td>
        <td><?= number_format( $xml->thumb->mylist_counter  / $xml->thumb->view_counter *100, 2)
        ?> %</td>
      </tr>
      <tr>
        <td> 総広告ポイント <span class="popup_adv" data-message="動画の今までの宣伝pt">?</span></td>
        <td><?= $obj_adv->total ?> pt</td>
      </tr>
      <tr>
        <td> 広告Lv </td>
        <td><?= $obj_adv->level ?> Lv</td>
      </tr>
      <tr>
        <td> 広告の有効 <span class="popup_advok" data-message="値が1なら今宣伝されており、0ならされてない">?</span></td>
        <td><?= $obj_adv->adflg ?></td>
      </tr>
      <tr>
      <td> 総合ポイント<span class="popup_total" data-message="総合ポイント＝再生数＋（コメント数×補正値）＋マイリスト数×15＋ニコニ広告宣伝ポイント×0.3
   補正値＝（再生数＋マイリスト数）÷（再生数＋コメント数＋マイリスト数）　  ニコニコランキングで使用されている">?</span></td>
          <td><?=
          number_format( $xml->thumb->view_counter + ($xml->thumb->comment_num * $revise_point) + $xml->thumb->mylist_counter * 15 + $obj_adv->total  * 0.3, 0) . ' pt';
          ?> </td>
      </tr>
      <td> 通常動画サイズ</td>
          <td><?=
          $byte = number_format( $xml->thumb->size_high/ 1048576, 2) . ' MB';
          ?> </td>
      </tr>
      <tr>
      <td> エコノミーモード動画サイズ</td>
          <td><?=
          $byte = number_format( $xml->thumb->size_low/ 1048576, 2) . ' MB';
          ?> </td>
      </tr>
      <tr>
      <td> 投稿者ID</td>
          <td><?= $xml->thumb->user_id ?> </td>
      </tr>
      <tr>
      <td> 投稿者</td>
          <td><?= $xml->thumb->user_nickname ?> </td>
      </tr>
      <tr>
        <td> 投稿者サムネイル </td>
        <td> <img src="<?= $xml->thumb->user_icon_url ?>">  </td>
      </tr>
      <tr>
        <td> 投稿者のユーザーページ </td>
        <td><a href="http://www.nicovideo.jp/user/<?= $xml->thumb->user_id ?>" target="_blank">
          <?=$xml->thumb->user_nickname ?>さんのユーザーページ</a>
        </td>
      </tr>
      <tr>
        <td> 投稿者のマイリスト </td>
        <td><a href="http://www.nicovideo.jp/user/<?= $xml->thumb->user_id ?>/mylist" target="_blank">
          <?=$xml->thumb->user_nickname ?>さんのマイリスト</a>
        </td>
      </tr>
      <tr>
        <td> 投稿者の投稿動画 </td>
        <td><a href="http://www.nicovideo.jp/user/<?= $xml->thumb->user_id ?>/video" target="_blank">
          <?=$xml->thumb->user_nickname ?>さんの投稿動画</a>
        </td>
      </tr>
      <tr>
        <td> Twitter ツイート検索 </td>
        <td><a href=<?= $twitter_retweet ?> target="_blank">
          <?=$id ?>のツイート</a>
        </td>
      </tr>
      <tr>
        <td> Yahoo リアルタイム検索 </td>
        <td><a href=<?= $yahoo_realtime ?> target="_blank">
          <?=$id ?>のツイート</a>
        </td>
      </tr>
      <tr>
        <td> ニコニコ動画関連動画検索 </td>
        <td><a href=<?= $niconico ?> target="_blank">
          <?=$id ?>の関連動画</a>
        </td>
      </tr>
      <tr>
        <td> Youtube関連動画検索 </td>
        <td><a href=<?= $youtube ?> target="_blank">
          <?=$id ?>の関連動画</a>
        </td>
      </tr>
      </table>


  <div class = "copyright" style='font-size:8px; margin:20px;'>
  Copyright &copy; 2016 Dhikigame<br>
  このサイトはニコニコ動画とまったく関係はありません。
 </div>


</body>
</html>
