<?php
    require('videoinfo_func.php');
    require('navbar/nicovideo_navbar.php');
    require('search_condition.php');
    require('pagination/pagination.php');
    require('info/userinfo.php');
?>
<?php
   $id = isset($_GET['nicoID']) ? $_GET['nicoID'] : null; //GETでIDを受け取る
   $video_id = video_IDsearch($id); //受け取ったIDの動画の種類を調べる(sm,so,nmを判断)
   $id_sort = $video_id;//sm,so,nmを保存
   $video_id = $video_id . $id;

   $base = 'https://ext.nicovideo.jp/api/getthumbinfo/'; //ニコニコAPI(動画情報取得)
   $base_id = 'https://ext.nicovideo.jp/api/getthumbinfo/' . $video_id; //ニコニコAPI(動画情報取得)
   $xml = simplexml_load_file($base_id); //ニコニコAPIのxml形式のデータを読み込む(動画情報取得)

   if($id == null){
    $title = "ニコニコ動画検索info コメント検索";
   }else{
    $title = $xml->thumb->title." - ニコニコ動画検索info コメント検索";
   }

   $meta_url = "https://nicosearch.info/nicocome.php?nicoID=".$id;
?>
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
      <?php echo "<title>".$title."</title>"; ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <?php echo '<meta property="og:url" content="'.$meta_url.'" />'; ?>
        <meta property="og:type" content="website" />
        <?php echo '<meta property="og:title" content="'.$title.' - ニコニコ動画検索info コメント検索" />'; ?>
        <meta property="og:description" content="ニコニコ動画に投稿された動画のコメント,タグ,投稿者ランキング,新着動画,フォロー・フォロワー等の詳細な検索ができるサイトです。" />
        <meta property="og:site_name" content="ニコニコ動画検索info" />
        <meta property="og:image" content="https://nicosearch.info/image/site-title.png" />

        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content="@dhiki_pico" />
        <?php echo '<meta name="twitter:title" content="'.$title.' - ニコニコ動画検索info コメント検索" />'; ?>
        <?php echo '<meta name="twitter:url" content="'.$meta_url.'" />'; ?>
        <meta name="twitter:description" content="ニコニコ動画に投稿された動画のコメント,タグ,投稿者ランキング,新着動画,フォロー・フォロワー等の詳細な検索ができるサイトです。" />
        <meta name="twitter:image" content="https://nicosearch.info/image/site-title.png" />

          <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
            <style type="text/css">
            @font-face {
                font-family: 'GenJyuuGothic';
                src: url('fonts/genjyuugothic-20150607/GenJyuuGothic-Medium.ttf');
            }
            body{
                font-family: 'GenJyuuGothic',sans-serif;
                /* table-layout: fixed; */
                /* text-align: center; */
            }
            p{
              font-size: 20px;
            }
            img {
              zoom: 150%;
            }
            button {
              -webkit-tap-highlight-color:rgba(0,0,0,0);
            }
            #top{
              position: fixed;
              bottom: 20px;
              right: 20px;
              cursor:pointer;
            }
            #top-sp{
              position: fixed;
              bottom: 20px;
              right: 0px;
              cursor:pointer;
            }
            .color-sort {
              color: #0066FF;
            }

            .breadcrumbs{
              margin-top: 10px;
              background: #e9edf5;
            }
            .breadcrumb_1 {
              display: inline;
            }
            .breadcrumb_2 {
              display: inline;
            }
            .breadcrumb_3 {
              display: inline;
            }
            .breadcrumbs li::after{
              content: '>>';
              display: inline-block;
              padding: 0 0px; /*左右に余白*/
            }
            .breadcrumbs li:last-child::after{
              display: none;
            }
            </style>
            <script type="application/ld+json">
            {
              "@context" : "http://schema.org",
              "@type" : "WebPage",
              "name" : "ニコニコ動画検索info",
            "url" : "https://nicosearch.info/",
            "sameAs" : [
              "https://twitter.com/dhiki_pico",
              "https://www.instagram.com/nicosearchinfo/?hl=ja"
              ]
            }
            </script>
            <script>
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

              ga('create', 'UA-85902327-1', 'auto');
              ga('send', 'pageview');

            </script>
              <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
              <script src="js/jquery.tablesorter.min.js"></script>
              <script src="js/bootstrap.min.js"></script>
              <!-- スクロール関連スクリプト -->
              <script src="js/scroll.js"></script>

              <script src="js/include.js"></script>
  </head>
  <body ontouchstart="">

<a name="up"></a>


<?php $select = 4; navbar($select); ?>

    <ul itemscope itemtype="https://schema.org/BreadcrumbList" class="breadcrumbs">
      <!-- 1つめ -->
      <li itemprop="itemListElement" itemscope
          itemtype="https://schema.org/ListItem" class="breadcrumb_1">
        <a itemprop="item" href="https://nicosearch.info/">
            <span itemprop="name">ニコニコ動画検索info トップ</span>
        </a>
        <meta itemprop="position" content="1" />
      </li>

      <!-- 2つめ -->
      <li itemprop="itemListElement" itemscope
          itemtype="https://schema.org/ListItem" class="breadcrumb_2">
        <a itemprop="item" href="https://nicosearch.info/nicocomesearch.php">
            <span itemprop="name">コメント検索</span>
        </a>
        <meta itemprop="position" content="2" />
      </li>

      <!-- 3つめ -->
      <li itemprop="itemListElement" itemscope
          itemtype="https://schema.org/ListItem" class="breadcrumb_3">
        <?php echo "<a itemprop='item' href='https://nicosearch.info/nicocome.php?nicoID=".$id."'>"; ?>
        <?php echo "<span itemprop='name'>[".$xml->thumb->title."]のコメント検索</span>"; ?>
        </a>
        <meta itemprop="position" content="3" />
      </li>
    </ul>

<center>
<?php
date_default_timezone_set('Asia/Tokyo');
$date = getdate();
$date_store = $date['year'].'年'.$date['mon'].'月'.$date['mday'].'日'.$date['hours'].'時'.$date['minutes'].'分'.$date['seconds'].'秒';


$data = array(
    "mail" => email(),        // ニコニコ動画のログインメールアドレス
    "password" => password()  // ニコニコ動画のログインパスワード
);

$thread_id = null; //動画id
$ms = null; //コメントサーバのURL

$error = null;

if(!empty($_GET['nicoID']) and ctype_alnum($_GET['nicoID'])){
  $id = $_GET['nicoID'];
  $id = intval($id); //小数点などを整数で返す
  $video_id = video_IDsearch($id);
  $video = $video_id.$id;
}
else{
  //不正な動画idが入っていたら空を返却して終了
  $ret = array(
    "thread_id" => $thread_id,
    "ms" => $ms,
  );
  echo "<div class='container' style='padding:20px'>";
  echo "<center>";
  
  if( $id == null){
    echo  '<form class="form-inline" action="nicocome.php" method="get">
            <div class="input-group">
              <div class="input-group-addon">sm,nmに対応</div>
                <input type="number" class="form-control" name="nicoID" maxlength="12" placeholder="niconicoID">
            </div>
            <button type="submit" class="btn btn-primary">検索</button>
          </form>';
    echo "<div class='container' style='padding:20px'>";
    echo "<a href=./nicocome_search.php><button type='button' class='btn btn-default' data-toggle='popover'>
    入力ページに戻る</button></a></center>";
    die ("IDが入力されてません");
    echo "</div>";
  }
}

 //GETでsortを受け取る(デフォルトは投稿の新しい順)
if(!empty($_GET['sort'])){
  $sort = $_GET['sort'];
  if($sort === "long" || $sort === "short" || $sort === "old" || $sort === "latest"){
    $sort = $_GET['sort'];
  }else{
    $sort = "latest";
  }
}else{
  $sort = "latest";
}

$offset = isset($_GET['offset']) ? $_GET['offset'] : 0; //GETでoffsetを受け取る
if( $offset > 900){
  echo "<div class='container' style='padding:20px'>";
  echo "<p>offsetは90までです</p>";
  echo "</div>";
    die();
}
if( $offset < 0){
  echo "<div class='container' style='padding:20px'>";
  echo "<p>offsetエラー</p>";
  echo "</div>";
    die();
}

$meta = stream_get_meta_data($fp = tmpfile());
// ニコニコ動画にログインする
$ch = curl_init("https://secure.nicovideo.jp/secure/login?site=niconico");  // ログイン先のURL
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_COOKIEJAR, $meta['uri']);   // 受信したCookieを{$cookieFile}に保存
curl_setopt($ch, CURLOPT_POST, TRUE);               // ログイン情報はPOSTで送信
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);        // 送信する内容$data
curl_exec($ch);                         // cURLセッションを実行
curl_close($ch);                        // cURLセッションを閉じる

// getflv APIを使って動画情報を取得する
$ch = curl_init("http://ext.nicovideo.jp/api/getflv/" . $video);      // getflv APIのアドレス
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_COOKIEFILE, $meta['uri']);      // $cookieFileに保存されたcookieを送信する
$getflv = curl_exec($ch);           // cURLセッションを実行し、その結果を$getflvに代入
fclose($fp);
curl_close($ch);
    
parse_str($getflv , $parse_result);// getflvで取得したクエリ文字列を変数に代入  $ms,$thread_idを使用

if(isset($error)){
  echo "<div class='container' style='padding:20px'>";
  echo "<center><a href='./nicocome_search.php?nicoID=".$id."'><button type='button' class='btn btn-default' data-toggle='popover'>
  入力画面に戻る</button></a>";
  echo "<p>動画が見つかりません</p></center>";
  echo "</div>";
    die();
}


// ページの一番上に戻るボタン
$ua = $_SERVER['HTTP_USER_AGENT'];
if((strpos($ua,'iPhone')!==false) || (strpos($ua,'iPod')!==false) || (strpos($ua,'Android.*Mobile')!==false) || (strpos($ua,'Windows.*Phone')!==false) || (strpos($ua,'Android')!==false)) {
  //echo "<img src='image/pagetop_sp.png' id='top-sp' width='30' height='30'>";
  //echo "<span class='glyphicon glyphicon-circle-arrow-up color-sort' id='top' style='font-size: 40px'></span><img src='image/pagetop.jpg' id='top' width='30' height='30'>";
}else{
  echo "<div class='top'>";
  echo "<img src='image/pagetop.png' id='top' width='50' height='50'>";
  echo "</div>";
}

echo "<div class='container' style='padding:20px'>";
echo "<center>
      <h1><b>コメント検索</b></h1>";
?>
 <form class="form-inline" action="nicocome.php" method="get">
  <div class="input-group">
    <div class="input-group-addon">sm,nmに対応</div>
    <input type="number" class="form-control" name="nicoID" maxlength='12' placeholder="niconicoID" value="<?php if( !empty($_GET['nicoID']) ){ echo $_GET['nicoID']; } ?>">
  </div>
 <button type="submit" class="btn btn-primary">検索</button>
 </form>
<?php
/*
echo "<center>";
echo"<a href='../nicocome.html'>入力画面に戻る</a><br>
    <a href='../index.html'>トップ</a><br>";
echo "<h1>動画リンク</h1>
  <iframe width='350' height='200' src=http://ext.nicovideo.jp/thumb/".$videoid."></iframe>
  <h1>コメント情報</h1>";
  */

  $video_id = video_IDsearch($id); //受け取ったIDの動画の種類を調べる(sm,so,nmを判断)
  $exist_id_check = $video_id . $id;

  $base = 'http://ext.nicovideo.jp/api/getthumbinfo/'; //ニコニコAPI(動画情報取得)
  $base_id = 'http://ext.nicovideo.jp/api/getthumbinfo/' . $exist_id_check; //ニコニコAPI(動画情報取得)
  $xml = simplexml_load_file($base_id); //ニコニコAPIのxml形式のデータを読み込む(動画情報取得)

  //エラー出力
  if( $xml->error->code == "DELETED"){
    echo "<div class='container' style='padding:20px'>";
    echo "<center><a href=./nicocome_search.php><button type='button' class='btn btn-default' data-toggle='popover'>
    入力ページに戻る</button></a></center>";
    echo "<p>入力値 ".$id."</p>";
    die ("動画が消去、または非公開です");
    echo "</div>";
   //   die();
  }
  if( $xml->error->code == "NOT_FOUND"){
    echo "<div class='container' style='padding:20px'>";
    echo "<center><a href=./nicocome_search.php><button type='button' class='btn btn-default' data-toggle='popover'>
    入力ページに戻る</button></a></center>";
    echo "<p>入力値 ".$id."</p>";
    die ("動画が消去、または存在しません");
    echo "</div>";
   //   die();
  }
  if( $id == null){
    echo "<div class='container' style='padding:20px'>";
    echo "<center><a href=./nicocome_search.php><button type='button' class='btn btn-default' data-toggle='popover'>
    入力ページに戻る</button></a></center>";
    die ("IDが入力されてません");
    echo "</div>";
     // die();
  }

  $thread_id = $parse_result["thread_id"];
  $ms = $parse_result["ms"];
echo $thread_id;
  $xml = $ms.'thread?version=20090904&thread='.$thread_id.'&res_from=-1000';
//$comment[0] = $dom->getElementsByTagName("chat")->item(0);
//$comment_number[0] = $comment[0]->getAttribute("no");
/*
    if($comment[0] != null){
      echo "<p>※以下のコメントのダウンロードが可能です。</p>";
      echo "<p><a href=../nicocomedown.php?nicoID=".$id.">ダウンロードページへ</a></p>";
    }
*/
  $comment = comment_data($xml, $id, $video_id, $sort, $offset);
  require_once('nicocome/comment_offset_num.php');
  $comment_num = comment_num($xml, $comment);

/*
for($i = 999; $i >= 0; $i--){
$comment[$i] = $dom->getElementsByTagName("chat")->item($i);
if($comment[$i] == null){
  continue;
}
$comment_number[$i] = $comment[$i]->getAttribute("no");
$comment_value[$i] = $comment[$i]->nodeValue;
$comment_date[$i] = $comment[$i]->getAttribute("date");
echo $comment_date[$i]."<br>";
$comment_date[$i] = date("Y-m-d H:i:s", $comment_date[$i]);
$comment_playtime[$i] = $comment[$i]->getAttribute("vpos");
$comment_playtime[$i] = $comment_playtime[$i] / 100; //vposは1/100秒の単位なので、秒単位に直す
$comment_playtime[$i] = date("i:s", $comment_playtime[$i]);
$comment_user_id[$i] = $comment[$i]->getAttribute("user_id");
$comment_user_id[$i] = substr($comment_user_id[$i] , 0, 10); //ユーザーIDから最初の10文字だけを抽出

echo "<tbody>
      <tr>
        <td>".$comment_date[$i]."</td>
        <td>".$comment_number[$i]."</td>
        <td>".$comment_playtime[$i]."</td>
        <td>".$comment_value[$i]."</td>
        <td>".$comment_user_id[$i]."</td>
      </tr>
      </tbody>";
}
*/
/*
function Excel_output_comment($videoid,$date_store,$comment_date,$comment_number,$comment_playtime,$comment_value,$comment_user_id){
  require_once(__DIR__ . "/PHPExcel_1.8.0_odt/Classes/PHPExcel.php");
  require_once(__DIR__ . "/PHPExcel_1.8.0_odt/Classes/PHPExcel/IOFactory.php");

  // エクセルを新規作成
  $excel = new PHPExcel();

  // 0番目のシートをアクティブにします(シートは0から数えます)
  // (エクセルを新規作成した時点で0番目の空のシートが作成されています)
  $excel->setActiveSheetIndex(0);

  // シートに対して何かを行うためにアクティブになっているシートを変数に入れます
  $sheet = $excel->getActiveSheet();

  // シート全体のフォント・サイズを変える
$sheet->getDefaultStyle()->getFont()->setName( 'ＭＳ ゴシック' )->setSize(15);

//echo $date_store;
  // シートに名前を付けます
  $sheet->setTitle($videoid.' '.$date_store);

  $sheet->setCellValue("A1", "コメント投稿日付");
  $sheet->setCellValue("B1", "コメント投稿順");
  $sheet->setCellValue("C1", "再生時間");
  $sheet->setCellValue("D1", "コメント");
  $sheet->setCellValue("E1", "ユーザーID");

  // セルに値を入力します。
  for($i = 2; $i <= 1001; $i++){
    $cell_number_A = "A" . $i;
    $cell_number_B = "B" . $i;
    $cell_number_C = "C" . $i;
    $cell_number_D = "D" . $i;
    $cell_number_E = "E" . $i;

    //echo $cell_number."<br>";
    $sheet->setCellValue($cell_number_A, $comment_date[$i-2]);
    $sheet->setCellValue($cell_number_B, $comment_number[$i-2]);
    $sheet->setCellValue($cell_number_C, $comment_playtime[$i-2]);
    $sheet->setCellValue($cell_number_D, $comment_value[$i-2]);
    $sheet->setCellValue($cell_number_E, $comment_user_id[$i-2]);

    if($comment_date[$i-1] == null){
      break;
    }
  }

  $video_info = 'http://ext.nicovideo.jp/api/getthumbinfo/'.$videoid;
  $xml = simplexml_load_file($video_info); //ニコニコAPIのxml形式のデータを読み込む(動画情報取得)


  $excel->createSheet();
  // 1番目のシートをアクティブにします(シートは0から数えます)
  $excel->setActiveSheetIndex(1);
  // シートに対して何かを行うためにアクティブになっているシートを変数に入れます
  $sheet = $excel->getActiveSheet();
  // シートに名前を付けます
  $sheet->setTitle("動画情報");

  $sheet->setCellValue("A1", "タイトル");
  $sheet->setCellValue("B1", $xml->thumb->title);
  $sheet->setCellValue("A2", "動画ID");
  $sheet->setCellValue("B2", $xml->thumb->video_id);

  //firsrt_retriereは投稿された日時を取得する
  $day = substr($xml->thumb->first_retrieve, 0, 10); //first_retrieveから日付だけを抽出
  $time = substr($xml->thumb->first_retrieve, 11, 8); //first_retrieveから時間だけを抽出
  $day_time = $day." ".$time;
  $sheet->setCellValue("A3", "投稿時間");
  $sheet->setCellValue("B3", $day_time);
  $sheet->setCellValue("A4", "説明");
  $sheet->setCellValue("B4", $xml->thumb->description);
  $sheet->setCellValue("A5", "再生数");
  $sheet->setCellValue("B5", $xml->thumb->view_counter);
  $sheet->setCellValue("A6", "コメント数");
  $sheet->setCellValue("B6", $xml->thumb->comment_num);
  $sheet->setCellValue("A7", "マイリスト数");
  $sheet->setCellValue("B7", $xml->thumb->mylist_counter);

    $sheet->setCellValue("A8", "タグ");
    $sheet->setCellValue("B8", $xml->thumb->tags->tag[0]);
    $sheet->setCellValue("B9", $xml->thumb->tags->tag[1]);
    $sheet->setCellValue("B10", $xml->thumb->tags->tag[2]);
    $sheet->setCellValue("B11", $xml->thumb->tags->tag[3]);
    $sheet->setCellValue("B12", $xml->thumb->tags->tag[4]);
    $sheet->setCellValue("B13", $xml->thumb->tags->tag[5]);
    $sheet->setCellValue("B14", $xml->thumb->tags->tag[6]);
    $sheet->setCellValue("B15", $xml->thumb->tags->tag[7]);
    $sheet->setCellValue("B16", $xml->thumb->tags->tag[8]);
    $sheet->setCellValue("B17", $xml->thumb->tags->tag[9]);
    $sheet->setCellValue("B18", $xml->thumb->tags->tag[10]);

  $sheet->setCellValue("A19", "投稿者ID");
  $sheet->setCellValue("B19", $xml->thumb->user_id);
  $sheet->setCellValue("A20", "投稿者");
  $sheet->setCellValue("B20", $xml->thumb->user_nickname);

  $sheet->setCellValue("A22", "コメント取得日時");
  $sheet->setCellValue("B22", $date_store);

  // Excel2007形式で出力する
  $writer = PHPExcel_IOFactory::createWriter($excel, "Excel2007");
  $writer->save("comment.xlsx");
}

Excel_output_comment($videoid,$date_store,$comment_date,$comment_number,$comment_playtime,$comment_value,$comment_user_id);
*/

?>

</table>
<div class='col-sm-12 col-xs-12' style='background:black;'></div>
<div class='col-sm-12 col-xs-12' style='background:black;'></div>
  </div>
</div>
<?php
/* pagination */
//paginationはスマホ版の時にレイアウト変更フラグつける
$ua = $_SERVER['HTTP_USER_AGENT'];
if((strpos($ua,'iPhone')!==false) || (strpos($ua,'iPod')!==false) || (strpos($ua,'Android.*Mobile')!==false) || (strpos($ua,'Windows.*Phone')!==false) || (strpos($ua,'Android')!==false)) {
  $sp = true;
}else{
  $sp = false;
}
comment_pagination($id, $sort, $offset, $comment_num, $sp);
?>
<a name="down"></a>
</center>
</body>
</html>


