<?php

/*  */
function offset_diff($comment, $num=0, $offset=0){
    if($num - $offset < 100){
        $offset_start = $offset;//コメントの範囲：始め(古い方)
        $offset_end = $offset + $num; //コメントの範囲：終わり(新しい方)
    }else{
        $offset_start = $offset;//コメントの範囲：始め(古い方)
        $offset_end = $offset + 100; //コメントの範囲：終わり(新しい方)
    }
    $comment_use_count = 0;//使用するコメントをカウント
    // offsetから最大1000件から当確の範囲を取得
    for($i = $offset_start; $i <= $offset_end; $i++){
        $comment_use[$comment_use_count] = $comment[$i];
        $comment_use_count++;
    }
    return $comment_use;
}

function comment_num($xml, $comment){
    $dom = new DOMDocument('1.0', 'UTF-8');
    $dom->preserveWhiteSpace = false;
    $dom->formatOutput = true;
    $dom->load($xml);

    $comment_count = 0;
    for($i = 0; $i < 1000; $i++){
        $comment[$i] = $dom->getElementsByTagName("chat")->item($i);
        if($comment[$i] === null){
            break;
        }
        $comment_count++;
    }
    return $comment_count;
}