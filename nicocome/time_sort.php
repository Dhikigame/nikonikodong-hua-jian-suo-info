<?php
require('comment_offset_num.php');

function time_sort($comment, $num=0, $sort="long", $offset=0){
    // echo $comment[23]->getAttribute("vpos");
    /* $sort=shortの場合 */
    if($sort === "short"){
        for($i = 0; $i < $num ; $i++){
            for($j = $num; $j > $i ; $j--){
                if($comment[$j - 1]->getAttribute("vpos") > $comment[$j]->getAttribute("vpos")){
                    $tmp = $comment[$j - 1];
                    $comment[$j - 1] = $comment[$j];
                    $comment[$j] = $tmp;
                }
            }
        }
    }
    /* $sort=longの場合 */
else{ 
        for($i = 0; $i < $num; $i++){
            for($j = $num; $j > $i ; $j--){
                if($comment[$j - 1]->getAttribute("vpos") < $comment[$j]->getAttribute("vpos")){
                    $tmp = $comment[$j - 1];
                    $comment[$j - 1] = $comment[$j];
                    $comment[$j] = $tmp;
                }
            }
        }
    }

    return offset_diff($comment, $num, $offset);
}

?>