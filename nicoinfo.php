<?php
/*
    動画の情報をそれぞれ出力
*/
    function data_output($video_id,$id,$id_sort){

      $base = 'https://ext.nicovideo.jp/api/getthumbinfo/'; //ニコニコAPI(動画情報取得)
      $base_id = 'https://ext.nicovideo.jp/api/getthumbinfo/' . $video_id; //ニコニコAPI(動画情報取得)
      $xml = simplexml_load_file($base_id); //ニコニコAPIのxml形式のデータを読み込む(動画情報取得)

      $day = substr($xml->thumb->first_retrieve, 0, 10); //first_retrieveから日付だけを抽出
      $time = substr($xml->thumb->first_retrieve, 11, 8); //first_retrieveから時間だけを抽出

      //ニコニコAPI(動画の宣伝情報:json形式) --> $base_adv
      $base_adv = 'http://uad-api.nicovideo.jp/sub/1.0/UadVideoService/findAdsInfoJsonp?idvideos=' . $video_id;
      $json_adv = file_get_contents( $base_adv ); //APIの情報を読み取る
      $json_adv = preg_replace('/.+?({.+}).+/','$1',$json_adv); //$json_advのJSON形式から余分なものを抜き取る --> {} の形になり、変数を読み込めるようになる
      $obj_adv = json_decode( $json_adv ) ; //JSON用の形式をPHP用に読み込む
      //動画IDからURLで関連動画、ツイートの状況を調べる
      $twitter_retweet =  'https://twitter.com/search?q=' . $video_id;//Twitter
      $yahoo_realtime = 'https://search.yahoo.co.jp/realtime/search?p=' . $video_id; //yahooリアルタイム検索
      $youtube = 'https://www.youtube.com/results?search_query=' . $xml->thumb->title; //Youtube関連動画検索
      $niconico = 'https://www.nicovideo.jp/search/' . $video_id; //ニコニコ動画関連動画検索
      $chart = 'http://www.nicochart.jp/watch/'.$xml->thumb->video_id; //ニコニコチャート検索
      //総合ポイント計算するための補正値
      $revise_point = ( $xml->thumb->view_counter + $xml->thumb->mylist_counter ) / ( $xml->thumb->view_counter + $xml->thumb->comment_num +$xml->thumb->mylist_counter );
      echo "<p></p>";
      echo "<p></p>";
      echo "<p></p>";
      echo "<center><a href=../nicoinfo_search.php><button type='button' class='btn btn-default' data-toggle='popover'>入力ページに戻る</button></a></center>";
      echo "<p></p>";
?>
<center>
      <form class="form-inline" action="./nicoinforesult.php" method="get">
          <div class="input-group">
            <div class="input-group-addon">sm,nm,soに対応</div>
            <input type="number" class="form-control" name="nicoID" maxlength='12' placeholder="niconicoID" value="<?php if( !empty($_GET['nicoID']) ){ echo $_GET['nicoID']; } ?>">
          </div>
          <button type="submit" class="btn btn-primary">検索</button>
        </form>
</center>
<?php
echo "<center>";
//echo "<div class='col-sm-12 col-xs-12' style='background:black;'></div>";
echo "<script type='application/javascript' src='https://embed.nicovideo.jp/watch/".$xml->thumb->video_id."/script?w=640&h=360'></script>";
echo "<noscript><a href='https://www.nicovideo.jp/watch/".$xml->thumb->video_id."'>".$xml->thumb->title."</a></noscript>";

  echo "<table border='1'>";
//    echo "<thead>";
      echo "<tr>";
        echo "<th colspan='3'><center><p>タイトル</p></center></th>";
        echo "<th><center><p>サムネイル・再生時間・動画ID</p></center></th>";
      echo "</tr>";
      echo "<tr>";
        echo "<th colspan='3'><center><p><a href=https://www.google.co.jp/#q=".$xml->thumb->title." target=_blank>".$xml->thumb->title."</a></p></center></th>";
        echo "<th><center><a href='https://www.nicovideo.jp/watch/".$xml->thumb->video_id."'  target=_blank><img src=".$xml->thumb->thumbnail_url."></a><p>".$xml->thumb->length."</p><p><a href=https://www.google.co.jp/#q=".$xml->thumb->video_id." target=_blank>".$xml->thumb->video_id."</a></p></center></th>";
      echo "</tr>";
      echo "<tr>";
        echo "<th colspan='4'><center><p>総合ポイント[1]</p></center></th>";
      echo "<tr>";
        echo "<th colspan='4'><center><p>".str_replace(',','',number_format( $xml->thumb->view_counter + ($xml->thumb->comment_num * $revise_point) + $xml->thumb->mylist_counter * 15 + $obj_adv->total  * 0.3, 0))."pt</p></center></th>";
      echo "</tr>";
      echo "<tr>";
        echo "<th><center><p>投稿時間</p></center></th>";
        echo "<th><center><p>再生</p></center></th>";
        echo "<th><center><p>コメント</p></center></th>";
        echo "<th><center><p>マイリスト</p></center></th>";
      echo "</tr>";
      echo "<tr>";
        echo "<th><p>".$day." ".$time."</p></th>";
        echo "<th><p>".$xml->thumb->view_counter."</p></th>";
        echo "<th><p>".$xml->thumb->comment_num."</p></th>";
        echo "<th><p>".$xml->thumb->mylist_counter."</p></th>";
      echo "</tr>";
      echo "<tr>";
        echo "<th colspan='4'><div><center><p>説明</p></center></div></th>";
      echo "</tr>";
      echo "<tr>";
        $description = wordwrap($xml->thumb->description, 20, "<br />");
        echo "<th colspan='4'><div><span>".$description."</span></div></th>";
      echo "</tr>";
      echo "<tr>";
        echo "<th colspan='4'><div><center><p>タグ</p></center></div></th>";
      echo "</tr>";
        echo "<th colspan='4'><div>";
      for( $i=0; $i<=10; $i++){
          if($xml->thumb->tags->tag[$i]['category'] == 1 && $xml->thumb->tags->tag[$i]['lock'] == 1 ){
            echo "<a href=../nicotagsearch.php?keyword=".$xml->thumb->tags->tag[$i].">".$xml->thumb->tags->tag[$i] ."</a><b style='color: #FF0000'>(カテゴリ)</b><b style='color: #FF8C00'>(ロック)</b> ";
          }
          if($xml->thumb->tags->tag[$i]['category'] == 1 && $xml->thumb->tags->tag[$i]['lock'] != 1 ){
            echo "<a href=../nicotagsearch.php?keyword=".$xml->thumb->tags->tag[$i].">".$xml->thumb->tags->tag[$i] ."</a><b style='color: #FF0000'>(カテゴリ)</b> ";
          }
          if($xml->thumb->tags->tag[$i]['category'] != 1 && $xml->thumb->tags->tag[$i]['lock'] == 1 ){
            echo "<a href=../nicotagsearch.php?keyword=".$xml->thumb->tags->tag[$i].">".$xml->thumb->tags->tag[$i] ."</a><b style='color: #FF8C00'>(ロック)</b> ";
          }
          if($xml->thumb->tags->tag[$i]['category'] != 1 && $xml->thumb->tags->tag[$i]['lock'] != 1 ){
            echo "<a href=../nicotagsearch.php?keyword=".$xml->thumb->tags->tag[$i].">".$xml->thumb->tags->tag[$i] ."</a></style> ";
          }
      }
        echo "</p></div></th>";

        echo "<tr>";
          echo "<th colspan='4'><center><div><p>最新のコメント</p></div></center></th>";
        echo "</tr>";
        echo "<tr>";
          $comment = wordwrap($xml->thumb->last_res_body, 100, "<br />\n");
          echo "<th colspan='4'><div><span>".$xml->thumb->last_res_body."</span></div></th>";
        echo "</tr>";

        echo "<tr>";
          echo "<th colspan='1'><div><center><p>マイリスト率[2]</p></center></div></th>";
          echo "<th colspan='1'><div><center><p>動画形式</p></center></div></th>";
          echo "<th colspan='2'><div><center><p>投稿者</p></center></div></th>";
        echo "</tr>";
        echo "<tr>";
          echo "<th colspan='1'><div><p>".number_format( $xml->thumb->mylist_counter  / $xml->thumb->view_counter *100, 2)."%</p></div></th>";
          echo "<th colspan='1'><div><p>".$xml->thumb->movie_type."</p></div></th>";
          echo "<th colspan='2'>";
        if($id_sort == "sm" || $id_sort == "nm"){
          echo "<center>
                <a href=https://www.nicovideo.jp/user/".$xml->thumb->user_id." target=_blank>"
                .$xml->thumb->user_nickname."<br></a>";
        }
        else if($id_sort == "so"){
          echo "<center>
                <a href=https://ch.nicovideo.jp/ch".$xml->thumb->ch_id." target=_blank>"
                .$xml->thumb->ch_name."<br></a>";
        }
        else{
          echo "Not found";
        }

      //画像処理(画像の解像度を縦横比を維持したまま調整する)
      $user_icon_url = $xml->thumb->user_icon_url;
      if($id_sort == "sm" || $id_sort == "nm") {
        require_once("./phpQuery/phpQuery-onefile.php");
        // ユーザのサムネイル
        if((strpos($ua,'iPhone')!==false)||(strpos($ua,'iPod')!==false)||(strpos($ua,'Android')!==false)) {
          echo "<a href=https://sp.nicovideo.jp/user/".$xml->thumb->user_id." target=_blank><img src=".$user_icon_url." class='img-responsive' alt='Responsive image'></a><br>";
        } else {
          echo "<a href=https://www.nicovideo.jp/user/".$xml->thumb->user_id." target=_blank><img src=".$user_icon_url." class='img-responsive' alt='Responsive image'></a><br>";
        }
        // ユーザのフォロー・フォロワー数を出力(ユーザが退会しているなら「ユーザが存在しません」を出力)
        $html = file_get_contents("https://www.nicovideo.jp/user/".$xml->thumb->user_id."");
        // 2020/07/27更新：マイページのユーザデータをスクレイピング
        $userinfo = phpQuery::newDocument($html)
              ->find("#js-initial-userpage-data")
              ->attr('data-initial-data');
        $userinfo_json = json_decode($userinfo, true);
        $follow_user = $userinfo_json['userDetails']['userDetails']['user']['followeeCount'];
        $follower_user = $userinfo_json['userDetails']['userDetails']['user']['followerCount'];
        if($follow_user !== "" && $follower_user !== "") {
          echo "フォロー： ".$follow_user."<br>";
          echo "フォロワー： ".$follower_user."";
        } else {
            echo "<font class='waring' color='#FF4500'>フォローフォロワー取得不可</font>";
        }
      }
      else if($id_sort == "so"){
        echo "<a href=https://ch.nicovideo.jp/ch".$xml->thumb->ch_id." target=_blank><img src=".$xml->thumb->ch_icon_url." class='img-responsive' alt='Responsive image'></a>";
      }
      else{
        echo "Not found";
      }

          echo "</center></th>";
        echo "</tr>";
        echo "<tr>";
          echo "<th colspan='4'><div><center><p>内部関連リンク</p></center></div></th>";
        echo "</tr>";
        echo "<tr>";
          echo "<th colspan='4'><div><b style='font-size: 20px;'><a href=../nicocome.php?nicoID=".$id.">コメント検索</a></b><span>: 過去1000件のコメントをコメントの新しい順で表示します</span><br>";
          echo "<b style='font-size: 20px;'><a href=../nicosearch.php?keyword=".$video_id.">関連動画検索</a></b><span>: この動画に関連する動画を検索</span><br>";
          echo "<b style='font-size: 20px;'><a href=../niconicokm.php?nicoID=".$id."&km=0>過去動画検索</a></b><span>: この動画から10件前まで投稿された動画を検索します</span><br>";
          echo "<b style='font-size: 20px;'><a href=../niconicokm.php?nicoID=".$id."&km=1>未来動画検索</a></b><span>: この動画から10件後まで投稿された動画を検索します</span></div></th>";
        echo "</tr>";
        echo "<tr>";
          echo "<th colspan='4'><div><center><p>外部関連リンク</p></center></div></th>";
        echo "</tr>";
        echo "<tr>";
          echo "<th colspan='4'><div><b style='font-size: 20px;'><a href='https://www.nicovideo.jp/user/".$xml->thumb->user_id ."/video' target=_blank>投稿動画</a></b><span>: 投稿者の投稿動画</span><br>";
          echo "<b style='font-size: 20px;'><a href='https://www.nicovideo.jp/user/".$xml->thumb->user_id ."/mylist' target=_blank>マイリスト</a></b><span>: 投稿者のマイリスト</span><br>";
          echo "<b style='font-size: 20px;'><a href=https://nicoad.nicovideo.jp/video/publish/".$video_id." target=_blank>ニコニ広告</a></b><span>: この動画についた広告をみる</span><br>";
          echo "<b style='font-size: 20px;'><a href=".$twitter_retweet." target=_blank>Twitter</a></b><span>: この動画のリツイート</span><br>";
          echo "<b style='font-size: 20px;'><a href=".$yahoo_realtime." target=_blank>Yahoo リアルタイム検索</a></b><span>: この動画のリツイートをYahooのリアルタイムで検索</span><br>";
          echo "<b style='font-size: 20px;'><a href=".$youtube." target=_blank>Youtube検索</a></b><span>: この動画をYoutubeで検索</span><br>";
          echo "<b style='font-size: 20px;'><a href=".$chart." target=_blank>ニコニコチャート</a></b><span>: ニコニコチャートよりこの動画のランキング情報について調べることができます</span><br>";
        echo "</tr>";
  echo "</table>";

  echo "<span>[1]:「総合ポイント＝再生数＋（コメント数×補正値）＋マイリスト数×15」
「補正値＝（再生数＋マイリスト数）÷（再生数＋コメント数＋マイリスト数）」　  総合ランキングで使用されているポイントです</span><br>";
  echo "<span>[2]:「マイリスト / 再生数」で出したもの  マイリスト率が高いほど良い動画というわけではありませんが、あくまでも参考程度でお願いします　</span><br>";

    }


/*
    ユーザの情報を出力
*/
    function userdata_output($userid){

      require('./db/video_db_data.php');

      $video_db_data = new VideoDbData();
      $video_db_data->userid_resister($userid);
      $video_db_data->db_connect();
      $video_db_data->db_select();
      $video_db_data->db_close();
    }
?>
