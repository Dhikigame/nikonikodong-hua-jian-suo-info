<!DOCTYPE html>
<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
<meta name='viewport'
content='width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no'>
<meta name='format-detection' content='telephone=no'>
<link media='only screen and (max-device-width:480px)'
href='smart.css' type='text/css' rel='stylesheet' />
<link media='screen and (min-device-width:481px)' href='design.css'
type='text/css' rel='stylesheet' />
<!--[if IE]>
<link href='design.css' type='text/css' rel='stylesheet' />
<![endif]-->
<link rel='stylesheet' href='css/top.css' type='text/css'>
    <title>結果</title>
  </head>
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-85902327-1', 'auto');
    ga('send', 'pageview');

  </script>
  <style type="text/css">

.clear{
clear:both;}

.graphcont {

color:#FFF;
font-weight:700;
float:center;
}

.graph {
float:center;
background-color:#cecece;
position:relative;
width:100px;
padding:1
}

.graph .bar {
display:block;
background-image:url(image/bargraph.gif);
background-repeat:repeat-x;
border-right:#538e02 1px solid;
color:#fff;
height:15px;
font-family:Arial, Helvetica, sans-serif;
font-size:12px;
line-height:1.9em
}

.graph .bar span {
position:absolute;
left:1em
}

  div{
background: #FFFFFF;
width: 0px;
height: 20px;
}
</style>
<?php

require_once 'DSN.php';
$link = mysqli_connect($dsn['host'],$dsn['user'],$dsn['pass']);
$sdb = mysqli_select_db($link, $mk8db['db']);
mysqli_set_charset($link, 'utf8');


if (!$link){
die("mysql接続失敗".mysqli_error($link));
}

  $player = $_POST["player"];
  $machine = $_POST["machine"];
  $tire = $_POST["tire"];
  $glider = $_POST["glider"];


//キャラ
$player_sql="SELECT stats.speed_ground,stats.speed_water,stats.speed_air,
	stats.speed_antigravity,stats.accel,stats.weight,stats.handling_ground,
	stats.handling_water,stats.handling_air,
	stats.handling_antigravity,stats.traction,stats.miniturbo
	FROM stats,player WHERE player.chara='$player'
	AND player.weightclass=stats.weightclass";

$result_player = mysqli_query($link, $player_sql);

if (!$result_player){
die("クエリ送信失敗<br />SQL:".$player_sql);
}
$rows_player = mysqli_num_rows($result_player);

if($rows_player){
    while($row = mysqli_fetch_array($result_player)) {
      $player_speed_ground = $row["speed_ground"];
      $player_speed_water = $row["speed_water"];
      $player_speed_air = $row["speed_air"];
      $player_speed_antigravity = $row["speed_antigravity"];
      $player_accel = $row["accel"];
      $player_weight = $row["weight"];
      $player_handling_ground = $row["handling_ground"];
      $player_handling_water = $row["handling_water"];
      $player_handling_air = $row["handling_air"];
      $player_handling_antigravity = $row["handling_antigravity"];
      $player_traction = $row["traction"];
      $player_miniturbo = $row["miniturbo"];
    }
}
  else{
    $msg_player = "データなし";
  }

  mysqli_free_result($result_player);


  //マシン
$machine_sql="SELECT speed_ground, speed_water, speed_air,
speed_antigravity, accel, weight, handling_ground, handling_water,
handling_air, handling_antigravity, traction, miniturbo
FROM machine
WHERE machine =  '$machine'";

$result_machine = mysqli_query($link, $machine_sql);

if (!$result_machine){
die("クエリ送信失敗<br />SQL:".$machine_sql);
}
$rows_machine = mysqli_num_rows($result_machine);

if($rows_machine){
    while($row = mysqli_fetch_array($result_machine)) {
      $machine_speed_ground = $row["speed_ground"];
      $machine_speed_water = $row["speed_water"];
      $machine_speed_air = $row["speed_air"];
      $machine_speed_antigravity = $row["speed_antigravity"];
      $machine_accel = $row["accel"];
      $machine_weight = $row["weight"];
      $machine_handling_ground = $row["handling_ground"];
      $machine_handling_water = $row["handling_water"];
      $machine_handling_air = $row["handling_air"];
      $machine_handling_antigravity = $row["handling_antigravity"];
      $machine_traction = $row["traction"];
      $machine_miniturbo = $row["miniturbo"];
    }
}
  else{
    $msg_machine = "データなし";
  }

  mysqli_free_result($result_machine);


  //タイヤ
$tire_sql="SELECT speed_ground,speed_water,speed_air,
	speed_antigravity,accel,weight,handling_ground,
	handling_water,handling_air,
	handling_antigravity,traction,miniturbo
	FROM tire WHERE tire.tire='$tire'";

$result_tire= mysqli_query($link, $tire_sql);

if (!$result_tire){
die("クエリ送信失敗<br />SQL:".$tire_sql);
}
$rows_tire = mysqli_num_rows($result_tire);

if($rows_tire){
    while($row = mysqli_fetch_array($result_tire)) {
      $tire_speed_ground = $row["speed_ground"];
      $tire_speed_water = $row["speed_water"];
      $tire_speed_air = $row["speed_air"];
      $tire_speed_antigravity = $row["speed_antigravity"];
      $tire_accel = $row["accel"];
      $tire_weight = $row["weight"];
      $tire_handling_ground = $row["handling_ground"];
      $tire_handling_water = $row["handling_water"];
      $tire_handling_air = $row["handling_air"];
      $tire_handling_antigravity = $row["handling_antigravity"];
      $tire_traction = $row["traction"];
      $tire_miniturbo = $row["miniturbo"];
    }
}
  else{
    $msg_tire = "データなし";
  }

  mysqli_free_result($result_tire);

    //カイト
$glider_sql="SELECT speed_ground,speed_water,speed_air,
	speed_antigravity,accel,weight,handling_ground,
	handling_water,handling_air,
	handling_antigravity,traction,miniturbo
	FROM glider WHERE glider.glider='$glider'";

$result_glider= mysqli_query($link, $glider_sql);

if (!$result_glider){
die("クエリ送信失敗<br />SQL:".$glider_sql);
}
$rows_glider = mysqli_num_rows($result_glider);

if($rows_glider){
    while($row = mysqli_fetch_array($result_glider)) {
      $glider_speed_ground = $row["speed_ground"];
      $glider_speed_water = $row["speed_water"];
      $glider_speed_air = $row["speed_air"];
      $glider_speed_antigravity = $row["speed_antigravity"];
      $glider_accel = $row["accel"];
      $glider_weight = $row["weight"];
      $glider_handling_ground = $row["handling_ground"];
      $glider_handling_water = $row["handling_water"];
      $glider_handling_air = $row["handling_air"];
      $glider_handling_antigravity = $row["handling_antigravity"];
      $glider_traction = $row["traction"];
      $glider_miniturbo = $row["miniturbo"];
    }
}
  else{
    $msg_glider = "データなし";
  }

  mysqli_free_result($result_glider);

  $close_flag = mysqli_close($link);
?>



  <body>



    <table border="1" cellpadding="2" width="630" align="center">

      <tr>
        <th colspan="2" style="font-size:big">
        <b>選択したマシンの組み合わせ</b>
      </th>
      </tr>
    　<tr>
        <td> キャラクター </td>
          <td><?= $player?></td>
      </tr>
      <tr>
        <td> マシン </td>
          <td><?= $machine?></td>
      </tr>
      <tr>
        <td> タイヤ </td>
          <td><?= $tire?></td>
      </tr>
      <tr>
        <td> カイト </td>
          <td><?= $glider?></td>
      </tr>
</table>

 <script src = "http://code.jquery.com/jquery-1.11.3.min.js"></script>
 <script src = "js/jquery.color-2.1.1.js"></script>
<script type="text/javascript">
$(document).ready(function(){
//基本情報
  var speed = <?php echo $player_speed_ground + $machine_speed_ground
    + $tire_speed_ground + $glider_speed_ground; ?>;
  var accel = <?php echo $player_accel + $machine_accel
    + $tire_accel + $glider_accel; ?>;
  var weight = <?php echo $player_weight + $machine_weight
    + $tire_weight + $glider_weight; ?>;
  var handling = <?php echo $player_handling_ground + $machine_handling_ground
    + $tire_handling_ground + $glider_handling_ground; ?>;
  var traction = <?php echo $player_traction + $machine_traction
    + $tire_traction + $glider_traction; ?>;
//詳細情報
  var speed_ground = <?php echo $player_speed_ground + $machine_speed_ground
    + $tire_speed_ground + $glider_speed_ground; ?>;
  var speed_water = <?php echo $player_speed_water + $machine_speed_water
    + $tire_speed_water + $glider_speed_water; ?>;
  var speed_air = <?php echo $player_speed_air + $machine_speed_air
    + $tire_speed_air + $glider_speed_air; ?>;
  var speed_antigravity = <?php echo $player_speed_antigravity + $machine_speed_antigravity
    + $tire_speed_antigravity + $glider_speed_antigravity; ?>;

  var handling_ground = <?php echo $player_handling_ground + $machine_handling_ground
    + $tire_handling_ground + $glider_handling_ground; ?>;
  var handling_water = <?php echo $player_handling_water + $machine_handling_water
    + $tire_handling_water + $glider_handling_water; ?>;
  var handling_air = <?php echo $player_handling_air + $machine_handling_air
    + $tire_handling_air + $glider_handling_air; ?>;
  var handling_antigravity = <?php echo $player_handling_antigravity + $machine_handling_antigravity
    + $tire_handling_antigravity + $glider_handling_antigravity; ?>;

  var miniturbo = <?php echo $player_miniturbo + $machine_miniturbo
    + $tire_miniturbo + $glider_miniturbo; ?>;

  /*もっといいやり方はないものなのか
  var speed_if = 1;
while(spped_if <= 6){
  if(speed == speed_if){
    $(".speed").animate({"width": "+=25px" , backgroundColor:"#00ffff"}, "slow");
  }
  speed_if += 0.25;
}*/
//スピード
if(speed == 1){
    $(".speed").animate({width: "100px" , backgroundColor:"#00ffff"}, "slow");
}
if(speed == 1.25){
    $(".speed").animate({width: "125px" , backgroundColor:"#00f5ff"}, "slow");
}
if(speed == 1.5){
    $(".speed").animate({width: "150px" , backgroundColor:"#00ebff"}, "slow");
}
if(speed == 1.75){
    $(".speed").animate({width: "175px" , backgroundColor:"#00e1ff"}, "slow");
}
if(speed == 2){
    $(".speed").animate({width: "200px" , backgroundColor:"#00d7ff"}, "slow");
}
if(speed == 2.25){
    $(".speed").animate({width: "225px" , backgroundColor:"#00cdff"}, "slow");
}
if(speed == 2.5){
    $(".speed").animate({width: "250px" , backgroundColor:"#00c3ff"}, "slow");
}
if(speed == 2.75){
    $(".speed").animate({width: "275px" , backgroundColor:"#00b9ff"}, "slow");
}
if(speed == 3){
    $(".speed").animate({width: "300px" , backgroundColor:"#00afff"}, "slow");
}
if(speed == 3.25){
    $(".speed").animate({width: "325px" , backgroundColor:"#00a5ff"}, "slow");
}
if(speed == 3.5){
    $(".speed").animate({width: "350px" , backgroundColor:"#009bff"}, "slow");
}
if(speed == 3.75){
    $(".speed").animate({width: "375px" , backgroundColor:"#0091ff"}, "slow");
}
if(speed == 4){
    $(".speed").animate({width: "400px" , backgroundColor:"#0087ff"}, "slow");
}
if(speed == 4.25){
    $(".speed").animate({width: "425px" , backgroundColor:"#007dff"}, "slow");
}
if(speed == 4.5){
    $(".speed").animate({width: "450px" , backgroundColor:"#0073ff"}, "slow");
}
if(speed == 4.75){
    $(".speed").animate({width: "475px" , backgroundColor:"#0069ff"}, "slow");
}
if(speed == 5){
    $(".speed").animate({width: "500px" , backgroundColor:"#005fff"}, "slow");
}
if(speed == 5.25){
    $(".speed").animate({width: "525px" , backgroundColor:"#0055ff"}, "slow");
}
if(speed == 5.5){
    $(".speed").animate({width: "550px" , backgroundColor:"#004bff"}, "slow");
}
if(speed == 5.75){
    $(".speed").animate({width: "575px" , backgroundColor:"#0041ff"}, "slow");
}
if(speed == 6){
    $(".speed").animate({width: "600px" , backgroundColor:"#0037ff"}, "slow");
}
//かそく
if(accel == 1){
    $(".accel").animate({width: "100px" , backgroundColor:"#00ffff"}, "slow");
}
if(accel == 1.25){
    $(".accel").animate({width: "125px" , backgroundColor:"#00f5ff"}, "slow");
}
if(accel == 1.5){
    $(".accel").animate({width: "150px" , backgroundColor:"#00ebff"}, "slow");
}
if(accel == 1.75){
    $(".accel").animate({width: "175px" , backgroundColor:"#00e1ff"}, "slow");
}
if(accel == 2){
    $(".accel").animate({width: "200px" , backgroundColor:"#00d7ff"}, "slow");
}
if(accel == 2.25){
    $(".accel").animate({width: "225px" , backgroundColor:"#00cdff"}, "slow");
}
if(accel == 2.5){
    $(".accel").animate({width: "250px" , backgroundColor:"#00c3ff"}, "slow");
}
if(accel == 2.75){
    $(".accel").animate({width: "275px" , backgroundColor:"#00b9ff"}, "slow");
}
if(accel == 3){
    $(".accel").animate({width: "300px" , backgroundColor:"#00afff"}, "slow");
}
if(accel == 3.25){
    $(".accel").animate({width: "325px" , backgroundColor:"#00a5ff"}, "slow");
}
if(accel == 3.5){
    $(".accel").animate({width: "350px" , backgroundColor:"#009bff"}, "slow");
}
if(accel == 3.75){
    $(".accel").animate({width: "375px" , backgroundColor:"#0091ff"}, "slow");
}
if(accel == 4){
    $(".accel").animate({width: "400px" , backgroundColor:"#0087ff"}, "slow");
}
if(accel == 4.25){
    $(".accel").animate({width: "425px" , backgroundColor:"#007dff"}, "slow");
}
if(accel == 4.5){
    $(".accel").animate({width: "450px" , backgroundColor:"#0073ff"}, "slow");
}
if(accel == 4.75){
    $(".accel").animate({width: "475px" , backgroundColor:"#0069ff"}, "slow");
}
if(accel == 5){
    $(".accel").animate({width: "500px" , backgroundColor:"#005fff"}, "slow");
}
if(accel == 5.25){
    $(".accel").animate({width: "525px" , backgroundColor:"#0055ff"}, "slow");
}
if(accel == 5.5){
    $(".accel").animate({width: "550px" , backgroundColor:"#004bff"}, "slow");
}
if(accel == 5.75){
    $(".accel").animate({width: "575px" , backgroundColor:"#0041ff"}, "slow");
}
if(accel == 6){
    $(".accel").animate({width: "600px" , backgroundColor:"#0037ff"}, "slow");
}
//おもさ
if(weight == 1){
    $(".weight").animate({width: "100px" , backgroundColor:"#00ffff"}, "slow");
}
if(weight == 1.25){
    $(".weight").animate({width: "125px" , backgroundColor:"#00f5ff"}, "slow");
}
if(weight == 1.5){
    $(".weight").animate({width: "150px" , backgroundColor:"#00ebff"}, "slow");
}
if(weight == 1.75){
    $(".weight").animate({width: "175px" , backgroundColor:"#00e1ff"}, "slow");
}
if(weight == 2){
    $(".weight").animate({width: "200px" , backgroundColor:"#00d7ff"}, "slow");
}
if(weight == 2.25){
    $(".weight").animate({width: "225px" , backgroundColor:"#00cdff"}, "slow");
}
if(weight == 2.5){
    $(".weight").animate({width: "250px" , backgroundColor:"#00c3ff"}, "slow");
}
if(weight == 2.75){
    $(".weight").animate({width: "275px" , backgroundColor:"#00b9ff"}, "slow");
}
if(weight == 3){
    $(".weight").animate({width: "300px" , backgroundColor:"#00afff"}, "slow");
}
if(weight == 3.25){
    $(".weight").animate({width: "325px" , backgroundColor:"#00a5ff"}, "slow");
}
if(weight == 3.5){
    $(".weight").animate({width: "350px" , backgroundColor:"#009bff"}, "slow");
}
if(weight == 3.75){
    $(".weight").animate({width: "375px" , backgroundColor:"#0091ff"}, "slow");
}
if(weight == 4){
    $(".weight").animate({width: "400px" , backgroundColor:"#0087ff"}, "slow");
}
if(weight == 4.25){
    $(".weight").animate({width: "425px" , backgroundColor:"#007dff"}, "slow");
}
if(weight == 4.5){
    $(".weight").animate({width: "450px" , backgroundColor:"#0073ff"}, "slow");
}
if(weight == 4.75){
    $(".weight").animate({width: "475px" , backgroundColor:"#0069ff"}, "slow");
}
if(weight == 5){
    $(".weight").animate({width: "500px" , backgroundColor:"#005fff"}, "slow");
}
if(weight == 5.25){
    $(".weight").animate({width: "525px" , backgroundColor:"#0055ff"}, "slow");
}
if(weight == 5.5){
    $(".weight").animate({width: "550px" , backgroundColor:"#004bff"}, "slow");
}
if(weight == 5.75){
    $(".weight").animate({width: "575px" , backgroundColor:"#0041ff"}, "slow");
}
if(weight == 6){
    $(".weight").animate({width: "600px" , backgroundColor:"#0037ff"}, "slow");
}
//まがりやすさ
if(handling == 1){
    $(".handling").animate({width: "100px" , backgroundColor:"#00ffff"}, "slow");
}
if(handling == 1.25){
    $(".handling").animate({width: "125px" , backgroundColor:"#00f5ff"}, "slow");
}
if(handling == 1.5){
    $(".handling").animate({width: "150px" , backgroundColor:"#00ebff"}, "slow");
}
if(handling == 1.75){
    $(".handling").animate({width: "175px" , backgroundColor:"#00e1ff"}, "slow");
}
if(handling == 2){
    $(".handling").animate({width: "200px" , backgroundColor:"#00d7ff"}, "slow");
}
if(handling == 2.25){
    $(".handling").animate({width: "225px" , backgroundColor:"#00cdff"}, "slow");
}
if(handling == 2.5){
    $(".handling").animate({width: "250px" , backgroundColor:"#00c3ff"}, "slow");
}
if(handling == 2.75){
    $(".handling").animate({width: "275px" , backgroundColor:"#00b9ff"}, "slow");
}
if(handling == 3){
    $(".handling").animate({width: "300px" , backgroundColor:"#00afff"}, "slow");
}
if(handling == 3.25){
    $(".handling").animate({width: "325px" , backgroundColor:"#00a5ff"}, "slow");
}
if(handling == 3.5){
    $(".handling").animate({width: "350px" , backgroundColor:"#009bff"}, "slow");
}
if(handling == 3.75){
    $(".handling").animate({width: "375px" , backgroundColor:"#0091ff"}, "slow");
}
if(handling == 4){
    $(".handling").animate({width: "400px" , backgroundColor:"#0087ff"}, "slow");
}
if(handling == 4.25){
    $(".handling").animate({width: "425px" , backgroundColor:"#007dff"}, "slow");
}
if(handling == 4.5){
    $(".handling").animate({width: "450px" , backgroundColor:"#0073ff"}, "slow");
}
if(handling == 4.75){
    $(".handling").animate({width: "475px" , backgroundColor:"#0069ff"}, "slow");
}
if(handling == 5){
    $(".handling").animate({width: "500px" , backgroundColor:"#005fff"}, "slow");
}
if(handling == 5.25){
    $(".handling").animate({width: "525px" , backgroundColor:"#0055ff"}, "slow");
}
if(handling == 5.5){
    $(".handling").animate({width: "550px" , backgroundColor:"#004bff"}, "slow");
}
if(handling == 5.75){
    $(".handling").animate({width: "575px" , backgroundColor:"#0041ff"}, "slow");
}
if(handling == 6){
    $(".handling").animate({width: "600px" , backgroundColor:"#0037ff"}, "slow");
}
//すべりやすさ
if(traction == 1){
    $(".traction").animate({width: "100px" , backgroundColor:"#00ffff"}, "slow");
}
if(traction == 1.25){
    $(".traction").animate({width: "125px" , backgroundColor:"#00f5ff"}, "slow");
}
if(traction == 1.5){
    $(".traction").animate({width: "150px" , backgroundColor:"#00ebff"}, "slow");
}
if(traction == 1.75){
    $(".traction").animate({width: "175px" , backgroundColor:"#00e1ff"}, "slow");
}
if(traction == 2){
    $(".traction").animate({width: "200px" , backgroundColor:"#00d7ff"}, "slow");
}
if(traction == 2.25){
    $(".traction").animate({width: "225px" , backgroundColor:"#00cdff"}, "slow");
}
if(traction == 2.5){
    $(".traction").animate({width: "250px" , backgroundColor:"#00c3ff"}, "slow");
}
if(traction == 2.75){
    $(".traction").animate({width: "275px" , backgroundColor:"#00b9ff"}, "slow");
}
if(traction == 3){
    $(".traction").animate({width: "300px" , backgroundColor:"#00afff"}, "slow");
}
if(traction == 3.25){
    $(".traction").animate({width: "325px" , backgroundColor:"#00a5ff"}, "slow");
}
if(traction == 3.5){
    $(".traction").animate({width: "350px" , backgroundColor:"#009bff"}, "slow");
}
if(traction == 3.75){
    $(".traction").animate({width: "375px" , backgroundColor:"#0091ff"}, "slow");
}
if(traction == 4){
    $(".traction").animate({width: "400px" , backgroundColor:"#0087ff"}, "slow");
}
if(traction == 4.25){
    $(".traction").animate({width: "425px" , backgroundColor:"#007dff"}, "slow");
}
if(traction == 4.5){
    $(".traction").animate({width: "450px" , backgroundColor:"#0073ff"}, "slow");
}
if(traction == 4.75){
    $(".traction").animate({width: "475px" , backgroundColor:"#0069ff"}, "slow");
}
if(traction == 5){
    $(".traction").animate({width: "500px" , backgroundColor:"#005fff"}, "slow");
}
if(traction == 5.25){
    $(".traction").animate({width: "525px" , backgroundColor:"#0055ff"}, "slow");
}
if(traction == 5.5){
    $(".traction").animate({width: "550px" , backgroundColor:"#004bff"}, "slow");
}
if(traction == 5.75){
    $(".traction").animate({width: "575px" , backgroundColor:"#0041ff"}, "slow");
}
if(traction == 6){
    $(".traction").animate({width: "600px" , backgroundColor:"#0037ff"}, "slow");
}
//地上スピード
if(speed_ground == 1){
    $(".speed_ground").animate({width: "100px" , backgroundColor:"#00ffff"}, "slow");
}
if(speed_ground == 1.25){
    $(".speed_ground").animate({width: "125px" , backgroundColor:"#00f5ff"}, "slow");
}
if(speed_ground == 1.5){
    $(".speed_ground").animate({width: "150px" , backgroundColor:"#00ebff"}, "slow");
}
if(speed_ground == 1.75){
    $(".speed_ground").animate({width: "175px" , backgroundColor:"#00e1ff"}, "slow");
}
if(speed_ground == 2){
    $(".speed_ground").animate({width: "200px" , backgroundColor:"#00d7ff"}, "slow");
}
if(speed_ground == 2.25){
    $(".speed_ground").animate({width: "225px" , backgroundColor:"#00cdff"}, "slow");
}
if(speed_ground == 2.5){
    $(".speed_ground").animate({width: "250px" , backgroundColor:"#00c3ff"}, "slow");
}
if(speed_ground == 2.75){
    $(".speed_ground").animate({width: "275px" , backgroundColor:"#00b9ff"}, "slow");
}
if(speed_ground == 3){
    $(".speed_ground").animate({width: "300px" , backgroundColor:"#00afff"}, "slow");
}
if(speed_ground == 3.25){
    $(".speed_ground").animate({width: "325px" , backgroundColor:"#00a5ff"}, "slow");
}
if(speed_ground == 3.5){
    $(".speed_ground").animate({width: "350px" , backgroundColor:"#009bff"}, "slow");
}
if(speed_ground == 3.75){
    $(".speed_ground").animate({width: "375px" , backgroundColor:"#0091ff"}, "slow");
}
if(speed_ground == 4){
    $(".speed_ground").animate({width: "400px" , backgroundColor:"#0087ff"}, "slow");
}
if(speed_ground == 4.25){
    $(".speed_ground").animate({width: "425px" , backgroundColor:"#007dff"}, "slow");
}
if(speed_ground == 4.5){
    $(".speed_ground").animate({width: "450px" , backgroundColor:"#0073ff"}, "slow");
}
if(speed_ground == 4.75){
    $(".speed_ground").animate({width: "475px" , backgroundColor:"#0069ff"}, "slow");
}
if(speed_ground == 5){
    $(".speed_ground").animate({width: "500px" , backgroundColor:"#005fff"}, "slow");
}
if(speed_ground == 5.25){
    $(".speed_ground").animate({width: "525px" , backgroundColor:"#0055ff"}, "slow");
}
if(speed_ground == 5.5){
    $(".speed_ground").animate({width: "550px" , backgroundColor:"#004bff"}, "slow");
}
if(speed_ground == 5.75){
    $(".speed_ground").animate({width: "575px" , backgroundColor:"#0041ff"}, "slow");
}
if(speed_ground == 6){
    $(".speed_ground").animate({width: "600px" , backgroundColor:"#0037ff"}, "slow");
}
//水中スピード
if(speed_water == 1){
    $(".speed_water").animate({width: "100px" , backgroundColor:"#00ffff"}, "slow");
}
if(speed_water == 1.25){
    $(".speed_water").animate({width: "125px" , backgroundColor:"#00f5ff"}, "slow");
}
if(speed_water == 1.5){
    $(".speed_water").animate({width: "150px" , backgroundColor:"#00ebff"}, "slow");
}
if(speed_water == 1.75){
    $(".speed_water").animate({width: "175px" , backgroundColor:"#00e1ff"}, "slow");
}
if(speed_water == 2){
    $(".speed_water").animate({width: "200px" , backgroundColor:"#00d7ff"}, "slow");
}
if(speed_water == 2.25){
    $(".speed_water").animate({width: "225px" , backgroundColor:"#00cdff"}, "slow");
}
if(speed_water == 2.5){
    $(".speed_water").animate({width: "250px" , backgroundColor:"#00c3ff"}, "slow");
}
if(speed_water == 2.75){
    $(".speed_water").animate({width: "275px" , backgroundColor:"#00b9ff"}, "slow");
}
if(speed_water == 3){
    $(".speed_water").animate({width: "300px" , backgroundColor:"#00afff"}, "slow");
}
if(speed_water == 3.25){
    $(".speed_water").animate({width: "325px" , backgroundColor:"#00a5ff"}, "slow");
}
if(speed_water == 3.5){
    $(".speed_water").animate({width: "350px" , backgroundColor:"#009bff"}, "slow");
}
if(speed_water == 3.75){
    $(".speed_water").animate({width: "375px" , backgroundColor:"#0091ff"}, "slow");
}
if(speed_water == 4){
    $(".speed_water").animate({width: "400px" , backgroundColor:"#0087ff"}, "slow");
}
if(speed_water == 4.25){
    $(".speed_water").animate({width: "425px" , backgroundColor:"#007dff"}, "slow");
}
if(speed_water == 4.5){
    $(".speed_water").animate({width: "450px" , backgroundColor:"#0073ff"}, "slow");
}
if(speed_water == 4.75){
    $(".speed_water").animate({width: "475px" , backgroundColor:"#0069ff"}, "slow");
}
if(speed_water == 5){
    $(".speed_water").animate({width: "500px" , backgroundColor:"#005fff"}, "slow");
}
if(speed_water == 5.25){
    $(".speed_water").animate({width: "525px" , backgroundColor:"#0055ff"}, "slow");
}
if(speed_water == 5.5){
    $(".speed_water").animate({width: "550px" , backgroundColor:"#004bff"}, "slow");
}
if(speed_water == 5.75){
    $(".speed_water").animate({width: "575px" , backgroundColor:"#0041ff"}, "slow");
}
if(speed_water == 6){
    $(".speed_water").animate({width: "600px" , backgroundColor:"#0037ff"}, "slow");
}
//空中スピード
if(speed_air == 1){
    $(".speed_air").animate({width: "100px" , backgroundColor:"#00ffff"}, "slow");
}
if(speed_air == 1.25){
    $(".speed_air").animate({width: "125px" , backgroundColor:"#00f5ff"}, "slow");
}
if(speed_air == 1.5){
    $(".speed_air").animate({width: "150px" , backgroundColor:"#00ebff"}, "slow");
}
if(speed_air == 1.75){
    $(".speed_air").animate({width: "175px" , backgroundColor:"#00e1ff"}, "slow");
}
if(speed_air == 2){
    $(".speed_air").animate({width: "200px" , backgroundColor:"#00d7ff"}, "slow");
}
if(speed_air == 2.25){
    $(".speed_air").animate({width: "225px" , backgroundColor:"#00cdff"}, "slow");
}
if(speed_air == 2.5){
    $(".speed_air").animate({width: "250px" , backgroundColor:"#00c3ff"}, "slow");
}
if(speed_air == 2.75){
    $(".speed_air").animate({width: "275px" , backgroundColor:"#00b9ff"}, "slow");
}
if(speed_air == 3){
    $(".speed_air").animate({width: "300px" , backgroundColor:"#00afff"}, "slow");
}
if(speed_air == 3.25){
    $(".speed_air").animate({width: "325px" , backgroundColor:"#00a5ff"}, "slow");
}
if(speed_air == 3.5){
    $(".speed_air").animate({width: "350px" , backgroundColor:"#009bff"}, "slow");
}
if(speed_air == 3.75){
    $(".speed_air").animate({width: "375px" , backgroundColor:"#0091ff"}, "slow");
}
if(speed_air == 4){
    $(".speed_air").animate({width: "400px" , backgroundColor:"#0087ff"}, "slow");
}
if(speed_air == 4.25){
    $(".speed_air").animate({width: "425px" , backgroundColor:"#007dff"}, "slow");
}
if(speed_air == 4.5){
    $(".speed_air").animate({width: "450px" , backgroundColor:"#0073ff"}, "slow");
}
if(speed_air == 4.75){
    $(".speed_air").animate({width: "475px" , backgroundColor:"#0069ff"}, "slow");
}
if(speed_air == 5){
    $(".speed_air").animate({width: "500px" , backgroundColor:"#005fff"}, "slow");
}
if(speed_air == 5.25){
    $(".speed_air").animate({width: "525px" , backgroundColor:"#0055ff"}, "slow");
}
if(speed_air == 5.5){
    $(".speed_air").animate({width: "550px" , backgroundColor:"#004bff"}, "slow");
}
if(speed_air == 5.75){
    $(".speed_air").animate({width: "575px" , backgroundColor:"#0041ff"}, "slow");
}
if(speed_air == 6){
    $(".speed_air").animate({width: "600px" , backgroundColor:"#0037ff"}, "slow");
}
//反重力スピード
if(speed_antigravity == 1){
    $(".speed_antigravity").animate({width: "100px" , backgroundColor:"#00ffff"}, "slow");
}
if(speed_antigravity == 1.25){
    $(".speed_antigravity").animate({width: "125px" , backgroundColor:"#00f5ff"}, "slow");
}
if(speed_antigravity == 1.5){
    $(".speed_antigravity").animate({width: "150px" , backgroundColor:"#00ebff"}, "slow");
}
if(speed_antigravity == 1.75){
    $(".speed_antigravity").animate({width: "175px" , backgroundColor:"#00e1ff"}, "slow");
}
if(speed_antigravity == 2){
    $(".speed_antigravity").animate({width: "200px" , backgroundColor:"#00d7ff"}, "slow");
}
if(speed_antigravity == 2.25){
    $(".speed_antigravity").animate({width: "225px" , backgroundColor:"#00cdff"}, "slow");
}
if(speed_antigravity == 2.5){
    $(".speed_antigravity").animate({width: "250px" , backgroundColor:"#00c3ff"}, "slow");
}
if(speed_antigravity == 2.75){
    $(".speed_antigravity").animate({width: "275px" , backgroundColor:"#00b9ff"}, "slow");
}
if(speed_antigravity == 3){
    $(".speed_antigravity").animate({width: "300px" , backgroundColor:"#00afff"}, "slow");
}
if(speed_antigravity == 3.25){
    $(".speed_antigravity").animate({width: "325px" , backgroundColor:"#00a5ff"}, "slow");
}
if(speed_antigravity == 3.5){
    $(".speed_antigravity").animate({width: "350px" , backgroundColor:"#009bff"}, "slow");
}
if(speed_antigravity == 3.75){
    $(".speed_antigravity").animate({width: "375px" , backgroundColor:"#0091ff"}, "slow");
}
if(speed_antigravity == 4){
    $(".speed_antigravity").animate({width: "400px" , backgroundColor:"#0087ff"}, "slow");
}
if(speed_antigravity == 4.25){
    $(".speed_antigravity").animate({width: "425px" , backgroundColor:"#007dff"}, "slow");
}
if(speed_antigravity == 4.5){
    $(".speed_antigravity").animate({width: "450px" , backgroundColor:"#0073ff"}, "slow");
}
if(speed_antigravity == 4.75){
    $(".speed_antigravity").animate({width: "475px" , backgroundColor:"#0069ff"}, "slow");
}
if(speed_antigravity == 5){
    $(".speed_antigravity").animate({width: "500px" , backgroundColor:"#005fff"}, "slow");
}
if(speed_antigravity == 5.25){
    $(".speed_antigravity").animate({width: "525px" , backgroundColor:"#0055ff"}, "slow");
}
if(speed_antigravity == 5.5){
    $(".speed_antigravity").animate({width: "550px" , backgroundColor:"#004bff"}, "slow");
}
if(speed_antigravity == 5.75){
    $(".speed_antigravity").animate({width: "575px" , backgroundColor:"#0041ff"}, "slow");
}
if(speed_antigravity == 6){
    $(".speed_antigravity").animate({width: "600px" , backgroundColor:"#0037ff"}, "slow");
}

//地上まがりやすさ
if(handling_ground == 1){
    $(".handling_ground").animate({width: "100px" , backgroundColor:"#00ffff"}, "slow");
}
if(handling_ground == 1.25){
    $(".handling_ground").animate({width: "125px" , backgroundColor:"#00f5ff"}, "slow");
}
if(handling_ground == 1.5){
    $(".handling_ground").animate({width: "150px" , backgroundColor:"#00ebff"}, "slow");
}
if(handling_ground == 1.75){
    $(".handling_ground").animate({width: "175px" , backgroundColor:"#00e1ff"}, "slow");
}
if(handling_ground == 2){
    $(".handling_ground").animate({width: "200px" , backgroundColor:"#00d7ff"}, "slow");
}
if(handling_ground == 2.25){
    $(".handling_ground").animate({width: "225px" , backgroundColor:"#00cdff"}, "slow");
}
if(handling_ground == 2.5){
    $(".handling_ground").animate({width: "250px" , backgroundColor:"#00c3ff"}, "slow");
}
if(handling_ground == 2.75){
    $(".handling_ground").animate({width: "275px" , backgroundColor:"#00b9ff"}, "slow");
}
if(handling_ground == 3){
    $(".handling_ground").animate({width: "300px" , backgroundColor:"#00afff"}, "slow");
}
if(handling_ground == 3.25){
    $(".handling_ground").animate({width: "325px" , backgroundColor:"#00a5ff"}, "slow");
}
if(handling_ground == 3.5){
    $(".handling_ground").animate({width: "350px" , backgroundColor:"#009bff"}, "slow");
}
if(handling_ground == 3.75){
    $(".handling_ground").animate({width: "375px" , backgroundColor:"#0091ff"}, "slow");
}
if(handling_ground == 4){
    $(".handling_ground").animate({width: "400px" , backgroundColor:"#0087ff"}, "slow");
}
if(handling_ground == 4.25){
    $(".handling_ground").animate({width: "425px" , backgroundColor:"#007dff"}, "slow");
}
if(handling_ground == 4.5){
    $(".handling_ground").animate({width: "450px" , backgroundColor:"#0073ff"}, "slow");
}
if(handling_ground == 4.75){
    $(".handling_ground").animate({width: "475px" , backgroundColor:"#0069ff"}, "slow");
}
if(handling_ground == 5){
    $(".handling_ground").animate({width: "500px" , backgroundColor:"#005fff"}, "slow");
}
if(handling_ground == 5.25){
    $(".handling_ground").animate({width: "525px" , backgroundColor:"#0055ff"}, "slow");
}
if(handling_ground == 5.5){
    $(".handling_ground").animate({width: "550px" , backgroundColor:"#004bff"}, "slow");
}
if(handling_ground == 5.75){
    $(".handling_ground").animate({width: "575px" , backgroundColor:"#0041ff"}, "slow");
}
if(handling_ground == 6){
    $(".handling_ground").animate({width: "600px" , backgroundColor:"#0037ff"}, "slow");
}
//水中まがりやすさ
if(handling_water == 1){
    $(".handling_water").animate({width: "100px" , backgroundColor:"#00ffff"}, "slow");
}
if(handling_water == 1.25){
    $(".handling_water").animate({width: "125px" , backgroundColor:"#00f5ff"}, "slow");
}
if(handling_water == 1.5){
    $(".handling_water").animate({width: "150px" , backgroundColor:"#00ebff"}, "slow");
}
if(handling_water == 1.75){
    $(".handling_water").animate({width: "175px" , backgroundColor:"#00e1ff"}, "slow");
}
if(handling_water == 2){
    $(".handling_water").animate({width: "200px" , backgroundColor:"#00d7ff"}, "slow");
}
if(handling_water == 2.25){
    $(".handling_water").animate({width: "225px" , backgroundColor:"#00cdff"}, "slow");
}
if(handling_water == 2.5){
    $(".handling_water").animate({width: "250px" , backgroundColor:"#00c3ff"}, "slow");
}
if(handling_water == 2.75){
    $(".handling_water").animate({width: "275px" , backgroundColor:"#00b9ff"}, "slow");
}
if(handling_water == 3){
    $(".handling_water").animate({width: "300px" , backgroundColor:"#00afff"}, "slow");
}
if(handling_water == 3.25){
    $(".handling_water").animate({width: "325px" , backgroundColor:"#00a5ff"}, "slow");
}
if(handling_water == 3.5){
    $(".handling_water").animate({width: "350px" , backgroundColor:"#009bff"}, "slow");
}
if(handling_water == 3.75){
    $(".handling_water").animate({width: "375px" , backgroundColor:"#0091ff"}, "slow");
}
if(handling_water == 4){
    $(".handling_water").animate({width: "400px" , backgroundColor:"#0087ff"}, "slow");
}
if(handling_water == 4.25){
    $(".handling_water").animate({width: "425px" , backgroundColor:"#007dff"}, "slow");
}
if(handling_water == 4.5){
    $(".handling_water").animate({width: "450px" , backgroundColor:"#0073ff"}, "slow");
}
if(handling_water == 4.75){
    $(".handling_water").animate({width: "475px" , backgroundColor:"#0069ff"}, "slow");
}
if(handling_water == 5){
    $(".handling_water").animate({width: "500px" , backgroundColor:"#005fff"}, "slow");
}
if(handling_water == 5.25){
    $(".handling_water").animate({width: "525px" , backgroundColor:"#0055ff"}, "slow");
}
if(handling_water == 5.5){
    $(".handling_water").animate({width: "550px" , backgroundColor:"#004bff"}, "slow");
}
if(handling_water == 5.75){
    $(".handling_water").animate({width: "575px" , backgroundColor:"#0041ff"}, "slow");
}
if(handling_water == 6){
    $(".handling_water").animate({width: "600px" , backgroundColor:"#0037ff"}, "slow");
}
//空中まがりやすさ
if(handling_air == 1){
    $(".handling_air").animate({width: "100px" , backgroundColor:"#00ffff"}, "slow");
}
if(handling_air == 1.25){
    $(".handling_air").animate({width: "125px" , backgroundColor:"#00f5ff"}, "slow");
}
if(handling_air == 1.5){
    $(".handling_air").animate({width: "150px" , backgroundColor:"#00ebff"}, "slow");
}
if(handling_air == 1.75){
    $(".handling_air").animate({width: "175px" , backgroundColor:"#00e1ff"}, "slow");
}
if(handling_air == 2){
    $(".handling_air").animate({width: "200px" , backgroundColor:"#00d7ff"}, "slow");
}
if(handling_air == 2.25){
    $(".handling_air").animate({width: "225px" , backgroundColor:"#00cdff"}, "slow");
}
if(handling_air == 2.5){
    $(".handling_air").animate({width: "250px" , backgroundColor:"#00c3ff"}, "slow");
}
if(handling_air == 2.75){
    $(".handling_air").animate({width: "275px" , backgroundColor:"#00b9ff"}, "slow");
}
if(handling_air == 3){
    $(".handling_air").animate({width: "300px" , backgroundColor:"#00afff"}, "slow");
}
if(handling_air == 3.25){
    $(".handling_air").animate({width: "325px" , backgroundColor:"#00a5ff"}, "slow");
}
if(handling_air == 3.5){
    $(".handling_air").animate({width: "350px" , backgroundColor:"#009bff"}, "slow");
}
if(handling_air == 3.75){
    $(".handling_air").animate({width: "375px" , backgroundColor:"#0091ff"}, "slow");
}
if(handling_air == 4){
    $(".handling_air").animate({width: "400px" , backgroundColor:"#0087ff"}, "slow");
}
if(handling_air == 4.25){
    $(".handling_air").animate({width: "425px" , backgroundColor:"#007dff"}, "slow");
}
if(handling_air == 4.5){
    $(".handling_air").animate({width: "450px" , backgroundColor:"#0073ff"}, "slow");
}
if(handling_air == 4.75){
    $(".handling_air").animate({width: "475px" , backgroundColor:"#0069ff"}, "slow");
}
if(handling_air == 5){
    $(".handling_air").animate({width: "500px" , backgroundColor:"#005fff"}, "slow");
}
if(handling_air == 5.25){
    $(".handling_air").animate({width: "525px" , backgroundColor:"#0055ff"}, "slow");
}
if(handling_air == 5.5){
    $(".handling_air").animate({width: "550px" , backgroundColor:"#004bff"}, "slow");
}
if(handling_air == 5.75){
    $(".handling_air").animate({width: "575px" , backgroundColor:"#0041ff"}, "slow");
}
if(handling_air == 6){
    $(".handling_air").animate({width: "600px" , backgroundColor:"#0037ff"}, "slow");
}
//反重力まがりやすさ
if(handling_antigravity == 1){
    $(".handling_antigravity").animate({width: "100px" , backgroundColor:"#00ffff"}, "slow");
}
if(handling_antigravity == 1.25){
    $(".handling_antigravity").animate({width: "125px" , backgroundColor:"#00f5ff"}, "slow");
}
if(handling_antigravity == 1.5){
    $(".handling_antigravity").animate({width: "150px" , backgroundColor:"#00ebff"}, "slow");
}
if(handling_antigravity == 1.75){
    $(".handling_antigravity").animate({width: "175px" , backgroundColor:"#00e1ff"}, "slow");
}
if(handling_antigravity == 2){
    $(".handling_antigravity").animate({width: "200px" , backgroundColor:"#00d7ff"}, "slow");
}
if(handling_antigravity == 2.25){
    $(".handling_antigravity").animate({width: "225px" , backgroundColor:"#00cdff"}, "slow");
}
if(handling_antigravity == 2.5){
    $(".handling_antigravity").animate({width: "250px" , backgroundColor:"#00c3ff"}, "slow");
}
if(handling_antigravity == 2.75){
    $(".handling_antigravity").animate({width: "275px" , backgroundColor:"#00b9ff"}, "slow");
}
if(handling_antigravity == 3){
    $(".handling_antigravity").animate({width: "300px" , backgroundColor:"#00afff"}, "slow");
}
if(handling_antigravity == 3.25){
    $(".handling_antigravity").animate({width: "325px" , backgroundColor:"#00a5ff"}, "slow");
}
if(handling_antigravity == 3.5){
    $(".handling_antigravity").animate({width: "350px" , backgroundColor:"#009bff"}, "slow");
}
if(handling_antigravity == 3.75){
    $(".handling_antigravity").animate({width: "375px" , backgroundColor:"#0091ff"}, "slow");
}
if(handling_antigravity == 4){
    $(".handling_antigravity").animate({width: "400px" , backgroundColor:"#0087ff"}, "slow");
}
if(handling_antigravity == 4.25){
    $(".handling_antigravity").animate({width: "425px" , backgroundColor:"#007dff"}, "slow");
}
if(handling_antigravity == 4.5){
    $(".handling_antigravity").animate({width: "450px" , backgroundColor:"#0073ff"}, "slow");
}
if(handling_antigravity == 4.75){
    $(".handling_antigravity").animate({width: "475px" , backgroundColor:"#0069ff"}, "slow");
}
if(handling_antigravity == 5){
    $(".handling_antigravity").animate({width: "500px" , backgroundColor:"#005fff"}, "slow");
}
if(handling_antigravity == 5.25){
    $(".handling_antigravity").animate({width: "525px" , backgroundColor:"#0055ff"}, "slow");
}
if(handling_antigravity == 5.5){
    $(".handling_antigravity").animate({width: "550px" , backgroundColor:"#004bff"}, "slow");
}
if(handling_antigravity == 5.75){
    $(".handling_antigravity").animate({width: "575px" , backgroundColor:"#0041ff"}, "slow");
}
if(handling_antigravity == 6){
    $(".handling_antigravity").animate({width: "600px" , backgroundColor:"#0037ff"}, "slow");
}
//ミニターボ
if(miniturbo == 1){
    $(".miniturbo").animate({width: "100px" , backgroundColor:"#00ffff"}, "slow");
}
if(miniturbo == 1.25){
    $(".miniturbo").animate({width: "125px" , backgroundColor:"#00f5ff"}, "slow");
}
if(miniturbo == 1.5){
    $(".miniturbo").animate({width: "150px" , backgroundColor:"#00ebff"}, "slow");
}
if(miniturbo == 1.75){
    $(".miniturbo").animate({width: "175px" , backgroundColor:"#00e1ff"}, "slow");
}
if(miniturbo == 2){
    $(".miniturbo").animate({width: "200px" , backgroundColor:"#00d7ff"}, "slow");
}
if(miniturbo == 2.25){
    $(".miniturbo").animate({width: "225px" , backgroundColor:"#00cdff"}, "slow");
}
if(miniturbo == 2.5){
    $(".miniturbo").animate({width: "250px" , backgroundColor:"#00c3ff"}, "slow");
}
if(miniturbo == 2.75){
    $(".miniturbo").animate({width: "275px" , backgroundColor:"#00b9ff"}, "slow");
}
if(miniturbo == 3){
    $(".miniturbo").animate({width: "300px" , backgroundColor:"#00afff"}, "slow");
}
if(miniturbo == 3.25){
    $(".miniturbo").animate({width: "325px" , backgroundColor:"#00a5ff"}, "slow");
}
if(miniturbo == 3.5){
    $(".miniturbo").animate({width: "350px" , backgroundColor:"#009bff"}, "slow");
}
if(miniturbo == 3.75){
    $(".miniturbo").animate({width: "375px" , backgroundColor:"#0091ff"}, "slow");
}
if(miniturbo == 4){
    $(".miniturbo").animate({width: "400px" , backgroundColor:"#0087ff"}, "slow");
}
if(miniturbo == 4.25){
    $(".miniturbo").animate({width: "425px" , backgroundColor:"#007dff"}, "slow");
}
if(miniturbo == 4.5){
    $(".miniturbo").animate({width: "450px" , backgroundColor:"#0073ff"}, "slow");
}
if(miniturbo == 4.75){
    $(".miniturbo").animate({width: "475px" , backgroundColor:"#0069ff"}, "slow");
}
if(miniturbo == 5){
    $(".miniturbo").animate({width: "500px" , backgroundColor:"#005fff"}, "slow");
}
if(miniturbo == 5.25){
    $(".miniturbo").animate({width: "525px" , backgroundColor:"#0055ff"}, "slow");
}
if(miniturbo == 5.5){
    $(".miniturbo").animate({width: "550px" , backgroundColor:"#004bff"}, "slow");
}
if(miniturbo == 5.75){
    $(".miniturbo").animate({width: "575px" , backgroundColor:"#0041ff"}, "slow");
}
if(miniturbo == 6){
    $(".miniturbo").animate({width: "600px" , backgroundColor:"#0037ff"}, "slow");
}

});
</script>

    <table border="1" cellpadding="2" width="800" align="center">

      <tr>
        <th colspan="2" style="font-size:big">
        基本情報<br>
        能力：(最低値)0.0〜6.0(最高値)
      </th>
      </tr>
      <tr>
        <td width="200">スピード</td>
          <td width="600"><?= $player_speed_ground + $machine_speed_ground
    + $tire_speed_ground + $glider_speed_ground?>
  <div class="speed"></div></td>
      </tr>
      <tr>
        <td>かそく</td>
          <td><?= $player_accel + $machine_accel
    + $tire_accel + $glider_accel?>
    <div class="accel"></div></td>
      </tr>
      <tr>
        <td>おもさ</td>
          <td><?= $player_weight + $machine_weight
    + $tire_weight + $glider_weight?>
      <div class="weight"></div></td>
      </tr>
    　<tr>
        <td>まがりやすさ</td>
          <td><?= $player_handling_ground + $machine_handling_ground
    + $tire_handling_ground + $glider_handling_ground?>
    <div class="handling"></div></td>
      </tr>
      <tr>
        <td>すべりにくさ</td>
          <td><?= $player_traction + $machine_traction
    + $tire_traction + $glider_traction?>
    <div class="traction"></div></td>
      </tr>
</table>

 <?php
//計算
	  $speed_ground = $player_speed_ground + $machine_speed_ground
    + $tire_speed_ground + $glider_speed_ground;
    $speed_water = $player_speed_water + $machine_speed_water
    + $tire_speed_water + $glider_speed_water;
    $speed_air = $player_speed_air + $machine_speed_air
    + $tire_speed_air + $glider_speed_air;
    $speed_antigravity = $player_speed_antigravity + $machine_speed_antigravity
    + $tire_speed_antigravity + $glider_speed_antigravity;
    $accel = $player_accel + $machine_accel
    + $tire_accel + $glider_accel;
    $weight = $player_weight + $machine_weight
    + $tire_weight + $glider_weight;
    $handling_ground = $player_handling_ground + $machine_handling_ground
    + $tire_handling_ground + $glider_handling_ground;
    $handling_water = $player_handling_water + $machine_handling_water
    + $tire_handling_water + $glider_handling_water;
    $handling_air = $player_handling_air + $machine_handling_air
    + $tire_handling_air + $glider_handling_air;
    $handling_antigravity = $player_handling_antigravity + $machine_handling_antigravity
    + $tire_handling_antigravity + $glider_handling_antigravity;
    $traction = $player_traction + $machine_traction
    + $tire_traction + $glider_traction;
    $miniturbo = $player_miniturbo + $machine_miniturbo
    + $tire_miniturbo + $glider_miniturbo;
//表示
    echo "<table border='1' cellpadding='2' width='800' align='center'>";
    echo "<tr>
        <th colspan='2' style='font-size:big'>
        詳細情報<br>
        能力：(最低値)0.0〜6.0(最高値)
      </th>
      </tr>";
    echo "<tr>
        <td width='200'>地上スピード</td>
          <td width='600'>".$speed_ground."
        <div class='speed_ground'></div></td>
      </tr>";
    echo "<tr>
        <td>水中スピード</td>
          <td>".$speed_water."
        <div class='speed_water'></div></td>
      </tr>";
    echo "<tr>
        <td>空中スピード</td>
          <td>".$speed_air."
        <div class='speed_air'></div></td>
      </tr>";
    echo "<tr>
        <td>反重力スピード</td>
          <td>".$speed_antigravity."
        <div class='speed_antigravity'></div></td>
      </tr>";
    echo "<tr>
        <td>かそく</td>
          <td>".$accel."
          <div class='accel'></div></td>
      </tr>";
    echo "<tr>
        <td>おもさ</td>
          <td>".$weight."
          <div class='weight'></div></td>
      </tr>";
    echo "<tr>
        <td>地上まがりやすさ</td>
          <td>".$handling_ground."
        <div class='handling_ground'></div></td>
      </tr>";
    echo "<tr>
        <td>水中まがりやすさ</td>
          <td>".$handling_water."
        <div class='handling_water'></div></td>
      </tr>";
    echo "<tr>
        <td>空中まがりやすさ</td>
          <td>".$handling_air."
        <div class='handling_air'></div></td>
      </tr>";
    echo "<tr>
        <td>反重力まがりやすさ</td>
          <td>".$handling_antigravity."
        <div class='handling_antigravity'></div></td>
      </tr>";
    echo "<tr>
        <td>すべりにくさ</td>
          <td>".$traction."
        <div class='traction'></div></td>
      </tr>";
    echo "<tr>
        <td>ミニターボ</td>
          <td>".$miniturbo."
        <div class='miniturbo'></div></td>
      </tr>
      </table>";


 ?>

    <a href="javascript:window.history.back();">前に戻る</a>
    <a href="../index.html">トップ</a><br>
  </body>
</html>
