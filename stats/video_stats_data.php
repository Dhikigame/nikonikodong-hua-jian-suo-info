<?php

class VideoStatsData{

    private $videodata;
    private $video_num;
    // 再生
    private $view_sum;
    private $view_ave;
    private $view_mid;
    // コメント
    private $comment_sum;
    private $comment_ave;
    private $comment_mid;
    // マイリスト
    private $mylist_sum;
    private $mylist_ave;
    private $mylist_mid;

    private function median($list){

        sort($list);
        if (count($list) % 2 == 0) {
            return (($list[(count($list)/2)-1]+$list[((count($list)/2))])/2);
        } else {
            return ($list[floor(count($list)/2)]);
        }
    }

    public function videodata_resister($videodata) {

        $this->videodata = $videodata;
        $this->video_num = count($this->videodata["view"]);
        // 合計
        $this->view_sum = array_sum($this->videodata["view"]);
        $this->comment_sum = array_sum($this->videodata["comment"]);
        $this->mylist_sum = array_sum($this->videodata["mylist"]);
        // 平均
        $this->view_ave = $this->view_sum / $this->video_num;
        $this->comment_ave = $this->comment_sum / $this->video_num;
        $this->mylist_ave = $this->mylist_sum / $this->video_num;
        // 中央値
        $this->view_mid = $this->median($this->videodata["view"]);
        $this->comment_mid = $this->median($this->videodata["comment"]);
        $this->mylist_mid = $this->median($this->videodata["mylist"]);
    }

    public function view_sum() {

        return $this->view_sum;
    }
    public function view_ave() {
       
        return floor($this->view_ave);
    }
    public function view_mid() {
       
        return floor($this->view_mid);
    }

    public function comment_sum() {

        return $this->comment_sum;
    }
    public function comment_ave() {
        
        return floor($this->comment_ave);
    }
    public function comment_mid() {
       
        return floor($this->comment_mid);
    }

    public function mylist_sum() {

        return $this->mylist_sum;
    }
    public function mylist_ave() {
       
        return floor($this->mylist_ave);
    }
    public function mylist_mid() {
       
        return floor($this->mylist_mid);
    }

    public function video_num() {
       
        return $this->video_num;
    } 
}