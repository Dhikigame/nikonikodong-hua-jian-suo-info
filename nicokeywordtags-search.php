<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
      <title>ニコニコ動画検索info　キーワード・タグ検索</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta property="og:url" content="https://nicosearch.info/nicokeywordtags-search.php" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="ニコニコ動画検索info　キーワード・タグ検索" />
        <meta property="og:description" content="ニコニコ動画に投稿された動画のコメント,タグ,投稿者ランキング,新着動画,フォロー・フォロワー等の詳細な検索ができるサイトです。" />
        <meta property="og:site_name" content="ニコニコ動画検索info" />
        <meta property="og:image" content="https://nicosearch.info/image/site-title.png" />

        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content="@dhiki_pico" />
        <meta name="twitter:title" content="ニコニコ動画検索info　キーワード・タグ検索" />
        <meta name="twitter:url" content="https://nicosearch.info/nicokeywordtags-search.php" />
        <meta name="twitter:description" content="ニコニコ動画に投稿された動画のコメント,タグ,投稿者ランキング,新着動画,フォロー・フォロワー等の詳細な検索ができるサイトです。" />
        <meta name="twitter:image" content="https://nicosearch.info/image/site-title.png" />

          <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
          <link href="./css/style.css" rel="stylesheet" media="screen">
            <style type="text/css">
            p{
              font-size: 20px;
            }
            @font-face {
                font-family: 'GenJyuuGothic';
                src: url('fonts/genjyuugothic-20150607/GenJyuuGothic-Medium.ttf');
            }
            body{
                font-family: 'GenJyuuGothic',sans-serif;
                /* table-layout: fixed; */
                /* text-align: center; */
            }
            img {
              zoom: 150%;
            }
            button {
              -webkit-tap-highlight-color:rgba(0,0,0,0);
            }
            </style>
            <script type="application/ld+json">
            {
              "@context" : "http://schema.org",
              "@type" : "WebPage",
              "name" : "ニコニコ動画検索info",
            "url" : "https://nicosearch.info/",
            "sameAs" : [
              "https://twitter.com/dhiki_pico",
              "https://www.instagram.com/nicosearchinfo/?hl=ja"
              ]
            }
            </script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-85902327-1', 'auto');
  ga('send', 'pageview');

</script>
              <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
              <script src="js/bootstrap.min.js"></script>
  </head>
    <body>
      <?php
      require('videoinfo_func.php');
      require('navbar/nicovideo_navbar.php');
       ?>
  <a name="up"></a>

  <?php $select = 1; navbar($select); ?>

    <ul itemscope itemtype="https://schema.org/BreadcrumbList" class="breadcrumbs">
      <!-- 1つめ -->
      <li itemprop="itemListElement" itemscope
          itemtype="https://schema.org/ListItem" class="breadcrumb_1">
        <a itemprop="item" href="https://nicosearch.info/">
            <span itemprop="name">ニコニコ動画検索info トップ</span>
        </a>
        <meta itemprop="position" content="1" />
      </li>

      <!-- 2つめ -->
      <li itemprop="itemListElement" itemscope
          itemtype="https://schema.org/ListItem" class="breadcrumb_2">
        <a itemprop="item" href="https://nicosearch.info/nicokeywordtags-search.php">
            <span itemprop="name">キーワード・タグ検索</span>
        </a>
        <meta itemprop="position" content="2" />
      </li>
    </ul>

        <div class="container" style="padding:20px">
<center>
    <form class="form-inline" action="nicosearch.php" method="get">
      <div class="form-group">
        <label class="control-label">キーワード検索</label>
        <input type="text" name="keyword" class="form-control" maxlength='30' placeholder="keyword">
      </div>
      <div class="form-group">
        <input type="submit" value="検索" class="btn btn-primary">
      </div>
    </form>

    <form class="form-inline" action="nicotagsearch.php" method="get">
      <div class="form-group">
        <label class="control-label">タグ検索</label>
        <input type="text" name="keyword" class="form-control" maxlength='30' placeholder="tag">
      </div>
      <div class="form-group">
        <input type="submit" value="検索" class="btn btn-primary">
      </div>
    </form>
  </center>

      </div>
    </body>
  </html>
