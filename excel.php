<?php

//require_once(__DIR__ . "/PHPExcel_1.8.0_odt/Classes/PHPExcel.php");
//require_once(__DIR__ . "/PHPExcel_1.8.0_odt/Classes/PHPExcel/IOFactory.php");
include '/usr/lib/Classes/PHPExcel.php';
include '/usr/lib/Classes/PHPExcel/IOFactory.php';

// Excel作成
$excel = new PHPExcel();

// シートをアクティブ
$excel->setActiveSheetIndex(0);

// シートに変数格納用意
$sheet = $excel->getActiveSheet();

// シートの名前
$sheet->setTitle("テスト");

// セルに値を入力
$sheet->setCellValue('A1', '値');
$sheet->setCellValue('A2', '12345');

// Excel2007形式で出力
$writer = PHPExcel_IOFactory::createWriter($excel, "Excel2007");
$writer->save("test.xlsx");
exit;

?>
