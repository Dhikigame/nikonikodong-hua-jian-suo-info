<?php
/*
    動画の情報をそれぞれ出力
*/
    function data_output($video_id,$id){
      //require('../videoinfo_func.php');

      $base = 'http://ext.nicovideo.jp/api/getthumbinfo/'; //ニコニコAPI(動画情報取得)
      $base_id = 'http://ext.nicovideo.jp/api/getthumbinfo/' . $video_id; //ニコニコAPI(動画情報取得)
      $xml = simplexml_load_file($base_id); //ニコニコAPIのxml形式のデータを読み込む(動画情報取得)
      //$id_sort = video_IDsearch($video_id); //動画sm,nm,so取得

      $day = substr($xml->thumb->first_retrieve, 0, 10); //first_retrieveから日付だけを抽出
      $time = substr($xml->thumb->first_retrieve, 11, 8); //first_retrieveから時間だけを抽出

      //ニコニコAPI(動画の宣伝情報:json形式) --> $base_adv
      $base_adv = 'http://uad-api.nicovideo.jp/sub/1.0/UadVideoService/findAdsInfoJsonp?idvideos=' . $video_id;
      $json_adv = file_get_contents( $base_adv ); //APIの情報を読み取る
      $json_adv = preg_replace('/.+?({.+}).+/','$1',$json_adv); //$json_advのJSON形式から余分なものを抜き取る --> {} の形になり、変数を読み込めるようになる
      $obj_adv = json_decode( $json_adv ) ; //JSON用の形式をPHP用に読み込む
      //動画IDからURLで関連動画、ツイートの状況を調べる
      $twitter_retweet =  'https://twitter.com/search?q=' . $video_id;//Twitter
      //$twitter_retweet_full =  'https://twitter.com/search?q=' . $id . '&since:' . $day; //全期間Twitter(Twitterの使用変更？で$twitter_retweetでも全機関のツイート全部出るみたい)
      $yahoo_realtime = 'http://realtime.search.yahoo.co.jp/search?p=' . $video_id; //yahooリアルタイム検索
      $youtube = 'https://www.youtube.com/results?search_query=' . $video_id; //Youtube関連動画検索
      $niconico = 'http://www.nicovideo.jp/search/' . $video_id; //ニコニコ動画関連動画検索
      $chart = 'http://www.nicochart.jp/watch/'.$xml->thumb->video_id; //ニコニコチャート検索
      //総合ポイント計算するための補正値
      $revise_point = ( $xml->thumb->view_counter + $xml->thumb->mylist_counter ) / ( $xml->thumb->view_counter + $xml->thumb->comment_num +$xml->thumb->mylist_counter );
      echo "<p></p>";
      echo "<p></p>";
      echo "<p></p>";
      echo "<center><a href=../nicoinfo_search.php><button type='button' class='btn btn-default' data-toggle='popover'>入力ページに戻る</button></a></center>";
      echo "<p></p>";
?>
<center>
      <form class="form-inline" action="./nicoinfo_result.php" method="get">
          <div class="input-group">
            <div class="input-group-addon">sm,nm,soに対応</div>
            <input type="number" class="form-control" name="nicoID" maxlength='12' placeholder="niconicoID">
          </div>
          <button type="submit" class="btn btn-primary">検索</button>
        </form>
</center>
<?php
echo "<center>";
//echo "<div class='col-sm-12 col-xs-12' style='background:black;'></div>";
echo "<script type='text/javascript' src='http://ext.nicovideo.jp/thumb_watch/".$xml->thumb->video_id."'></script><noscript>";
echo "<a href='http://www.nicovideo.jp/watch/".$xml->thumb->video_id."'>".$xml->thumb->title."</a></noscript>";

  echo "<table border='1'>";
//    echo "<thead>";
      echo "<tr>";
        echo "<th colspan='3'><center><p>タイトル</p></center></th>";
        echo "<th><center><p>サムネイル・再生時間・動画ID</p></center></th>";
      echo "</tr>";
      echo "<tr>";
        echo "<th colspan='3'><center><p><a href=https://www.google.co.jp/#q=".$xml->thumb->title." target=_blank>".$xml->thumb->title."</a></p></center></th>";
        echo "<th><center><a href='http://www.nicovideo.jp/watch/".$xml->thumb->video_id."'  target=_blank><img src=".$xml->thumb->thumbnail_url."></a><p>".$xml->thumb->length."</p><p><a href=https://www.google.co.jp/#q=".$xml->thumb->video_id." target=_blank>".$xml->thumb->video_id."</a></p></center></th>";
      echo "</tr>";
      echo "<tr>";
        echo "<th colspan='4'><center><p>総合ポイント[1]</p></center></th>";
      echo "<tr>";
        echo "<th colspan='4'><center><p>".str_replace(',','',number_format( $xml->thumb->view_counter + ($xml->thumb->comment_num * $revise_point) + $xml->thumb->mylist_counter * 15 + $obj_adv->total  * 0.3, 0))."pt</p></center></th>";
      echo "</tr>";
      echo "<tr>";
        echo "<th><center><p>投稿時間</p></center></th>";
        echo "<th><center><p>再生</p></center></th>";
        echo "<th><center><p>コメント</p></center></th>";
        echo "<th><center><p>マイリスト</p></center></th>";
      echo "</tr>";
      echo "<tr>";
        echo "<th><p>".$day." ".$time."</p></th>";
        echo "<th><p>".$xml->thumb->view_counter."</p></th>";
        echo "<th><p>".$xml->thumb->comment_num."</p></th>";
        echo "<th><p>".$xml->thumb->mylist_counter."</p></th>";
      echo "</tr>";
      echo "<tr>";
        echo "<th colspan='4'><div><center><p>説明</p></center></div></th>";
      echo "</tr>";
      echo "<tr>";
        $description = wordwrap($xml->thumb->description, 20, "<br />");
        echo "<th colspan='4'><div><span>".$description."</span></div></th>";
      echo "</tr>";
      echo "<tr>";
        echo "<th colspan='4'><div><center><p>タグ</p></center></div></th>";
      echo "</tr>";
        echo "<th colspan='4'><div>";
      for( $i=0; $i<=10; $i++){
          if($xml->thumb->tags->tag[$i]['category'] == 1 && $xml->thumb->tags->tag[$i]['lock'] == 1 ){
            echo "<a href=../nicotagsearch.php?keyword=".$xml->thumb->tags->tag[$i].">".$xml->thumb->tags->tag[$i] ."</a><b style='color: #FF0000'>(カテゴリ)</b><b style='color: #FF8C00'>(ロック)</b> ";
          }
          if($xml->thumb->tags->tag[$i]['category'] == 1 && $xml->thumb->tags->tag[$i]['lock'] != 1 ){
            echo "<a href=../nicotagsearch.php?keyword=".$xml->thumb->tags->tag[$i].">".$xml->thumb->tags->tag[$i] ."</a><b style='color: #FF0000'>(カテゴリ)</b> ";
          }
          if($xml->thumb->tags->tag[$i]['category'] != 1 && $xml->thumb->tags->tag[$i]['lock'] == 1 ){
            echo "<a href=../nicotagsearch.php?keyword=".$xml->thumb->tags->tag[$i].">".$xml->thumb->tags->tag[$i] ."</a><b style='color: #FF8C00'>(ロック)</b> ";
          }
          if($xml->thumb->tags->tag[$i]['category'] != 1 && $xml->thumb->tags->tag[$i]['lock'] != 1 ){
            echo "<a href=../nicotagsearch.php?keyword=".$xml->thumb->tags->tag[$i].">".$xml->thumb->tags->tag[$i] ."</a></style> ";
          }
/*
          if($i % 2 == 1){
            echo "<p></p>";
          }
*/
      }
        echo "</p></div></th>";

        echo "<tr>";
          echo "<th colspan='4'><center><div><p>最新のコメント</p></div></center></th>";
        echo "</tr>";
        echo "<tr>";
          $comment = wordwrap($xml->thumb->last_res_body, 100, "<br />\n");
          echo "<th colspan='4'><div><span>".$xml->thumb->last_res_body."</span></div></th>";
        echo "</tr>";

        echo "<tr>";
//          echo "<th><div><p>動画形式</p></div></th>";
          echo "<th><div><center><p>マイリスト率[2]</p></center></div></th>";
          echo "<th><div><center><p>宣伝ポイント[3]</p></center></div></th>";
          echo "<th><div><center><p>広告レベル[4]</p></center></div></th>";
          echo "<th><div><center><p>宣伝中？[5]</p></center></div></th>";
        echo "</tr>";
//          echo "<th><div><p>".$xml->thumb->movie_type."</p></div></th>";
        echo "<tr>";
          echo "<th><div><p>".number_format( $xml->thumb->mylist_counter  / $xml->thumb->view_counter *100, 2)."%</p></div></th>";
          echo "<th><div><p>".$obj_adv->total."pt</p></div></th>";
          echo "<th><div><p>Lv".$obj_adv->level."</p></div></th>";
          if($obj_adv->adflg == 1){
            $advok = "宣伝中";
          }
          else{
            $advok = "No";
          }
          echo "<th><div><p>".$advok."</p></div></th>";
        echo "</tr>";
        echo "<tr>";
//          echo "<th><div><p>動画形式</p></div></th>";
          echo "<th><div><center><p>動画形式</p></center></div></th>";
          echo "<th><div><center><p>通常動画サイズ</p></center></div></th>";
          echo "<th><div><center><p>エコノミーモード動画サイズ</p></center></div></th>";
          echo "<th><div><center><p>投稿者</p></center></div></th>";
        echo "</tr>";
        echo "<tr>";
          echo "<th><div><p>".$xml->thumb->movie_type."</p></div></th>";
          echo "<th><div><p>" . number_format( $xml->thumb->size_high/ 1048576, 2) . "MB</p></div></th>";
          echo "<th><div><p>" . number_format( $xml->thumb->size_low/ 1048576, 2) . "MB</p></div></th>";
          echo "<th><div><a href='http://www.nicovideo.jp/user/".$xml->thumb->user_id ."' target=_blank><center><p><img src=". $xml->thumb->user_icon_url ."></a></p>";
 //               if($id_sort == "sm" || $id_sort == "nm"){
                    require_once("../phpQuery/phpQuery-onefile.php");
                    $html = file_get_contents("http://www.nicovideo.jp/user/".$xml->thumb->user_id."");
                    echo "フォロワー数：".phpQuery::newDocument($html)->find(".userDetail")->find(".profile")->find(".stats")->find("ul")->find("li:eq(1)")->find("span")->text()."<br>";
                    echo "スタンプEXP：".phpQuery::newDocument($html)->find(".userDetail")->find(".profile")->find(".stats")->find("ul")->find("li:eq(4)")->find("span")->text();
//                }
          echo "</center><br>";
          echo "<a href='http://www.nicovideo.jp/user/".$xml->thumb->user_id ."' target=_blank><center><p>".$xml->thumb->user_nickname."</a></p></center></div></th>";
        echo "</tr>";
        echo "<tr>";
          echo "<th colspan='4'><div><center><p>内部関連リンク</p></center></div></th>";
        echo "</tr>";
        echo "<tr>";
          echo "<th colspan='4'><div><b style='font-size: 20px;'><a href=../nicocome.php?nicoID=".$id.">コメント検索</a></b><span>: 過去1000件のコメントをコメントの新しい順で表示します</span><br>";
          echo "<b style='font-size: 20px;'><a href=../nicosearch.php?keyword=".$video_id.">関連動画検索</a></b><span>: この動画に関連する動画を検索</span><br>";
          echo "<b style='font-size: 20px;'><a href=../niconicokm.php?nicoID=".$id."&km=0>過去動画検索</a></b><span>: この動画から10件前まで投稿された動画を検索します</span><br>";
          echo "<b style='font-size: 20px;'><a href=../niconicokm.php?nicoID=".$id."&km=1>未来動画検索</a></b><span>: この動画から10件後まで投稿された動画を検索します</span></div></th>";
        echo "</tr>";
        echo "<tr>";
          echo "<th colspan='4'><div><center><p>外部関連リンク</p></center></div></th>";
        echo "</tr>";
        echo "<tr>";
          echo "<th colspan='4'><div><b style='font-size: 20px;'><a href='http://www.nicovideo.jp/user/".$xml->thumb->user_id ."/video' target=_blank>投稿動画</a></b><span>: 投稿者の投稿動画</span><br>";
          echo "<b style='font-size: 20px;'><a href='http://www.nicovideo.jp/user/".$xml->thumb->user_id ."/mylist' target=_blank>マイリスト</a></b><span>: 投稿者のマイリスト</span><br>";
          echo "<b style='font-size: 20px;'><a href=https://nicoad.nicovideo.jp/video/publish/".$video_id." target=_blank>ニコニ広告</a></b><span>: この動画についた広告をみる</span><br>";
          echo "<b style='font-size: 20px;'><a href=".$twitter_retweet." target=_blank>Twitter</a></b><span>: この動画のリツイート</span><br>";
          echo "<b style='font-size: 20px;'><a href=".$yahoo_realtime." target=_blank>Yahoo リアルタイム検索</a></b><span>: この動画のリツイートをYahooのリアルタイムで検索</span><br>";
          echo "<b style='font-size: 20px;'><a href=".$youtube." target=_blank>Youtube検索</a></b><span>: この動画をYoutubeで検索</span><br>";
          echo "<b style='font-size: 20px;'><a href=".$chart." target=_blank>ニコニコチャート</a></b><span>: ニコニコチャートよりこの動画のランキング情報について調べることができます</span><br>";
        echo "</tr>";
  echo "</table>";

  echo "<span>[1]:「総合ポイント＝再生数＋（コメント数×補正値）＋マイリスト数×15＋ニコニ広告宣伝ポイント×0.3」
「補正値＝（再生数＋マイリスト数）÷（再生数＋コメント数＋マイリスト数）」　  総合ランキングで使用されているポイントです</span><br>";
  echo "<span>[2]:「マイリスト / 再生数」で出したもの  マイリスト率が高いほど良い動画というわけではありませんが、あくまでも参考程度でお願いします　</span><br>";
  echo "<span>[3]:動画の今までに入った宣伝ポイント　</span>";
  echo "<span>[4]:宣伝ポイントによってレベルが変わります(レベル範囲1～10)　</span>";
  echo "<span>[5]:今この動画が宣伝されているか</span>";
    }
?>
