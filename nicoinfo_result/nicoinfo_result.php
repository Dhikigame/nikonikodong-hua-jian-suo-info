<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
      <title>ニコニコ動画検索info　動画詳細検索</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
            <style type="text/css">
            p{
              font-size: 20px;
            }
            img {
              zoom: 150%;
            }
            button {
              -webkit-tap-highlight-color:rgba(0,0,0,0);
            }
            </style>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-85902327-1', 'auto');
  ga('send', 'pageview');

</script>
<script src = "http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script>
$(function() {
// 参考URL:https://remotestance.com/blog/1522/
//popup_mylist
  $(".popup_mylist").mouseover(function() {
    // 「?」にマウスが重なった場合の処理です。

    // ヘルプウィンドウのトップマージンを定義します。
    // 0だと「?」と上端が揃います。
    var marginTop = 0;

    // ヘルプウィンドウのレフトマージンを定義します。
    // 0だと「?」と左端が揃います。
    var marginLeft = 20;

    // ヘルプウィンドウ表示させる際のスピードを定義します。
    var speed = 300;

    // ヘルプウィンドウのオブジェクトを取得します。
    var popupObj_mylist = $(".popup_mylist");

    if (!popupObj_mylist.length) {
      // ウィンドウがなければ作成します。
      popupObj_mylist = $("<p/>").addClass("popup_mylist").appendTo($("body"));
    }

    // ウィンドウにメッセージを設定します。
    popupObj_mylist.text($(this).attr("data-message"));

    // ウィンドウのオフセットを計算します。
    var offsetTop = $(this).offset().top + marginTop;
    var offsetLeft = $(this).offset().left + marginLeft;

    // ウィンドウの位置を整え、表示します。
    popupObj_mylist.css({
      "top": offsetTop,
      "left": offsetLeft
    }).show(speed);

  }).mouseout(function() {
    // 「?」からマウスが離れた場合、テキストを空にしてウィンドウを隠します。
    $(".popup_mylist").text("?");
  });


//popup_adv
  $(".popup_adv").mouseover(function() {
    // 「?」にマウスが重なった場合の処理です。

    // ヘルプウィンドウのトップマージンを定義します。
    // 0だと「?」と上端が揃います。
    var marginTop = 0;

    // ヘルプウィンドウのレフトマージンを定義します。
    // 0だと「?」と左端が揃います。
    var marginLeft = 20;

    // ヘルプウィンドウ表示させる際のスピードを定義します。
    var speed = 300;

    // ヘルプウィンドウのオブジェクトを取得します。
    var popupObj_adv = $(".popup_adv");

    if (!popupObj_adv.length) {
      // ウィンドウがなければ作成します。
      popupObj_adv = $("<p/>").addClass("popup_adv").appendTo($("body"));
    }

    // ウィンドウにメッセージを設定します。
    popupObj_adv.text($(this).attr("data-message"));

    // ウィンドウのオフセットを計算します。
    var offsetTop = $(this).offset().top + marginTop;
    var offsetLeft = $(this).offset().left + marginLeft;

    // ウィンドウの位置を整え、表示します。
    popupObj_adv.css({
      "top": offsetTop,
      "left": offsetLeft
    }).show(speed);

  }).mouseout(function() {
    // 「?」からマウスが離れた場合、テキストを空にしてウィンドウを隠します。
    $(".popup_adv").text("?");
  });


  //popup_advok
  $(".popup_advok").mouseover(function() {
    // 「?」にマウスが重なった場合の処理です。

    // ヘルプウィンドウのトップマージンを定義します。
    // 0だと「?」と上端が揃います。
    var marginTop = 0;

    // ヘルプウィンドウのレフトマージンを定義します。
    // 0だと「?」と左端が揃います。
    var marginLeft = 20;

    // ヘルプウィンドウ表示させる際のスピードを定義します。
    var speed = 300;

    // ヘルプウィンドウのオブジェクトを取得します。
    var popupObj_advok = $(".popup_advok");

    if (!popupObj_advok.length) {
      // ウィンドウがなければ作成します。
      popupObj_advok = $("<p/>").addClass("popup_advok").appendTo($("body"));
    }

    // ウィンドウにメッセージを設定します。
    popupObj_advok.text($(this).attr("data-message"));

    // ウィンドウのオフセットを計算します。
    var offsetTop = $(this).offset().top + marginTop;
    var offsetLeft = $(this).offset().left + marginLeft;

    // ウィンドウの位置を整え、表示します。
    popupObj_advok.css({
      "top": offsetTop,
      "left": offsetLeft
    }).show(speed);

  }).mouseout(function() {
    // 「?」からマウスが離れた場合、テキストを空にしてウィンドウを隠します。
    $(".popup_advok").text("?");
  });


    //popup_total
  $(".popup_total").mouseover(function() {
    // 「?」にマウスが重なった場合の処理です。

    // ヘルプウィンドウのトップマージンを定義します。
    // 0だと「?」と上端が揃います。
    var marginTop = 0;

    // ヘルプウィンドウのレフトマージンを定義します。
    // 0だと「?」と左端が揃います。
    var marginLeft = 20;

    // ヘルプウィンドウ表示させる際のスピードを定義します。
    var speed = 300;

    // ヘルプウィンドウのオブジェクトを取得します。
    var popupObj_total = $(".popup_total");

    if (!popupObj_total.length) {
      // ウィンドウがなければ作成します。
      popupObj_total = $("<p/>").addClass("popup_total").appendTo($("body"));
    }

    // ウィンドウにメッセージを設定します。
    popupObj_total.text($(this).attr("data-message"));

    // ウィンドウのオフセットを計算します。
    var offsetTop = $(this).offset().top + marginTop;
    var offsetLeft = $(this).offset().left + marginLeft;

    // ウィンドウの位置を整え、表示します。
    popupObj_total.css({
      "top": offsetTop,
      "left": offsetLeft
    }).show(speed);

  }).mouseout(function() {
    // 「?」からマウスが離れた場合、テキストを空にしてウィンドウを隠します。
    $(".popup_total").text("?");
  });

      //popup_tweet
  $(".popup_tweet").mouseover(function() {
    // 「?」にマウスが重なった場合の処理です。

    // ヘルプウィンドウのトップマージンを定義します。
    // 0だと「?」と上端が揃います。
    var marginTop = 0;

    // ヘルプウィンドウのレフトマージンを定義します。
    // 0だと「?」と左端が揃います。
    var marginLeft = 20;

    // ヘルプウィンドウ表示させる際のスピードを定義します。
    var speed = 300;

    // ヘルプウィンドウのオブジェクトを取得します。
    var popupObj_tweet = $(".popup_tweet");

    if (!popupObj_tweet.length) {
      // ウィンドウがなければ作成します。
      popupObj_tweet = $("<p/>").addClass("popup_tweet").appendTo($("body"));
    }

    // ウィンドウにメッセージを設定します。
    popupObj_tweet.text($(this).attr("data-message"));

    // ウィンドウのオフセットを計算します。
    var offsetTop = $(this).offset().top + marginTop;
    var offsetLeft = $(this).offset().left + marginLeft;

    // ウィンドウの位置を整え、表示します。
    popupObj_tweet.css({
      "top": offsetTop,
      "left": offsetLeft
    }).show(speed);

  }).mouseout(function() {
    // 「?」からマウスが離れた場合、テキストを空にしてウィンドウを隠します。
    $(".popup_tweet").text("?");
  });

  //popup_advlv
  $(".popup_advlv").mouseover(function() {
    // 「?」にマウスが重なった場合の処理です。

    // ヘルプウィンドウのトップマージンを定義します。
    // 0だと「?」と上端が揃います。
    var marginTop = 0;

    // ヘルプウィンドウのレフトマージンを定義します。
    // 0だと「?」と左端が揃います。
    var marginLeft = 20;

    // ヘルプウィンドウ表示させる際のスピードを定義します。
    var speed = 300;

    // ヘルプウィンドウのオブジェクトを取得します。
    var popupObj_advok = $(".popup_advlv");

    if (!popupObj_advlv.length) {
      // ウィンドウがなければ作成します。
      popupObj_advok = $("<p/>").addClass("popup_advlv").appendTo($("body"));
    }

    // ウィンドウにメッセージを設定します。
    popupObj_advlv.text($(this).attr("data-message"));

    // ウィンドウのオフセットを計算します。
    var offsetTop = $(this).offset().top + marginTop;
    var offsetLeft = $(this).offset().left + marginLeft;

    // ウィンドウの位置を整え、表示します。
    popupObj_advlv.css({
      "top": offsetTop,
      "left": offsetLeft
    }).show(speed);

  }).mouseout(function() {
    // 「?」からマウスが離れた場合、テキストを空にしてウィンドウを隠します。
    $(".popup_advlv").text("?");
  });


})
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>

<script src="../js/include.js"></script>
</head>

<body>
  <a name="up"></a>
<?php
   require('../videoinfo_func.php');
   require('../navbar/nicoinfo_navbar.php');
   $select = 2; navbar($select);
   require('./nicoinfo.php');
?>

<?php
   $id = isset($_GET['nicoID']) ? $_GET['nicoID'] : null; //GETでIDを受け取る
   $video_id = video_IDsearch($id); //受け取ったIDの動画の種類を調べる(sm,so,nmを判断)
//echo $id;
   $video_id = $video_id . $id;

   $base = 'http://ext.nicovideo.jp/api/getthumbinfo/'; //ニコニコAPI(動画情報取得)
   $base_id = 'http://ext.nicovideo.jp/api/getthumbinfo/' . $video_id; //ニコニコAPI(動画情報取得)
   $xml = simplexml_load_file($base_id); //ニコニコAPIのxml形式のデータを読み込む(動画情報取得)
//echo  $base_id;

   //エラー出力
   if( $xml->error->code == "DELETED"){
     echo "<div class='container' style='padding:20px'>";
     echo "<center><a href=../nicoinfo_search.php><button type='button' class='btn btn-default' data-toggle='popover'>
     入力ページに戻る</button></a></center>";
     echo "<p>入力値 ".$id."</p>";
     die ("動画が消去、または非公開です");
     echo "</div>";
    //   die();
   }
   if( $xml->error->code == "NOT_FOUND"){
     echo "<div class='container' style='padding:20px'>";
     echo "<center><a href=../nicoinfo_search.php><button type='button' class='btn btn-default' data-toggle='popover'>
     入力ページに戻る</button></a></center>";
     echo "<p>入力値 ".$id."</p>";
     die ("動画が消去、または存在しません");
     echo "</div>";
    //   die();
   }
   if( $id == null){
     echo "<div class='container' style='padding:20px'>";
     echo "<center><a href=../nicoinfo_search.php><button type='button' class='btn btn-default' data-toggle='popover'>
     入力ページに戻る</button></a></center>";
     echo "<p>入力値 ".$id."</p>";
     die ("IDが入力されてません");
     echo "</div>";
      // die();
   }

data_output($video_id,$id);
?>

  <a name="down"></a>
</div>
 </body>
</html>
