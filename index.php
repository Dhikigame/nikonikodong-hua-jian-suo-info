<!DOCTYPE html>
<html lang="ja" prefix="og: http://ogp.me/ns#">
  <head>
    <meta charset="utf-8">
      <title>ニコニコ動画検索info 〜ニコニコ動画に投稿された動画の詳細検索サイト〜</title>

      <meta property="og:url" content="https://nicosearch.info/" />
      <meta property="og:type" content="website" />
      <meta property="og:title" content="ニコニコ動画検索info 〜ニコニコ動画に投稿された動画の詳細検索サイト〜" />
      <meta property="og:description" content="ニコニコ動画に投稿された動画のコメント,タグ,投稿者ランキング,新着動画,フォロー・フォロワー等の詳細な検索ができるサイトです。" />
      <meta property="og:site_name" content="ニコニコ動画検索info" />
      <meta property="og:image" content="https://nicosearch.info/image/site-title.png" />

      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:site" content="@dhiki_pico" />
      <meta name="twitter:title" content="ニコニコ動画検索info 〜ニコニコ動画に投稿された動画の詳細検索サイト〜" />
      <meta name="twitter:url" content="https://nicosearch.info/" />
      <meta name="twitter:description" content="ニコニコ動画に投稿された動画のコメント,タグ,投稿者ランキング,新着動画,フォロー・フォロワー等の詳細な検索ができるサイトです。" />
      <meta name="twitter:image" content="https://nicosearch.info/image/site-title.png" />

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="ニコニコ動画に投稿された動画のコメント,タグ,投稿者ランキング,新着動画,フォロー・フォロワー等の詳細な検索ができるサイトです。">
          <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
            <style type="text/css">
            @font-face {
                font-family: 'GenJyuuGothic';
                src: url('fonts/genjyuugothic-20150607/GenJyuuGothic-Medium.ttf');
            }
            body{
                font-family: 'GenJyuuGothic',sans-serif;
                /* table-layout: fixed; */
                text-align: center;
            }
            p{
              font-size: 20px;
            }
            img {
              zoom: 150%;
            }
            button {
              -webkit-tap-highlight-color:rgba(0,0,0,0);
            }
            </style>
            <script>
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

              ga('create', 'UA-85902327-1', 'auto');
              ga('send', 'pageview');

            </script>
<script type="application/ld+json">
{
  "@context" : "http://schema.org",
  "@type" : "WebPage",
  "name" : "ニコニコ動画検索info",
 "url" : "https://nicosearch.info/",
 "sameAs" : [
   "https://twitter.com/dhiki_pico",
   "https://www.instagram.com/nicosearchinfo/?hl=ja"
   ]
}
</script>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
 <script src="js/bootstrap.min.js"></script>


 <script src="js/include.js"></script>
 <body>
   <?php
   require('videoinfo_func.php');
   require('navbar/nicovideo_navbar.php');
    ?>

   <?php $select = 0; navbar($select); ?>
   <div class="container" style="padding:20px">
<center>
 	<h1>ニコニコ動画検索info</h1>
   ニコニコ動画の動画を検索できるサイトです<br>
   ※PCからアクセスされた方は上のバーから利用してください<br>
   ※スマートフォンからアクセスされた方は右上のアイコンから利用してください<br><br>

   <p><a href="./history.php">更新履歴</a></p>
   <section itemscope itemtype="http://schema.org/Search">
   <h2>・主な検索機能</h2>
    <b><a itemprop="nicokeywordtagssearch" href="./nicokeywordtags-search.php">キーワード・タグ</a></b>：ニコニコ動画のキーワード・タグ検索に相当します。<br>
    <b><a itemprop="nicoinfosearch" href="./nicoinfosearch.php">動画詳細</a></b>：当サイトで動画の視聴可能、ページ内には動画に関連した便利な被リンクも搭載しております。<br>
    <b><a itemprop="niconewvideo" href="./niconewvideo.php">新着動画</a></b>：各カテゴリタグに新しく投稿された動画を検索します。<br>
    <b><a itemprop="nicocomesearch" href="./nicocomesearch.php">コメント</a></b>：動画IDを入力すると、投稿されたコメントを最大1000件まで確認可能になります。<br>
    <b><a itemprop="nicorand" href="./nicorand.php">ランダム</a></b>：今までニコニコ動画に投稿された動画から10件ランダムで検索します。<br>
    <b><a itemprop="nicokmsearch" href="./nicokmsearch.php">過去・未来</a></b>：動画IDを入力すると、その動画の前後に投稿された動画を確認可能です。<br>
    <b><a itemprop="ranking" href="./ranking/">ランキング</a></b>：運営者が統計したニコニコ動画の動画全体のデータから、ランキング動画を作成しております。<br><br>
   </section>
   <section itemscope itemtype="http://schema.org/Backlink">
   <h2>・動画の便利な被リンク</h2>
    <b itemprop="nicochart">チャート</b>：<a href="http://www.nicochart.jp/">ニコニコチャート</a>様のリンクからデイリーポイント・現在ランキングが確認可能になります。<br>
    <b itemprop="postedvideo">投稿動画</b>：投稿者ユーザが過去に投稿した動画を見れるページに移動可能になります。(再生数の多い順で表示されます)<br>
    <b itemprop="nicoad">ニコニ広告</b>：動画についた広告の確認,自ら広告をすることも可能です。<br>
    <b itemprop="twitter">Twitter</b>：Twitterにて動画が共有されているリツイートがあるか確認可能です。<br><br>
   </section>

  
</center>

</div>

</body>

<center>
  <div class='col-sm-12 col-xs-12' style='background:black;'></div>

  <script type="text/javascript" >
     include("copyright/copyright.html");
  </script>

</center>

</html>
