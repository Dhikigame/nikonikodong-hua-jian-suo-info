<?php
/* ランキングデータ情報取得 */ 
require('db/db_search.php');
$rank = db_select('db/VOICEROIDtheater.db');

require('../pagination/pagination.php');
$rank_pagination = isset($_GET['rank_pagination']) ? $_GET['rank_pagination'] : null; //GETでキーワードを受け取る
$meta_url = "https://nicosearch.info/ranking/voiceroidtheater.php?rank_pagination=".$rank_pagination;
?>

<!DOCTYPE html>

<html lang="ja">
  <head>
    <meta charset="utf-8">
      <title>ニコニコ動画検索info 投稿者ランキング(VOICEROID劇場編)</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <?php echo '<meta property="og:url" content="'.$meta_url.'" />'; ?>
        <meta property="og:type" content="website" />
        <meta property="og:title" content="ニコニコ動画検索info 投稿者ランキング(VOICEROID劇場編)" />
        <meta property="og:description" content="ニコニコ動画に投稿された動画のコメント,タグ,投稿者ランキング,新着動画,フォロー・フォロワー等の詳細な検索ができるサイトです。" />
        <meta property="og:site_name" content="ニコニコ動画検索info" />
        <meta property="og:image" content="https://nicosearch.info/image/site-title.png" />

        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content="@dhiki_pico" />
        <meta name="twitter:title" content="ニコニコ動画検索info 投稿者ランキング(VOICEROID劇場編)" />
        <?php echo '<meta name="twitter:url" content="'.$meta_url.'" />'; ?>
        <meta name="twitter:description" content="ニコニコ動画に投稿された動画のコメント,タグ,投稿者ランキング,新着動画,フォロー・フォロワー等の詳細な検索ができるサイトです。" />
        <meta name="twitter:image" content="https://nicosearch.info/image/site-title.png" />

          <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
            <style type="text/css">
            @font-face {
                font-family: 'GenJyuuGothic';
                src: url('../fonts/genjyuugothic-20150607/GenJyuuGothic-Medium.ttf');
            }
            body{
                font-family: 'GenJyuuGothic',sans-serif;
                /* table-layout: fixed; */
                /* text-align: center; */
            }
            p{
              font-size: 20px;
            }
            p.video_num{
              padding-top: 20px;
              font-size: 40px;
            }
            span.video_master{
              padding-top: 10px;
              font-size: 20px;
              font-weight: bold; 
            }
            p.point{
              padding-top: 30px;
              font-size: 50px;
            }
            p.point_smartphone{
              padding-top: 30px;
              font-size: 30px;
            }
            img {
              zoom: 150%;
            }
            button {
              -webkit-tap-highlight-color:rgba(0,0,0,0);
            }
            .rank {
              padding-top: 30px;
              font-size: 60px;
              font-weight: bold; 
            }
            #top{
              position: fixed;
              bottom: 20px;
              right: 20px;
              cursor:pointer;
            }
            #top-sp{
              position: fixed;
              bottom: 20px;
              right: 0px;
              cursor:pointer;
            }
            .color-sort {
              color: #0066FF;
            }

            .breadcrumbs{
              margin-top: 10px;
              background: #e9edf5;
            }
            .breadcrumb_1 {
              display: inline;
            }
            .breadcrumb_2 {
              display: inline;
            }
            .breadcrumb_3 {
              display: inline;
            }
            .breadcrumbs li::after{
              content: '>>';
              display: inline-block;
              padding: 0 0px; /*左右に余白*/
            }
            .breadcrumbs li:last-child::after{
              display: none;
            }
            </style>
            <script type="application/ld+json">
            {
              "@context" : "http://schema.org",
              "@type" : "WebPage",
              "name" : "ニコニコ動画検索info",
            "url" : "https://nicosearch.info/",
            "sameAs" : [
              "https://twitter.com/dhiki_pico",
              "https://www.instagram.com/nicosearchinfo/?hl=ja"
              ]
            }
            </script>
            <script>
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

              ga('create', 'UA-85902327-1', 'auto');
              ga('send', 'pageview');

            </script>
              <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
              <script src="../js/jquery.tablesorter.min.js"></script>
              <script src="../js/bootstrap.min.js"></script>

              <!-- スクロール関連スクリプト -->
              <script src="../js/scroll.js"></script>
              <script src="../js/include.js"></script>
  </head>
  <body ontouchstart="">
    <?php
    require('../videoinfo_func.php');
    require('../navbar/nicovideo_navbar.php');
    require('../search_condition.php');
     ?>
<a name="up"></a>

<?php 
    $select = 7; navbar_ranking($select); 
?>

    <ul itemscope itemtype="https://schema.org/BreadcrumbList" class="breadcrumbs">
      <!-- 1つめ -->
      <li itemprop="itemListElement" itemscope
          itemtype="https://schema.org/ListItem" class="breadcrumb_1">
        <a itemprop="item" href="https://nicosearch.info/">
            <span itemprop="name">ニコニコ動画検索info トップ</span>
        </a>
        <meta itemprop="position" content="1" />
      </li>

      <!-- 2つめ -->
      <li itemprop="itemListElement" itemscope
          itemtype="https://schema.org/ListItem" class="breadcrumb_2">
        <a itemprop="item" href="https://nicosearch.info/ranking/">
            <span itemprop="name">ランキングトップ</span>
        </a>
        <meta itemprop="position" content="2" />
      </li>

      <!-- 3つめ -->
      <li itemprop="itemListElement" itemscope
          itemtype="https://schema.org/ListItem" class="breadcrumb_3">
        <a itemprop="item" href="https://nicosearch.info/ranking/voiceroidtheater.php">
            <span itemprop="name">投稿者ランキング(VOICEROID劇場編)</span>
        </a>
        <meta itemprop="position" content="3" />
      </li>
    </ul>

<?php   
    // ページの一番上に戻るボタン
    $ua = $_SERVER['HTTP_USER_AGENT'];
    if((strpos($ua,'iPhone')!==false) || (strpos($ua,'iPod')!==false) || (strpos($ua,'Android.*Mobile')!==false) || (strpos($ua,'Windows.*Phone')!==false) || (strpos($ua,'Android')!==false)) {
      // echo "<img src='image/pagetop_sp.png' id='top-sp' width='30' height='30'>";
      // echo "<span class='glyphicon glyphicon-circle-arrow-up color-sort' id='top' style='font-size: 40px'></span><img src='image/pagetop.jpg' id='top' width='30' height='30'>";
    }else{
      echo "<div class='top'>";
      echo "<img src='../image/pagetop.png' id='top' width='50' height='50'>";
      echo "</div>";
    }
?>

 <div class="container" style="padding:20px">
  <center><a href="#down" class="btn btn-primary btn-lg" role="button">一番下へ</a></center>
  <center>
   
   <?php
        if((strpos($ua,'iPhone')!==false) || (strpos($ua,'iPod')!==false) || (strpos($ua,'Android.*Mobile')!==false) || (strpos($ua,'Windows.*Phone')!==false) || (strpos($ua,'Android')!==false)) {
            echo "<h1><b>VOICEROID劇場<br>投稿者ランキング</b></h1>";  
        }else{
            echo "<h1><b>VOICEROID劇場 投稿者ランキング</b></h1>";  
        }
    ?>
  </center>
  <a href=http://www.nicovideo.jp/watch/sm36412369 target=_blank>ニコニコ</a>と<a href=https://youtu.be/2sgyiSKsJd8 target=_blank>Youtube</a>にあるランキング動画の100位までのバージョンとなっております。<br>
  ・投稿したVOICEROID劇場動画の合計総合ポイントが高い投稿者で順位付をしております。(2020/01/05まで投稿された動画が統計対象)<br>
  ・動画のタイトルまたはタグに「VOICEROID劇場」のワードが含まれている動画をVOICEROID劇場動画として統計しております。<br>
  ・また、各投稿者の投稿したVOICEROID劇場動画の中から総合ポイントの高い2作の動画を代表作としてリンクを貼らさせて頂いております。<br>
  ・何かありましたら<a href=https://twitter.com/DHIKI_pico target=_blank>Twitter</a>まで<br>
  <left>
  <b>※注意点</b><br>
  あくまでも、VOICEROID劇場のワードがタイトルとタグに含まれる動画が統計対象となっております。<br>
　投稿者様によってもっと多くのVOICEROID劇場の動画を投稿しているはずですが、タグに登録されていないため本数にカウントされていない方もいらっしゃいます。<br>
  不完全性のあるランキングとなっておりますので、参考程度に観覧をお願いします。<br><br>
  </left>
  <center>
<?php
    if($rank_pagination == null || $rank_pagination == 0){
      $rank_pagination = 0;
    }
    if($rank_pagination > 90){
      $rank_pagination = 90;
    }
    //paginationはスマホ版の時にレイアウト変更フラグつける
    $ua = $_SERVER['HTTP_USER_AGENT'];
    if((strpos($ua,'iPhone')!==false) || (strpos($ua,'iPod')!==false) || (strpos($ua,'Android.*Mobile')!==false) || (strpos($ua,'Windows.*Phone')!==false) || (strpos($ua,'Android')!==false)) {
      $sp = true;
    }else{
      $sp = false;
    }
    rank_pagination(basename(__FILE__, ".php"), $sp);

    echo "<div class='col-sm-2 col-xs-6'>順位</div>";
    echo "<div class='col-sm-2 col-xs-6'>投稿者</div>";
    echo "<div class='col-sm-4 col-xs-6'>VOICEROID劇場投稿数<br>代表作</div>";
    echo "<div class='col-sm-4 col-xs-6'>合計総合ポイント</div>";
    for($i = $rank_pagination;$i < $rank_pagination + 10;$i++){

        echo "<div class='col-sm-12 col-xs-12' style='background:black;'></div>";

        $base_id = 'http://ext.nicovideo.jp/api/getthumbinfo/' . $rank[$i][0];
        $xml = simplexml_load_file($base_id); //ニコニコAPIのxml形式のデータを読み込む(動画情報取得)
        
        /* 順位 */
        $rank_num = $i + 1;
        echo "<div class='col-sm-2 col-xs-6 rank'>";
            echo "<span>" . $rank_num . "</span>";
        echo "</div>";
        
        /* 投稿者 */
        echo "<div class='col-sm-2 col-xs-6'><center>";
            echo "<a href=http://www.nicovideo.jp/user/".$xml->thumb->user_id." target=_blank>".$xml->thumb->user_nickname."</a>";

            $user_icon_url = $xml->thumb->user_icon_url; //投稿者画像
            // ユーザのサムネイル
            require_once("../phpQuery/phpQuery-onefile.php");
            echo "<a href=http://www.nicovideo.jp/user/".$xml->thumb->user_id." target=_blank><img src=".$user_icon_url." class='img-responsive' alt='Responsive image'></a><br>";
                    // ユーザのフォロー・フォロワー数を出力(ユーザが退会しているなら「ユーザが存在しません」を出力)
                    $html = file_get_contents("http://www.nicovideo.jp/user/".$xml->thumb->user_id."");
                    // 2020/07/27更新：マイページのユーザデータをスクレイピング
                    $userinfo = phpQuery::newDocument($html)
                          ->find("#js-initial-userpage-data")
                          ->attr('data-initial-data');
                    $userinfo_json = json_decode($userinfo, true);
                    $follow_user = $userinfo_json['userDetails']['userDetails']['user']['followeeCount'];
                    $follower_user = $userinfo_json['userDetails']['userDetails']['user']['followerCount'];
                    if($follow_user !== "" && $follower_user !== ""){
                    echo "フォロー：".$follow_user."<br>";
                    echo "フォロワー：".$follower_user;
                    }else{
                        echo "<font class='waring' color='#FF4500'>フォロー・フォロワー取得不可</font>";
                    }
        echo "</div>";

        /* 投稿数 */
        echo "<div class='col-sm-4 col-xs-6'>";
            echo "<p class='video_num'>" . $rank[$i][2] . "本</p>";   
            echo "<span class='video_master'>代表作<br></span>";
            for($j = 0;$j <= 1;$j++){
                $base_id = 'http://ext.nicovideo.jp/api/getthumbinfo/' . $rank[$i][$j];
                $xml = simplexml_load_file($base_id); //ニコニコAPIのxml形式のデータを読み込む(動画情報取得)
                echo "<a href=http://www.nicovideo.jp/watch/".$xml->thumb->video_id." target=_blank>" . $xml->thumb->title . "</a><br>";
            }
        echo "</div>";

        /* 合計総合ポイント */
        echo "<div class='col-sm-4 col-xs-6'>";
        if((strpos($ua,'iPhone')!==false) || (strpos($ua,'iPod')!==false) || (strpos($ua,'Android.*Mobile')!==false) || (strpos($ua,'Windows.*Phone')!==false) || (strpos($ua,'Android')!==false)) {
            echo "<p class='point_smartphone'>" . $rank[$i][3] . " pt</p>";  
        }else{
            echo "<p class='point'>" . $rank[$i][3] . " pt</p>";  
        }
        echo "</div>";

        echo "<div class='col-sm-12 col-xs-12' style='background:black;'></div>";

    }

    rank_pagination(basename(__FILE__, ".php"), $sp);
?>
 </div>
  <a name="down"></a>
  <center><a href="#up" class="btn btn-primary btn-lg" role="button">一番上へ</a></center>
  </center>
 </body>
</html>