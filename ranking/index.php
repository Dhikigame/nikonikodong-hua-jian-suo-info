<!DOCTYPE html>

<html lang="ja">
  <head>
    <meta charset="utf-8">
      <title>ニコニコ動画検索info ランキング</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta property="og:url" content="https://nicosearch.info/ranking/" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="ニコニコ動画検索info ランキング" />
        <meta property="og:description" content="ニコニコ動画に投稿された動画のコメント,タグ,投稿者ランキング,新着動画,フォロー・フォロワー等の詳細な検索ができるサイトです。" />
        <meta property="og:site_name" content="ニコニコ動画検索info" />
        <meta property="og:image" content="https://nicosearch.info/image/site-title.png" />

        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content="@dhiki_pico" />
        <meta name="twitter:title" content="ニコニコ動画検索info ランキング" />
        <meta name="twitter:url" content="https://nicosearch.info/ranking/" />
        <meta name="twitter:description" content="ニコニコ動画に投稿された動画のコメント,タグ,投稿者ランキング,新着動画,フォロー・フォロワー等の詳細な検索ができるサイトです。" />
        <meta name="twitter:image" content="https://nicosearch.info/image/site-title.png" />

          <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
          <link href="../css/style.css" rel="stylesheet" media="screen">
            <style type="text/css">
            @font-face {
                font-family: 'GenJyuuGothic';
                src: url('../fonts/genjyuugothic-20150607/GenJyuuGothic-Medium.ttf');
            }
            body{
                font-family: 'GenJyuuGothic',sans-serif;
                /* table-layout: fixed; */
                /* text-align: center; */
            }
            p{
              font-size: 20px;
            }
            p.video_num{
              padding-top: 20px;
              font-size: 40px;
            }
            span.video_master{
              padding-top: 10px;
              font-size: 20px;
              font-weight: bold; 
            }
            p.point{
              padding-top: 30px;
              font-size: 50px;
            }
            img {
              zoom: 150%;
            }
            button {
              -webkit-tap-highlight-color:rgba(0,0,0,0);
            }
            .rank {
              padding-top: 30px;
              font-size: 60px;
              font-weight: bold; 
            }
            #top{
              position: fixed;
              bottom: 20px;
              right: 20px;
              cursor:pointer;
            }
            #top-sp{
              position: fixed;
              bottom: 20px;
              right: 0px;
              cursor:pointer;
            }
            .color-sort {
              color: #0066FF;
            }
            </style>
            <script type="application/ld+json">
            {
              "@context" : "http://schema.org",
              "@type" : "WebPage",
              "name" : "ニコニコ動画検索info",
            "url" : "https://nicosearch.info/",
            "sameAs" : [
              "https://twitter.com/dhiki_pico",
              "https://www.instagram.com/nicosearchinfo/?hl=ja"
              ]
            }
            </script>
            <script>
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

              ga('create', 'UA-85902327-1', 'auto');
              ga('send', 'pageview');

            </script>
              <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
              <script src="../js/jquery.tablesorter.min.js"></script>
              <script src="../js/bootstrap.min.js"></script>

              <!-- スクロール関連スクリプト -->
              <script src="../js/scroll.js"></script>
              <script src="../js/include.js"></script>
  </head>
  <body ontouchstart="">
    <?php
    require('../videoinfo_func.php');
    require('../navbar/nicovideo_navbar.php');
    require('../search_condition.php');
     ?>
<a name="up"></a>

<?php 
    $select = 7; navbar_ranking($select); 
?>
    <ul itemscope itemtype="https://schema.org/BreadcrumbList" class="breadcrumbs">
      <!-- 1つめ -->
      <li itemprop="itemListElement" itemscope
          itemtype="https://schema.org/ListItem" class="breadcrumb_1">
        <a itemprop="item" href="https://nicosearch.info/">
            <span itemprop="name">ニコニコ動画検索info トップ</span>
        </a>
        <meta itemprop="position" content="1" />
      </li>

      <!-- 2つめ -->
      <li itemprop="itemListElement" itemscope
          itemtype="https://schema.org/ListItem" class="breadcrumb_2">
        <a itemprop="item" href="https://nicosearch.info/ranking/">
            <span itemprop="name">ランキングトップ</span>
        </a>
        <meta itemprop="position" content="2" />
      </li>
    </ul>
<center>
<?php
    // ページの一番上に戻るボタン
    $ua = $_SERVER['HTTP_USER_AGENT'];
    if((strpos($ua,'iPhone')!==false) || (strpos($ua,'iPod')!==false) || (strpos($ua,'Android.*Mobile')!==false) || (strpos($ua,'Windows.*Phone')!==false) || (strpos($ua,'Android')!==false)) {
      // echo "<img src='image/pagetop_sp.png' id='top-sp' width='30' height='30'>";
      // echo "<span class='glyphicon glyphicon-circle-arrow-up color-sort' id='top' style='font-size: 40px'></span><img src='image/pagetop.jpg' id='top' width='30' height='30'>";
    }else{
      echo "<div class='top'>";
      echo "<img src='../image/pagetop.png' id='top' width='50' height='50'>";
      echo "</div>";
}
?>

 <div class="container" style="padding:20px">

  <h3><b><a href=MusicMAD.php>音MAD 投稿者ランキング</a></b></h3>
  公開日：2020/02/03<br>
  集計日：2020/01/05<br>
  <a href=https://www.nicovideo.jp/watch/sm36313260 target=_blank>ニコニコ</a>と<a href=https://youtu.be/0QHaETzl0WY target=_blank>Youtube</a>にあるランキング動画の100位までの統計<br>

  <h3><b><a href=voiceroidtheater.php>VOICEROID劇場 投稿者ランキング</a></b></h3>
  公開日：2020/02/11<br>
  集計日：2020/01/05<br>
  <a href=https://www.nicovideo.jp/watch/sm36363285 target=_blank>ニコニコ</a>と<a href=https://youtu.be/2sgyiSKsJd8 target=_blank>Youtube</a>にあるランキング動画の100位までの統計<br>

  <h3><b><a href=kankore.php>艦これ 投稿者ランキング</a></b></h3>
  公開日：2020/02/24<br>
  集計日：2020/01/05<br>
  <a href=https://www.nicovideo.jp/watch/sm36412369 target=_blank>ニコニコ</a>と<a href=https://youtu.be/BkLkb8vFB8w target=_blank>Youtube</a>にあるランキング動画の100位までの統計<br>

  <h3><b><a href=dance.php>踊ってみた 投稿者ランキング</a></b></h3>
  公開日：2020/03/14<br>
  集計日：2020/02/20<br>
  <a href=https://www.nicovideo.jp/watch/sm36511624 target=_blank>ニコニコ</a>と<a href=https://youtu.be/B-snPk3m0y8 target=_blank>Youtube</a>にあるランキング動画の100位までの統計<br>

  <h3><b><a href=bbsenpaitheater.php>BB先輩劇場 投稿者ランキング</a></b></h3>
  公開日：2020/03/16<br>
  集計日：2020/02/20<br>
  <a href=https://www.nicovideo.jp/watch/sm36524358 target=_blank>ニコニコ</a>と<a href=https://youtu.be/zTWLTV2zVg8 target=_blank>Youtube</a>にあるランキング動画の100位までの統計<br>

  <h3><b><a href=carvideo.php>車載動画 投稿者ランキング</a></b></h3>
  公開日：2020/05/13<br>
  集計日：2020/04/09<br>
  <a href=https://www.nicovideo.jp/watch/sm36854708 target=_blank>ニコニコ</a>と<a href=https://youtu.be/U-mj4WDMZus target=_blank>Youtube</a>にあるランキング動画の100位までの統計<br>

  <h3><b><a href=RTA.php>RTA 投稿者ランキング</a></b></h3>
  公開日：2020/06/22<br>
  集計日：2020/05/01<br>
  <a href=https://www.nicovideo.jp/watch/sm37072480 target=_blank>ニコニコ</a>と<a href=https://youtu.be/VUYKghYfjUQ target=_blank>Youtube</a>にあるランキング動画の100位までの統計<br>


  </left>
</center>
 </body>
</html>