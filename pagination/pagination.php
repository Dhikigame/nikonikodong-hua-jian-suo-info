<?php
function newvideo_pagination($offset=0, $keyword=null, $sp=false){
    $i = 0;
    echo "<center>";
    if($sp === true){
        echo "<ul class='pagination pagination-sm'>";
    }else{
        echo "<ul class='pagination pagination-lg'>";
    }

        for($i = 1; $i <= 10; $i++){
            $offset_save_sort = ($i * 10) - 10;
            if($offset == $offset_save_sort || $offset == 1){
                echo "<li class='active'><a href='#'>".$i."</a></li>";
                continue;
            }
            echo "<li><a href=./niconewvideo.php?keyword=".$keyword."&offset=".$offset_save_sort.">".$i."</a></li>";
        }
    
      echo "</ul>";
    echo "</center>";
}

function search_pagination($offset=0, $keyword=null, $field, $sort, $screen_video_num=10, $type="search", $sp=false){
    $i = 0;
    echo "<center>";
    if($sp === true){
        echo "<ul class='pagination pagination-sm'>";
    }else{
        echo "<ul class='pagination pagination-lg'>";
    }

    if($offset !== 0 || $screen_video_num > 9){
        for($i = 1; $i <= 10; $i++){
            $offset_save_sort = ($i * 10) - 10;
            if($offset == $offset_save_sort || $offset == 1){
                echo "<li class='active'><a href='#'>".$i."</a></li>";
                continue;
            }
            if($type === "search"){
                echo "<li><a href=./nicosearch.php?keyword=".$keyword."&field=".$field."&sort=".$sort."&offset=".$offset_save_sort.">".$i."</a></li>";
            }else{
                echo "<li><a href=./nicotagsearch.php?keyword=".$keyword."&field=".$field."&sort=".$sort."&offset=".$offset_save_sort.">".$i."</a></li>";
            }

        }
    }

        echo "</ul>";
    echo "</center>";
}

function comment_pagination($id, $sort, $offset=0, $comment_num=0, $sp=false){

    echo "<center>";
    if($sp === true){
        echo "<ul class='pagination pagination-sm'>";
    }else{
        echo "<ul class='pagination pagination-lg'>";
    }

    $offset = intval($offset / 100) + 1;
    $comment_num = intval((($comment_num - 1) / 100) + 1);
    if($comment_num === 0){
        $comment_num = 1;
    }
        for($i = 1; $i <= 10; $i++){

            if($offset === $i){
                echo "<li class='active'><a href='#'>".$i."</a></li>";
            }else{
                $pagination_offset = $i * 100 - 100;
                echo "<li><a href=./nicocome.php?nicoID=".$id."&sort=".$sort."&offset=".$pagination_offset.">".$i."</a></li>";
            }

            if($i === $comment_num){
                break;
            }
        }
    
        echo "</ul>";
    echo "</center>";
}

function rank_pagination($ranking_page, $sp=false){
    if($sp === true){
        echo "<ul class='pagination pagination-sm'>";
    }else{
        echo "<ul class='pagination pagination-lg'>";
    }

    for($i = 0; $i <= 90; $i=$i+10){
        $pagination_min = $i + 1;
        $pagination_max = $i + 10;
        echo "<li><a href=./".$ranking_page.".php?rank_pagination=".$i.">".$pagination_min."-".$pagination_max."位</a></li>";

    }
    echo "</ul>";
}
?>