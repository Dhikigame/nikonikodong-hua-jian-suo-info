<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
      <title>ニコニコ動画検索info ランダム検索</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta property="og:url" content="https://nicosearch.info/nicorand.php" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="ニコニコ動画検索inf ランダム検索" />
        <meta property="og:description" content="ニコニコ動画に投稿された動画のコメント,タグ,投稿者ランキング,新着動画,フォロー・フォロワー等の詳細な検索ができるサイトです。" />
        <meta property="og:site_name" content="ニコニコ動画検索info" />
        <meta property="og:image" content="https://nicosearch.info/image/site-title.png" />

        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content="@dhiki_pico" />
        <meta name="twitter:title" content="ニコニコ動画検索inf ランダム検索" />
        <meta name="twitter:url" content="https://nicosearch.info/nicorand.php" />
        <meta name="twitter:description" content="ニコニコ動画に投稿された動画のコメント,タグ,投稿者ランキング,新着動画,フォロー・フォロワー等の詳細な検索ができるサイトです。" />
        <meta name="twitter:image" content="https://nicosearch.info/image/site-title.png" />

          <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
          <link href="./css/style.css" rel="stylesheet" media="screen">
            <style type="text/css">
            @font-face {
                font-family: 'GenJyuuGothic';
                src: url('fonts/genjyuugothic-20150607/GenJyuuGothic-Medium.ttf');
            }
            body{
                font-family: 'GenJyuuGothic',sans-serif;
                /* table-layout: fixed; */
                /* text-align: center; */
            }
            p{
              font-size: 20px;
            }
            img {
              zoom: 150%;
            }
            button {
              -webkit-tap-highlight-color:rgba(0,0,0,0);
            }
            </style>
            <script type="application/ld+json">
            {
              "@context" : "http://schema.org",
              "@type" : "WebPage",
              "name" : "ニコニコ動画検索info",
            "url" : "https://nicosearch.info/",
            "sameAs" : [
              "https://twitter.com/dhiki_pico",
              "https://www.instagram.com/nicosearchinfo/?hl=ja"
              ]
            }
            </script>
            <script>
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

              ga('create', 'UA-85902327-1', 'auto');
              ga('send', 'pageview');

            </script>
              <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
              <script src="js/bootstrap.min.js"></script>

              <script src="js/include.js"></script>

  </head>
    <body>
      <?php
      require('videoinfo_func.php');
      require('navbar/nicovideo_navbar.php');
      require('search_condition.php');
       ?>
<?php

$keyword_set = "アニメ OR ゲーム OR 実況プレイ動画 OR 東方 OR アイドルマスター OR ラジオ OR 描いてみた OR
            エンターテイメント OR 音楽 OR 歌ってみた OR 演奏してみた OR 踊ってみた OR VOCALOID OR ニコニコインディーズ OR
            動物 OR 料理 OR 自然 OR 旅行 OR スポーツ OR ニコニコ動画講座 OR 車載動画 OR 歴史 OR
            科学 OR ニコニコ技術部 OR ニコニコ手芸部 OR 作ってみた OR
            政治 OR
            例のアレ OR その他 OR 日記 OR
            ファッション";

$keyword_encode = urlencode($keyword_set);
//echo $keyword_encode."<br>";

$base_key = 'https://api.search.nicovideo.jp/api/v2/snapshot/video/contents/search?q='.$keyword_encode.'&targets=tagsExact&fields=contentId&_sort='.urlencode("-").'startTime&_limit=1';
//echo $base_key."<br><br>";
$json = file_get_contents($base_key);

//echo $json."<br><br>";
//$arr = video_newone($keyword_encode);

$arr = json_decode($json);

$max_id = substr($arr->data[0]->contentId, 2);

?>
<a name="up"></a>

<?php $select = 5; navbar($select); ?>

<ul itemscope itemtype="https://schema.org/BreadcrumbList" class="breadcrumbs">
      <!-- 1つめ -->
      <li itemprop="itemListElement" itemscope
          itemtype="https://schema.org/ListItem" class="breadcrumb_1">
        <a itemprop="item" href="https://nicosearch.info/">
            <span itemprop="name">ニコニコ動画検索info トップ</span>
        </a>
        <meta itemprop="position" content="1" />
      </li>

      <!-- 2つめ -->
      <li itemprop="itemListElement" itemscope
          itemtype="https://schema.org/ListItem" class="breadcrumb_2">
        <a itemprop="item" href="https://nicosearch.info/nicorand.php">
            <span itemprop="name">ランダム検索</span>
        </a>
        <meta itemprop="position" content="2" />
      </li>
    </ul>

<div class="container" style="padding:20px">
  <center>
    <a href="#down" class="btn btn-primary btn-lg " role="button">一番下へ</a>
<p>今までに投稿された動画をランダムで10件出力します</p>

<?php
echo "<div id='loading' style='width:100%;margin:0;padding:0;height:100%;'>";
echo "<font size='6'>検索中...</font><center><img src='image/loading.gif' class='img-responsive' alt='Responsive image'></center>";
$i = 1;
while($i <= 10){
  ob_flush();       // PHPの出力バッファを強制出力します。
  flush();          // Webサーバの出力バッファを強制出力します。
  //echo ".";
  $rand_id[$i] = rand(0, $max_id);
  //echo $rand_id;
  $video_id[$i] = video_IDsearch($rand_id[$i]);

  if($video_id[$i] == 1)
    continue;

  $i++;
}
echo "</div>";
/*
echo "</ul>";
echo "</div>";
*/
 ?>
 <style>
#loading {
  display:none;
}
</style>

    <div class="container" style="padding:20px">
      <script type="text/javascript" >
         include("navbar/videoinfo.html");
      </script>

<?php
echo "<p>";
for($i = 1;$i <= 10;$i++)
video_data($rand_id[$i],$video_id[$i]);
 ?>

</div>
  <a href="#up" class="btn btn-primary btn-lg " role="button">一番上へ</a>
    </div>
  </div>
<a name="down"></a>
</center>
    </body>
</html>
