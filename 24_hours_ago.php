<?php
date_default_timezone_set('Asia/Tokyo');

//削除期限
$expire = strtotime("24 hours ago");

//ディレクトリ
$dir = dirname(__FILE__) . '/comment/';

$list = scandir($dir);
foreach($list as $value){
    $file = $dir . $value;
    if(!is_file($file)) continue;
    $mod = filemtime( $file );
    if($mod < $expire){
        //chmod($file, 0666);
        unlink($file);
    }
}
?>
