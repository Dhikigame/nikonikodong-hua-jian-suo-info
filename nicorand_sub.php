<!DOCTYPE html>
<html>
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
<meta name='viewport'
content='width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no'>
<meta name='format-detection' content='telephone=no'>
<link media='only screen and (max-device-width:480px)'
href='smart.css' type='text/css' rel='stylesheet' />
<link media='screen and (min-device-width:481px)' href='design.css'
type='text/css' rel='stylesheet' />
<!--[if IE]>
<link href='design.css' type='text/css' rel='stylesheet' />
<![endif]-->

 <head>
  <title>ニコニコ動画　ランダム動画検索</title>
</head>
  <style type="text/css">
  div{
  border-bottom: medium black solid;
  }

  p{
  font-size: 50px;
}
  </style>
  <body>
    <center>
<?php

echo"<a href='../index.html'>トップ</a><br>";
echo "<h1>ニコニコ動画　ランダム動画検索(一括var var3.0)</h1>
ニコニコ動画に今まで投稿された動画の中からランダムで検索するというものです。<br>
<a href='../nicorand.php'>更新</a>することでランダムで動画が変わります<br>
<a href='../niconicos2.html'>こちら</a>との違いは<b>消去動画や非公開動画を出さないようにしたもの</b><br>";


$keyword_set = "アニメ OR ゲーム OR 東方 OR アイドルマスター OR ラジオ OR 描いてみた OR
            エンターテイメント OR 音楽 OR 歌ってみた OR 演奏してみた OR 踊ってみた OR VOCALOID OR ニコニコインディーズ OR
            動物 OR 料理 OR 自然 OR 旅行 OR スポーツ OR ニコニコ動画講座 OR 車載動画 OR 歴史 OR
            科学 OR ニコニコ技術部 OR ニコニコ手芸部 OR 作ってみた OR
            政治 OR
            例のアレ OR その他 OR 日記 OR
            ファッション";

$keyword_encode = urlencode($keyword_set);
//echo $keyword_encode."<br>";
$base_key = 'http://api.search.nicovideo.jp/api/v2/video/contents/search?q='.$keyword_encode.'&targets=tagsExact&fields=contentId&_sort='.urlencode("-").'startTime&_limit=1';
//echo $base_key."<br><br>";
$json = file_get_contents($base_key);
//echo $json."<br><br>";
$arr = json_decode($json);

$max_id = substr($arr->data[0]->contentId, 2);
//echo $max_id."<br><br>";
/*
$rand_id = rand(0, $max_id);
echo $rand_id;
*/
/*
if($arr->data[$i]->contentId == null){
    break;
  }
*/
echo "<table border='1' cellpadding='5' width='600'>";
echo "<tr>
        <td><b>動画情報</b></td>
        <td><b>動画リンク</b></td>
      </tr>";

$i = 1;
while($i <= 10){
  $rand_id = rand(0, $max_id);
  //echo $rand_id;
        $base_id = 'http://ext.nicovideo.jp/api/getthumbinfo/sm' . $rand_id; //ニコニコAPI(動画情報取得)
        $xml = simplexml_load_file($base_id); //ニコニコAPIのxml形式のデータを読み込む(動画情報取得)

        if($xml->thumb->video_id == null){
          $base_id = 'http://ext.nicovideo.jp/api/getthumbinfo/so' . $rand_id; //ニコニコAPI(動画情報取得)
          $xml = simplexml_load_file($base_id); //ニコニコAPIのxml形式のデータを読み込む(動画情報取得)
            if($xml->thumb->video_id == null){
              continue;
            }
        }
        $id_sort = substr($xml->thumb->video_id, 0, 2);
        $i++;
        //echo $rand_id."<br>";
        //echo "<iframe width='350' height='200' src=http://ext.nicovideo.jp/thumb/".$xml->thumb->video_id."></iframe><br>";
        echo "<tr>
        <td>
        <a href=../nicoinfo_result.php?nicoID=".$rand_id." target=_blank>動画情報</a>
        <a href=../nicocome.php?nicoID=".$rand_id." target=_blank>コメント</a>
        <a href=http://www.nicovideo.jp/user/".$xml->thumb->user_id."/video target=_blank>投稿動画</a><br>
        【動画ID】".$xml->thumb->video_id."<br>";
        if($id_sort == "so"){
          echo "<center><a href=http://www.nicovideo.jp/watch/".$xml->thumb->video_id." target=_blank>動画リンク</a></center>";
        }

        echo "【投稿者】".$xml->thumb->user_nickname.
        "<br>【サムネ】<img src=".$xml->thumb->user_icon_url."><br>
        【タグ】<br><a href=../nicotagsearch.php?keyword=".$xml->thumb->tags->tag[0].">".$xml->thumb->tags->tag[0] ."</a>
                  <a href=../nicotagsearch.php?keyword=".$xml->thumb->tags->tag[1].">".$xml->thumb->tags->tag[1] ."</a>
                  <a href=../nicotagsearch.php?keyword=".$xml->thumb->tags->tag[2].">".$xml->thumb->tags->tag[2] ."</a>
                  <a href=../nicotagsearch.php?keyword=".$xml->thumb->tags->tag[3].">".$xml->thumb->tags->tag[3] ."</a>
                  <a href=../nicotagsearch.php?keyword=".$xml->thumb->tags->tag[4].">".$xml->thumb->tags->tag[4] ."</a>
        </td>
        <td><iframe width='350' height='200' src=http://ext.nicovideo.jp/thumb/".$xml->thumb->video_id."></iframe></td>
        </tr>";

}
      echo "</table>";

      echo "<br><br>";
        echo"<a href='../index.html'>トップ</a><br>";

?>

<div class = "copyright" style='font-size:8px; margin:20px;'>
Copyright &copy; 2016 Dhikigame<br>
このサイトはニコニコ動画とまったく関係はありません。<br>
更新：2016/09/18
</div>

</center>
</body>



</html>
