# ニコニコ動画検索info

ニコニコ動画の動画を検索できるサイト



A site where you can search for Nico Nico Douga

# Release

https://nicosearch.info/

# Requirement

- PHP7
- SQLite3
- HTML5
- Javascript
- Bootstrap
- niconicoAPI
  - getthumbinfo
  - スナップショット検索API v2 (https://site.nicovideo.jp/search-api-docs/snapshot)



Operating environment

- さくらVPS
- CentOS7(Web+API Server)

# Features

各ページの情報は以下の通り

### キーワード・タグ

ニコニコ動画のキーワードとタグ検索と同等に検索できる

### 動画詳細

当サイトで動画の視聴可能、動画に関連した便利な被リンクを搭載

### 新着動画

新しく投稿された動画をカテゴリタグの条件指定で検索できる

### コメント

動画IDを入力することで、最大1000件までのコメントを表示可能

### ランダム

今まで投稿された動画の中から10件検索する

### 過去・未来検索

動画IDで検索すると過去と未来にどんな動画が投稿されたか10件検索する

### ランキング

ニコニコ動画情報データベースを別サーバで運用しており、そのデータベース解析してランキング動画を作成しております。

https://www.upload.nicovideo.jp/garage/series/97621

# Author

- Dhiki(Infrastructure engineer & Video contributor)
- https://twitter.com/DHIKI_pico