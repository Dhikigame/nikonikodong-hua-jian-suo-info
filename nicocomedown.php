<!DOCTYPE html>
<html>
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
<meta name='viewport'
content='width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no'>
<meta name='format-detection' content='telephone=no'>
<link media='only screen and (max-device-width:480px)'
href='smart.css' type='text/css' rel='stylesheet' />
<link media='screen and (min-device-width:481px)' href='design.css'
type='text/css' rel='stylesheet' />
<!--[if IE]>
<link href='design.css' type='text/css' rel='stylesheet' />
<![endif]-->
<link rel='stylesheet' href='css/top.css' type='text/css'>
 <head>
  <title>ニコニコ動画　コメントサーバ</title>
  <style type="text/css">
  div{
  border-bottom: medium black solid;
  }

  p{
  font-size: 50px;
}


  </style>
  </head>
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-85902327-1', 'auto');
    ga('send', 'pageview');

  </script>
   <body>
     <center>
<?php
date_default_timezone_set('Asia/Tokyo');
$date = getdate();
$date_store = $date['year'].'年'.$date['mon'].'月'.$date['mday'].'日'.$date['hours'].'時'.$date['minutes'].'分'.$date['seconds'].'秒';


$data = array(
    "mail" => "ikuyoiiya@yahoo.co.jp",   // ニコニコ動画のログインメールアドレス
    "password" => "okawariikisu"           // ニコニコ動画のログインパスワード
);

$thread_id = null; //動画id
$ms = null; //コメントサーバのURL

$error = null;

if(!empty($_GET['nicoID']) and ctype_alnum($_GET['nicoID'])){
    $videoid = "sm".$_GET['nicoID'];            // idが空でなく英数字の場合、そのまま$videoidに代入
}else{
  //不正な動画idが入っていたら空を返却して終了
  $ret = array(
    "thread_id" => $thread_id,
    "ms" => $ms,
  );
  $json_value = json_encode($ret);
  echo $json_value;
  return;
}

$meta = stream_get_meta_data($fp = tmpfile());
// ニコニコ動画にログインする
$ch = curl_init("https://secure.nicovideo.jp/secure/login?site=niconico");  // ログイン先のURL
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_COOKIEJAR, $meta['uri']);   // 受信したCookieを{$cookieFile}に保存
curl_setopt($ch, CURLOPT_POST, TRUE);               // ログイン情報はPOSTで送信
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);        // 送信する内容$data
curl_exec($ch);                         // cURLセッションを実行
curl_close($ch);                        // cURLセッションを閉じる

// getflv APIを使って動画情報を取得する
$ch = curl_init("http://ext.nicovideo.jp/api/getflv/" . $videoid);      // getflv APIのアドレス
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_COOKIEFILE, $meta['uri']);      // $cookieFileに保存されたcookieを送信する
$getflv = curl_exec($ch);           // cURLセッションを実行し、その結果を$getflvに代入
fclose($fp);
curl_close($ch);
//echo $getflv;
//parse_str($getflv);                 // getflvで取得したクエリ文字列を変数に代入  $ms,$thread_idを使用
parse_str($getflv , $parse_result);
//echo $getflv;
if(isset($error)){
  echo"<a href='../nicocome.html'>入力画面に戻る</a><br>
    <a href='../index.html'>トップ</a><br>";
    exit("動画が見つかりません");        // $errorがnullではない場合、不正な動画IDと判断して終了
}

$thread_id = $parse_result["thread_id"];
$ms = $parse_result["ms"];

$xml = $ms.'thread?version=20090904&thread='.$thread_id.'&res_from=-1000';

//echo $xml;
$dom = new DOMDocument('1.0', 'UTF-8');
$dom->preserveWhiteSpace = false;
$dom->formatOutput = true;
$dom->load($xml);

echo "<center>";
echo"<a href='../nicocome.html'>入力画面に戻る</a><br>
    <a href='../index.html'>トップ</a><br>";
echo "<h1>動画リンク</h1>
  <iframe width='350' height='200' src=http://ext.nicovideo.jp/thumb/".$videoid."></iframe>";



$comment[0] = $dom->getElementsByTagName("chat")->item(0);
//$comment_number[0] = $comment[0]->getAttribute("no");
echo "<br><style=font-size: 150%;>".date( "Y年m月d日 H時i分s秒" )."までに投稿されたコメントを出力します<br>";



for($i = 999; $i >= 0; $i--){
$comment[$i] = $dom->getElementsByTagName("chat")->item($i);
if($comment[$i] == null){
  continue;
}
$comment_number[$i] = $comment[$i]->getAttribute("no");
$comment_value[$i] = $comment[$i]->nodeValue;
$comment_date[$i] = $comment[$i]->getAttribute("date");
$comment_date[$i] = date("Y-m-d H:i:s", $comment_date[$i]);
$comment_playtime[$i] = $comment[$i]->getAttribute("vpos");
$comment_playtime[$i] = $comment_playtime[$i] / 100; //vposは1/100秒の単位なので、秒単位に直す
$comment_playtime[$i] = date("i:s", $comment_playtime[$i]);
$comment_user_id[$i] = $comment[$i]->getAttribute("user_id");
$comment_user_id[$i] = substr($comment_user_id[$i] , 0, 10); //ユーザーIDから最初の10文字だけを抽出
}

function Excel_output_comment($videoid,$date_store,$comment_date,$comment_number,$comment_playtime,$comment_value,$comment_user_id){
  require_once(__DIR__ . "/PHPExcel_1.8.0_odt/Classes/PHPExcel.php");
  require_once(__DIR__ . "/PHPExcel_1.8.0_odt/Classes/PHPExcel/IOFactory.php");

  // エクセルを新規作成
  $excel = new PHPExcel();

  // 0番目のシートをアクティブにします(シートは0から数えます)
  // (エクセルを新規作成した時点で0番目の空のシートが作成されています)
  $excel->setActiveSheetIndex(0);

  // シートに対して何かを行うためにアクティブになっているシートを変数に入れます
  $sheet = $excel->getActiveSheet();

  // シート全体のフォント・サイズを変える
  $sheet->getDefaultStyle()->getFont()->setName( 'ＭＳ ゴシック' )->setSize(15);

  //echo $date_store;
  // シートに名前を付けます
  $sheet->setTitle($videoid.' '.$date_store);

  $sheet->setCellValue("A1", "コメント投稿日付");
  $sheet->setCellValue("B1", "コメント投稿順");
  $sheet->setCellValue("C1", "再生時間");
  $sheet->setCellValue("D1", "コメント");
  $sheet->setCellValue("E1", "ユーザーID");

  // セルに値を入力します。
  for($i = 2; $i <= 1001; $i++){
    $cell_number_A = "A" . $i;
    $cell_number_B = "B" . $i;
    $cell_number_C = "C" . $i;
    $cell_number_D = "D" . $i;
    $cell_number_E = "E" . $i;

    //echo $cell_number."<br>";
    $sheet->setCellValue($cell_number_A, $comment_date[$i-2]);
    $sheet->setCellValue($cell_number_B, $comment_number[$i-2]);
    $sheet->setCellValue($cell_number_C, $comment_playtime[$i-2]);
    $sheet->setCellValue($cell_number_D, $comment_value[$i-2]);
    $sheet->setCellValue($cell_number_E, $comment_user_id[$i-2]);

    if($comment_date[$i-1] == null){
      break;
    }
  }

  $video_info = 'http://ext.nicovideo.jp/api/getthumbinfo/'.$videoid;
  $xml = simplexml_load_file($video_info); //ニコニコAPIのxml形式のデータを読み込む(動画情報取得)


  $excel->createSheet();
  // 1番目のシートをアクティブにします(シートは0から数えます)
  $excel->setActiveSheetIndex(1);
  // シートに対して何かを行うためにアクティブになっているシートを変数に入れます
  $sheet = $excel->getActiveSheet();
  // シートに名前を付けます
  $sheet->setTitle("動画情報");

  $sheet->setCellValue("A1", "タイトル");
  $sheet->setCellValue("B1", $xml->thumb->title);
  $sheet->setCellValue("A2", "動画ID");
  $sheet->setCellValue("B2", $xml->thumb->video_id);

  //firsrt_retriereは投稿された日時を取得する
  $day = substr($xml->thumb->first_retrieve, 0, 10); //first_retrieveから日付だけを抽出
  $time = substr($xml->thumb->first_retrieve, 11, 8); //first_retrieveから時間だけを抽出
  $day_time = $day." ".$time;
  $sheet->setCellValue("A3", "投稿時間");
  $sheet->setCellValue("B3", $day_time);
  $sheet->setCellValue("A4", "説明");
  $sheet->setCellValue("B4", $xml->thumb->description);
  $sheet->setCellValue("A5", "再生数");
  $sheet->setCellValue("B5", $xml->thumb->view_counter);
  $sheet->setCellValue("A6", "コメント数");
  $sheet->setCellValue("B6", $xml->thumb->comment_num);
  $sheet->setCellValue("A7", "マイリスト数");
  $sheet->setCellValue("B7", $xml->thumb->mylist_counter);

    $sheet->setCellValue("A8", "タグ");
    $sheet->setCellValue("B8", $xml->thumb->tags->tag[0]);
    $sheet->setCellValue("B9", $xml->thumb->tags->tag[1]);
    $sheet->setCellValue("B10", $xml->thumb->tags->tag[2]);
    $sheet->setCellValue("B11", $xml->thumb->tags->tag[3]);
    $sheet->setCellValue("B12", $xml->thumb->tags->tag[4]);
    $sheet->setCellValue("B13", $xml->thumb->tags->tag[5]);
    $sheet->setCellValue("B14", $xml->thumb->tags->tag[6]);
    $sheet->setCellValue("B15", $xml->thumb->tags->tag[7]);
    $sheet->setCellValue("B16", $xml->thumb->tags->tag[8]);
    $sheet->setCellValue("B17", $xml->thumb->tags->tag[9]);
    $sheet->setCellValue("B18", $xml->thumb->tags->tag[10]);

  $sheet->setCellValue("A19", "投稿者ID");
  $sheet->setCellValue("B19", $xml->thumb->user_id);
  $sheet->setCellValue("A20", "投稿者");
  $sheet->setCellValue("B20", $xml->thumb->user_nickname);

  $sheet->setCellValue("A22", "コメント取得日時");
  $sheet->setCellValue("B22", $date_store);

  // Excel2007形式で出力する
  $writer = PHPExcel_IOFactory::createWriter($excel, "Excel2007");
  $writer->save($xml->thumb->video_id.".xlsx");

  rename("./".$xml->thumb->video_id.".xlsx", "./comment/".$xml->thumb->video_id.".xlsx");

  if($xml->thumb->video_id != null){
    echo "Excel2007の形式でダウンロードします。<br>";
    echo "<a href=./comment/".$xml->thumb->video_id.".xlsx>コメントダウンロード</a><br><br>";
    echo "もしかしてこのページを使用している人が多かったら違う動画のコメントがダウンロードされる可能性もあります<br>";
  }
}

Excel_output_comment($videoid,$date_store,$comment_date,$comment_number,$comment_playtime,$comment_value,$comment_user_id);


//unlink('comment.xlsx');
?>


</center>
</body>
</html>
