            //画像をクリック・タップしたらページの一番上に戻る処理
            $(function(){
                var topBtn = $('#top');
                topBtn.hide();
                //更新ボタンをクリックしたら、Loaderを出力
                var redoBtn = $('#redo');

                //ボタンの表示設定
                $(window).scroll(function(){
                  if($(this).scrollTop() > 200){
                    //画面を1000pxスクロールしたら、ボタンを表示する
                    topBtn.fadeIn();
                  }else{
                    //画面が1000pxより上なら、ボタンを表示しない
                    topBtn.fadeOut();
                  } 
                });

                //var loadBtn=$('#load');
                //loadBtn.hide();
                //console.log("id=",redoBtn);
                redoBtn.click(function(){
                  redoBtn.className = "ui active inline loader";
                  //$('big.redo.icon').removeClass("big redo icon").addClass("ui active inline loader");  
                  //クリックしたら、Loaderを表示する
                  //redoBtn.fadeOut();
                  //loadBtn.fadeIn();
                });

                //画像にカーソル当たったとき画像のサイズ変更
                /*
                $('#top').hover(function(){
                    $(this).animate({width:30}, 100);
                }, function(){
                    $(this).animate({width:30}, 100);
                });
                */
                //ボタンをクリックしたら、スクロールして上に戻る
                topBtn.click(function(){
                  $('body, html').animate({scrollTop: 0},200);
                });
              });


              var linkTouchStart = function(){
                thisAnchor = $(this);
                touchPos = thisAnchor.offset().top;
                moveCheck = function(){
                nowPos = thisAnchor.offset().top;
                if(touchPos == nowPos){
                thisAnchor.addClass('img-responsive');
                }
                }
                setTimeout(moveCheck,100);
                }
                var linkTouchEnd = function(){
                thisAnchor = $(this);
                hoverRemove = function(){
                thisAnchor.removeClass('img-responsive');
                }
                setTimeout(hoverRemove,500);
                }
                
                $(document).on('touchstart mousedown','a',linkTouchStart);
                $(document).on('touchend mouseup','a',linkTouchEnd);
                
                