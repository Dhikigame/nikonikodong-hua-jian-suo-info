<?php
/*
    ・検索するカテゴリタグのリンクを出力　現在検索されている項目は class=active にする
    ・検索条件を出力
*/
function search_condition($keyword_search){
      echo "<center>";
       //すべてのタグの検索
       if($keyword_search == null || $keyword_search == "全"){
          $keyword_set = "アニメ OR ゲーム OR 実況プレイ動画 OR 東方 OR アイドルマスター OR ラジオ OR 描いてみた OR TRPG OR
                          エンターテイメント OR 音楽 OR 歌ってみた OR 演奏してみた OR 踊ってみた OR VOCALOID OR ニコニコインディーズ OR ASMR OR MMD OR バーチャル OR
                          動物 OR 料理 OR 自然 OR 旅行 OR スポーツ OR ニコニコ動画講座 OR 車載動画 OR 歴史 OR 鉄道 OR
                          科学 OR ニコニコ技術部 OR ニコニコ手芸部 OR 作ってみた OR
                          政治 OR
                          例のアレ OR その他 OR 日記";

          echo "<a href=./niconewvideo.php?keyword=全><button type='button' class='btn btn-default active' data-toggle='popover'>全てのカテゴリタグ</button></a> ";
        }else {
          echo "<a href=./niconewvideo.php?keyword=全><button type='button' class='btn btn-default' data-toggle='popover'>全てのカテゴリタグ</button></a> ";
        }
      //アニメ_ゲーム_絵の検索
      echo "<div class='btn-group-vertical'>";
                 if($keyword_search == "アニメ_ゲーム_絵"){
                   $keyword_set = "アニメ OR ゲーム OR 実況プレイ動画 OR 東方 OR アイドルマスター OR ラジオ OR 描いてみた OR TRPG";
                   echo "<a href=./niconewvideo.php?keyword=アニメ_ゲーム_絵><button type='button' class='btn btn-default active' data-toggle='popover'>アニメ・ゲーム・絵</button></a> ";
                 }
                 else{
                   echo "<a href=./niconewvideo.php?keyword=アニメ_ゲーム_絵><button type='button' class='btn btn-default' data-toggle='popover'>アニメ・ゲーム・絵</button></a> ";
                 }
                 echo "<div class='btn-group'>
                         <button class='btn btn-default dropdown-toggle' data-toggle='dropdown'>
                           <i class='glyphicon glyphicon-search'></i>タグ検索<span class='caret'></span>
                         </button>
                         <ul class='dropdown-menu'>
                           <li><a href=./niconewvideo.php?keyword=アニメ>アニメ</a></li>
                           <li><a href=./niconewvideo.php?keyword=ゲーム>ゲーム</a></li>
                           <li><a href=./niconewvideo.php?keyword=実況プレイ動画>実況プレイ動画</a></li>
                           <li><a href=./niconewvideo.php?keyword=東方>東方</a></li>
                           <li><a href=./niconewvideo.php?keyword=アイドルマスター>アイドルマスター</a></li>
                           <li><a href=./niconewvideo.php?keyword=ラジオ>ラジオ</a></li>
                           <li><a href=./niconewvideo.php?keyword=描いてみた>描いてみた</a></li>
                           <li><a href=./niconewvideo.php?keyword=TRPG>TRPG</a></li>
                         </ul>
                       </div>
                     </div>";
                 if($keyword_search == "アニメ" || $keyword_search == "ゲーム" || $keyword_search == "実況プレイ動画" || $keyword_search == "東方" || $keyword_search == "アイドルマスター" || $keyword_search == "ラジオ" || $keyword_search == "描いてみた" || $keyword_search == "TRPG"){
                   $keyword_set = $keyword_search;
                 }
                 //エンタメ_音楽の検索
                 echo "<div class='btn-group-vertical'>";
                 if($keyword_search == "エンタメ_音楽"){
                   $keyword_set = "エンターテイメント OR 音楽 OR 歌ってみた OR 演奏してみた OR 踊ってみた OR VOCALOID OR ニコニコインディーズ OR ASMR OR MMD OR バーチャル";
                   echo "<a href=./niconewvideo.php?keyword=エンタメ_音楽><button type='button' class='btn btn-default active' data-toggle='popover'>エンタメ・音楽</button></a> ";
                 }
                 else{
                   echo "<a href=./niconewvideo.php?keyword=エンタメ_音楽><button type='button' class='btn btn-default' data-toggle='popover'>エンタメ・音楽</button></a> ";
                 }
                 echo "<div class='btn-group'>
                         <button class='btn btn-default dropdown-toggle' data-toggle='dropdown'>
                           <i class='glyphicon glyphicon-search'></i>タグ検索<span class='caret'></span>
                         </button>
                         <ul class='dropdown-menu'>
                           <li><a href=./niconewvideo.php?keyword=エンターテイメント>エンターテイメント</a></li>
                           <li><a href=./niconewvideo.php?keyword=音楽>音楽</a></li>
                           <li><a href=./niconewvideo.php?keyword=歌ってみた>歌ってみた</a></li>
                           <li><a href=./niconewvideo.php?keyword=演奏してみた>演奏してみた</a></li>
                           <li><a href=./niconewvideo.php?keyword=踊ってみた>踊ってみた</a></li>
                           <li><a href=./niconewvideo.php?keyword=VOCALOID>VOCALOID</a></li>
                           <li><a href=./niconewvideo.php?keyword=ニコニコインディーズ>ニコニコインディーズ</a></li>
                           <li><a href=./niconewvideo.php?keyword=ASMR>ASMR</a></li>
                           <li><a href=./niconewvideo.php?keyword=MMD>MMD</a></li>
                           <li><a href=./niconewvideo.php?keyword=バーチャル>バーチャル</a></li>
                         </ul>
                       </div>
                     </div>";
                 if($keyword_search == "エンターテイメント" || $keyword_search == "音楽" || $keyword_search == "歌ってみた" || $keyword_search == "演奏してみた" || $keyword_search == "踊ってみた" || $keyword_search == "VOCALOID"
                 || $keyword_search == "ニコニコインディーズ" || $keyword_search == "ASMR" || $keyword_search == "MMD" || $keyword_search == "バーチャル"){
                   $keyword_set = $keyword_search;
                 }
                 //生活_一般_スポの検索
                 echo "<div class='btn-group-vertical'>";
                 if($keyword_search == "生活_一般_スポ"){
                   $keyword_set = "動物 OR 料理 OR 自然 OR 旅行 OR スポーツ OR ニコニコ動画講座 OR 車載動画 OR 歴史 OR 鉄道";
                   echo "<a href=./niconewvideo.php?keyword=生活_一般_スポ><button type='button' class='btn btn-default active' data-toggle='popover'>生活・一般・スポ</button></a> ";
                 }
                 else{
                   echo "<a href=./niconewvideo.php?keyword=生活_一般_スポ><button type='button' class='btn btn-default' data-toggle='popover'>生活・一般・スポ</button></a> ";
                 }
                 echo "<div class='btn-group'>
                         <button class='btn btn-default dropdown-toggle' data-toggle='dropdown'>
                           <i class='glyphicon glyphicon-search'></i>タグ検索<span class='caret'></span>
                         </button>
                         <ul class='dropdown-menu'>
                           <li><a href=./niconewvideo.php?keyword=動物>動物</a></li>
                           <li><a href=./niconewvideo.php?keyword=料理>料理</a></li>
                           <li><a href=./niconewvideo.php?keyword=自然>自然</a></li>
                           <li><a href=./niconewvideo.php?keyword=旅行>旅行</a></li>
                           <li><a href=./niconewvideo.php?keyword=スポーツ>スポーツ</a></li>
                           <li><a href=./niconewvideo.php?keyword=ニコニコ動画講座>ニコニコ動画講座</a></li>
                           <li><a href=./niconewvideo.php?keyword=車載動画>車載動画</a></li>
                           <li><a href=./niconewvideo.php?keyword=歴史>歴史</a></li>
                           <li><a href=./niconewvideo.php?keyword=鉄道>鉄道</a></li>
                         </ul>
                       </div>
                     </div>";
                 if($keyword_search == "動物" || $keyword_search == "料理" || $keyword_search == "自然" || $keyword_search == "旅行" || $keyword_search == "スポーツ" || $keyword_search == "ニコニコ動画講座" || $keyword_search == "車載動画" || $keyword_search == "歴史" || $keyword_search == "鉄道"){
                   $keyword_set = $keyword_search;
                 }
                 //科学_技術の検索
                 echo "<div class='btn-group-vertical'>";
                 if($keyword_search == "科学_技術"){
                   $keyword_set = "科学 OR ニコニコ技術部 OR ニコニコ手芸部 OR 作ってみた";
                   echo "<a href=./niconewvideo.php?keyword=科学_技術><button type='button' class='btn btn-default active' data-toggle='popover'>  科学・技術  </button></a> ";
                 }
                 else{
                   echo "<a href=./niconewvideo.php?keyword=科学_技術><button type='button' class='btn btn-default' data-toggle='popover'>  科学・技術  </button></a> ";
                 }
                 echo "<div class='btn-group'>
                         <button class='btn btn-default dropdown-toggle' data-toggle='dropdown'>
                           <i class='glyphicon glyphicon-search'></i>タグ検索<span class='caret'></span>
                         </button>
                         <ul class='dropdown-menu'>
                           <li><a href=./niconewvideo.php?keyword=科学>科学</a></li>
                           <li><a href=./niconewvideo.php?keyword=ニコニコ技術部>ニコニコ技術部</a></li>
                           <li><a href=./niconewvideo.php?keyword=ニコニコ手芸部>ニコニコ手芸部</a></li>
                           <li><a href=./niconewvideo.php?keyword=作ってみた>作ってみた</a></li>
                         </ul>
                       </div>
                     </div>";
                 if($keyword_search == "科学" || $keyword_search == "ニコニコ技術部" || $keyword_search == "ニコニコ手芸部" || $keyword_search == "作ってみた"){
                   $keyword_set = $keyword_search;
                 }
                 //政治の検索
                 if($keyword_search == "政治"){
                   $keyword_set = "政治";
                   echo "<a href=./niconewvideo.php?keyword=政治><button type='button' class='btn btn-default active' data-toggle='popover'>政治</button></a> ";
                 }
                 else{
                   echo "<a href=./niconewvideo.php?keyword=政治><button type='button' class='btn btn-default' data-toggle='popover'>政治</button></a> ";
                 }
                 //その他の検索
                 echo "<div class='btn-group-vertical'>";
                 if($keyword_search == "その他_例のアレ_日記"){
                   $keyword_set = "例のアレ OR その他 OR 日記";
                   echo "<a href=./niconewvideo.php?keyword=その他_例のアレ_日記><button type='button' class='btn btn-default active' data-toggle='popover'>その他</button></a> ";
                 }
                 else{
                   echo "<a href=./niconewvideo.php?keyword=その他_例のアレ_日記><button type='button' class='btn btn-default' data-toggle='popover'>その他</button></a> ";
                 }
                 echo "<div class='btn-group'>
                         <button class='btn btn-default dropdown-toggle' data-toggle='dropdown'>
                           <i class='glyphicon glyphicon-search'></i>タグ検索<span class='caret'></span>
                         </button>
                         <ul class='dropdown-menu'>
                           <li><a href=./niconewvideo.php?keyword=例のアレ>例のアレ</a></li>
                           <li><a href=./niconewvideo.php?keyword=その他>その他</a></li>
                           <li><a href=./niconewvideo.php?keyword=日記>日記</a></li>
                         </ul>
                       </div>
                     </div>";
                 if($keyword_search == "例のアレ" || $keyword_search == "その他" || $keyword_search == "日記"){
                   $keyword_set = $keyword_search;
                 }
                 echo "</center>";

       if($keyword_search == null || $keyword_search == "全"){
         echo "<center>
                 <div class='panel panel-primary'>
                   <div class='panel-heading'>
                   検索条件
                   </div>
                   <div class='panel-body'>
                   全てのカテゴリ
                   </div>
                 </div>
               </center>";
         $keyword_search = "全";
       }
       else{
         echo "<center>
                 <div class='panel panel-primary'>
                   <div class='panel-heading'>
                   検索条件
                   </div>
                   <div class='panel-body'>"
                   .$keyword_set."
                   </div>
                 </div>
               </center>";
       }

       $keyword = [
        'keyword_search' => $keyword_search,
        'keyword_set' => $keyword_set
      ];

      return $keyword;

}
/*    キーワード版
    ・検索するためのリンクを出力　現在検索されている項目は class=active にする
    ・検索条件を出力
*/
function search_keyword_condition($keyword_encode,$field,$sort){
      echo "<center>";
      //再生数のリンクを出力
        echo "<div class='btn-group-vertical'>";
          if($field == "viewCounter" && $sort == "-"){
            echo "<a href=./nicosearch.php?keyword=".$keyword_encode."&field=viewCounter&sort=->
                  <button type='button' class='btn btn-default btn-block active' data-toggle='popover'>
                  再生数が多い順</button></a> ";
          }
          else{
            echo "<a href=./nicosearch.php?keyword=".$keyword_encode."&field=viewCounter&sort=->
                  <button type='button' class='btn btn-default btn-block' data-toggle='popover'>
                  再生数が多い順</button></a> ";
          }

          if($field == "viewCounter" && $sort == "+"){
            echo "<a href=./nicosearch.php?keyword=".$keyword_encode."&field=viewCounter&sort=+>
                  <button type='button' class='btn btn-default btn-block active' data-toggle='popover'>
                  再生数が少ない順</button></a> ";
          }
          else{
            echo "<a href=./nicosearch.php?keyword=".$keyword_encode."&field=viewCounter&sort=+>
                  <button type='button' class='btn btn-default btn-block' data-toggle='popover'>
                  再生数が少ない順</button></a> ";
          }
        echo "</div>";
      //コメント数のリンクを出力
        echo "<div class='btn-group-vertical'>";
          if($field == "commentCounter" && $sort == "-"){
            echo "<a href=./nicosearch.php?keyword=".$keyword_encode."&field=commentCounter&sort=->
                  <button type='button' class='btn btn-default btn-block active' data-toggle='popover'>
                  コメント数が多い順</button></a> ";
          }
          else{
            echo "<a href=./nicosearch.php?keyword=".$keyword_encode."&field=commentCounter&sort=->
                  <button type='button' class='btn btn-default btn-block' data-toggle='popover'>
                  コメント数が多い順</button></a> ";
          }

          if($field == "commentCounter" && $sort == "+"){
            echo "<a href=./nicosearch.php?keyword=".$keyword_encode."&field=commentCounter&sort=+>
                  <button type='button' class='btn btn-default btn-block active' data-toggle='popover'>
                  コメント数が少ない順</button></a> ";
          }
          else{
            echo "<a href=./nicosearch.php?keyword=".$keyword_encode."&field=commentCounter&sort=+>
                  <button type='button' class='btn btn-default btn-block' data-toggle='popover'>
                  コメント数が少ない順</button></a> ";
          }
        echo "</div>";
      //マイリスト数のリンクを出力
          echo "<div class='btn-group-vertical'>";
            if($field == "mylistCounter" && $sort == "-"){
              echo "<a href=./nicosearch.php?keyword=".$keyword_encode."&field=mylistCounter&sort=->
                    <button type='button' class='btn btn-default btn-block active' data-toggle='popover'>
                    マイリスト数が多い順</button></a> ";
            }
            else{
              echo "<a href=./nicosearch.php?keyword=".$keyword_encode."&field=mylistCounter&sort=->
                    <button type='button' class='btn btn-default btn-block' data-toggle='popover'>
                    マイリスト数が多い順</button></a> ";
            }

            if($field == "mylistCounter" && $sort == "+"){
              echo "<a href=./nicosearch.php?keyword=".$keyword_encode."&field=mylistCounter&sort=+>
                    <button type='button' class='btn btn-default btn-block active' data-toggle='popover'>
                    マイリスト数が少ない順</button></a> ";
            }
            else{
              echo "<a href=./nicosearch.php?keyword=".$keyword_encode."&field=mylistCounter&sort=+>
                    <button type='button' class='btn btn-default btn-block' data-toggle='popover'>
                    マイリスト数が少ない順</button></a> ";
            }
          echo "</div>";
        //投稿日時のリンクを出力
              echo "<div class='btn-group-vertical'>";
                if($field == "startTime" && $sort == "-"){
                  echo "<a href=./nicosearch.php?keyword=".$keyword_encode."&field=startTime&sort=->
                        <button type='button' class='btn btn-default btn-block active' data-toggle='popover'>
                        投稿日時が新しい順</button></a> ";
                }
                else{
                  echo "<a href=./nicosearch.php?keyword=".$keyword_encode."&field=startTime&sort=->
                        <button type='button' class='btn btn-default btn-block' data-toggle='popover'>
                        投稿日時が新しい順</button></a> ";
                }

                if($field == "startTime" && $sort == "+"){
                  echo "<a href=./nicosearch.php?keyword=".$keyword_encode."&field=startTime&sort=+>
                        <button type='button' class='btn btn-default btn-block active' data-toggle='popover'>
                        投稿日時が古い順</button></a> ";
                }
                else{
                  echo "<a href=./nicosearch.php?keyword=".$keyword_encode."&field=startTime&sort=+>
                        <button type='button' class='btn btn-default btn-block' data-toggle='popover'>
                        投稿日時が古い順</button></a> ";
                }
              echo "</div>";
      echo "</center>";
        //検索条件を出力 キーワードはエンコードされているのででコードする
      $keyword_encode  = urldecode($keyword_encode);
      if($keyword_encode != null){
        echo "<center>
                <div class='panel panel-primary'>
                  <div class='panel-heading'>
                  検索条件
                  </div>
                  <div class='panel-body'>
                  <p>検索キーワード[".$keyword_encode."]</p>";

                  echo "<p>並び替え";

                      if($field == "viewCounter" && $sort == "-"){
                        echo '[再生数が多い順]';
                      }
                      if($field == "commentCounter" && $sort == "-"){
                        echo '[コメント数が多い順]';
                      }
                      if($field == "mylistCounter" && $sort == "-"){
                        echo '[マイリスト多い順]';
                      }
                      if($field == "startTime" && $sort == "-"){
                        echo '[投稿日時が新しい順]';
                      }
                      if($field == "viewCounter" && $sort == "+"){
                        echo '[再生数が少ない順]';
                      }
                      if($field == "commentCounter" && $sort == "+"){
                        echo '[コメント数が少ない順]';
                      }
                      if($field == "mylistCounter" && $sort == "+"){
                        echo '[マイリスト少ない順]';
                      }
                      if($field == "startTime" && $sort == "+"){
                        echo '[投稿日時が古い順]';
                      }

        echo "      </p>
                  </div>
                </div>
              </center>";
      }
}
/*    タグ版
    ・検索するためのリンクを出力　現在検索されている項目は class=active にする
    ・検索条件を出力
*/
function search_tag_condition($keyword_encode,$field,$sort){
      echo "<center>";
      //再生数のリンクを出力
        echo "<div class='btn-group-vertical'>";
          if($field == "viewCounter" && $sort == "-"){
            echo "<a href=./nicotagsearch.php?keyword=".$keyword_encode."&field=viewCounter&sort=->
                  <button type='button' class='btn btn-default btn-block active' data-toggle='popover'>
                  再生数が多い順</button></a> ";
          }
          else{
            echo "<a href=./nicotagsearch.php?keyword=".$keyword_encode."&field=viewCounter&sort=->
                  <button type='button' class='btn btn-default btn-block' data-toggle='popover'>
                  再生数が多い順</button></a> ";
          }

          if($field == "viewCounter" && $sort == "+"){
            echo "<a href=./nicotagsearch.php?keyword=".$keyword_encode."&field=viewCounter&sort=+>
                  <button type='button' class='btn btn-default btn-block active' data-toggle='popover'>
                  再生数が少ない順</button></a> ";
          }
          else{
            echo "<a href=./nicotagsearch.php?keyword=".$keyword_encode."&field=viewCounter&sort=+>
                  <button type='button' class='btn btn-default btn-block' data-toggle='popover'>
                  再生数が少ない順</button></a> ";
          }
        echo "</div>";
      //コメント数のリンクを出力
        echo "<div class='btn-group-vertical'>";
          if($field == "commentCounter" && $sort == "-"){
            echo "<a href=./nicotagsearch.php?keyword=".$keyword_encode."&field=commentCounter&sort=->
                  <button type='button' class='btn btn-default btn-block active' data-toggle='popover'>
                  コメント数が多い順</button></a> ";
          }
          else{
            echo "<a href=./nicotagsearch.php?keyword=".$keyword_encode."&field=commentCounter&sort=->
                  <button type='button' class='btn btn-default btn-block' data-toggle='popover'>
                  コメント数が多い順</button></a> ";
          }

          if($field == "commentCounter" && $sort == "+"){
            echo "<a href=./nicotagsearch.php?keyword=".$keyword_encode."&field=commentCounter&sort=+>
                  <button type='button' class='btn btn-default btn-block active' data-toggle='popover'>
                  コメント数が少ない順</button></a> ";
          }
          else{
            echo "<a href=./nicotagsearch.php?keyword=".$keyword_encode."&field=commentCounter&sort=+>
                  <button type='button' class='btn btn-default btn-block' data-toggle='popover'>
                  コメント数が少ない順</button></a> ";
          }
        echo "</div>";
      //マイリスト数のリンクを出力
          echo "<div class='btn-group-vertical'>";
            if($field == "mylistCounter" && $sort == "-"){
              echo "<a href=./nicotagsearch.php?keyword=".$keyword_encode."&field=mylistCounter&sort=->
                    <button type='button' class='btn btn-default btn-block active' data-toggle='popover'>
                    マイリスト数が多い順</button></a> ";
            }
            else{
              echo "<a href=./nicotagsearch.php?keyword=".$keyword_encode."&field=mylistCounter&sort=->
                    <button type='button' class='btn btn-default btn-block' data-toggle='popover'>
                    マイリスト数が多い順</button></a> ";
            }

            if($field == "mylistCounter" && $sort == "+"){
              echo "<a href=./nicotagsearch.php?keyword=".$keyword_encode."&field=mylistCounter&sort=+>
                    <button type='button' class='btn btn-default btn-block active' data-toggle='popover'>
                    マイリスト数が少ない順</button></a> ";
            }
            else{
              echo "<a href=./nicotagsearch.php?keyword=".$keyword_encode."&field=mylistCounter&sort=+>
                    <button type='button' class='btn btn-default btn-block' data-toggle='popover'>
                    マイリスト数が少ない順</button></a> ";
            }
          echo "</div>";
        //投稿日時のリンクを出力
              echo "<div class='btn-group-vertical'>";
                if($field == "startTime" && $sort == "-"){
                  echo "<a href=./nicotagsearch.php?keyword=".$keyword_encode."&field=startTime&sort=->
                        <button type='button' class='btn btn-default btn-block active' data-toggle='popover'>
                        投稿日時が新しい順</button></a> ";
                }
                else{
                  echo "<a href=./nicotagsearch.php?keyword=".$keyword_encode."&field=startTime&sort=->
                        <button type='button' class='btn btn-default btn-block' data-toggle='popover'>
                        投稿日時が新しい順</button></a> ";
                }

                if($field == "startTime" && $sort == "+"){
                  echo "<a href=./nicotagsearch.php?keyword=".$keyword_encode."&field=startTime&sort=+>
                        <button type='button' class='btn btn-default btn-block active' data-toggle='popover'>
                        投稿日時が古い順</button></a> ";
                }
                else{
                  echo "<a href=./nicotagsearch.php?keyword=".$keyword_encode."&field=startTime&sort=+>
                        <button type='button' class='btn btn-default btn-block' data-toggle='popover'>
                        投稿日時が古い順</button></a> ";
                }
              echo "</div>";
      echo "</center>";
        //検索条件を出力 キーワードはエンコードされているのでデコードする
      $keyword_encode  = urldecode($keyword_encode);
      if($keyword_encode != null){
        echo "<center>
                <div class='panel panel-primary'>
                  <div class='panel-heading'>
                  検索条件
                  </div>
                  <div class='panel-body'>
                  <p>検索タグ[".$keyword_encode."]</p>";

                  echo "<p>並び替え";

                      if($field == "viewCounter" && $sort == "-"){
                        echo '[再生数が多い順]';
                      }
                      if($field == "commentCounter" && $sort == "-"){
                        echo '[コメント数が多い順]';
                      }
                      if($field == "mylistCounter" && $sort == "-"){
                        echo '[マイリスト多い順]';
                      }
                      if($field == "startTime" && $sort == "-"){
                        echo '[投稿日時が新しい順]';
                      }
                      if($field == "viewCounter" && $sort == "+"){
                        echo '[再生数が少ない順]';
                      }
                      if($field == "commentCounter" && $sort == "+"){
                        echo '[コメント数が少ない順]';
                      }
                      if($field == "mylistCounter" && $sort == "+"){
                        echo '[マイリスト少ない順]';
                      }
                      if($field == "startTime" && $sort == "+"){
                        echo '[投稿日時が古い順]';
                      }

        echo "      </p>
                  </div>
                </div>
              </center>";
      }
}
?>
