<?php
echo "
<style type='text/css'>
p{
  font-size: 20px;
}
.category_tag{
  background: #ffa07a;
}
.lock_tag{
  background: #fffacd;
}
.tag{
  background: #ffffff;
}
.img-responsive:hover{
  opacity: 0.8;
  cursor: pointer;
}
.img-responsive:active{
  opacity: 1.0;
}
.waring{
  font-size: 10px;
}
</style>";
echo "<script src='js/scroll.js'></script>";
/*
    動画の情報(動画情報、投稿者、タグ、再生数・コメント数・マイリスト数・サムネイル)それぞれ出力
*/
function video_data($id,$id_sort){
  $base_id = 'https://ext.nicovideo.jp/api/getthumbinfo/' . $id_sort . $id; //ニコニコAPI(動画情報取得)
  $xml = simplexml_load_file($base_id); //ニコニコAPIのxml形式のデータを読み込む(動画情報取得)
  $ua = $_SERVER['HTTP_USER_AGENT'];

    echo '<article itemscope itemtype="http://schema.org/VideoObject">';
      echo " <div class='col-sm-12 col-xs-12 lead'><a href=https://www.google.co.jp/search?q=".$xml->thumb->video_id." target=_blank>".$xml->thumb->video_id."</a>  <a itemprop='contentUrl' href='https://www.nicovideo.jp/watch/" . $id_sort . $id . "' target=_blank><span itemprop='name'>".$xml->thumb->title."</span></a></div>";

      echo " <div class='col-sm-1 col-xs-3'>
              <button class='btn btn-default btn-sm dropdown-toggle' data-toggle='dropdown'>
                動画情報<span class='caret'></span>
              </button>
                 <ul class='dropdown-menu'>
                  <li><a href=./nicoinforesult.php?nicoID=".$id.">動画情報</a></li>";

        if($id_sort == "sm" || $id_sort == "nm") {
            echo "<li><a href=./nicocome.php?nicoID=".$id.">コメント</a></li>";
          if((strpos($ua,'iPhone')!==false)||(strpos($ua,'iPod')!==false)||(strpos($ua,'Android')!==false)) {
            echo "<li><a href=https://sp.nicovideo.jp/user/".$xml->thumb->user_id."/video?sortKey=viewCount&sortOrder=desc target=_blank>投稿動画</a></li>";
          } else {
            echo "<li><a href=https://www.nicovideo.jp/user/".$xml->thumb->user_id."/video?sortKey=viewCount&sortOrder=desc target=_blank>投稿動画</a></li>";
          }
        }
        else if($id_sort == "so"){
             echo "<li><a href=https://ch.nicovideo.jp/ch".$xml->thumb->ch_id." target=_blank>投稿動画</a></li>";
        }
            echo "<li><a href=https://www.nicovideo.jp/search/".$xml->thumb->video_id." target=_blank>関連動画</a></li>
                  <li><a href=https://nicoad.nicovideo.jp/video/publish/".$xml->thumb->video_id." target=_blank>ニコニ広告</a></li>
                  <li><a href=http://www.nicochart.jp/watch/".$xml->thumb->video_id." target=_blank>チャート</a></li>
                  <li><a href=https://twitter.com/search?q=".$xml->thumb->video_id." target=_blank>Twitter</a></li>
                </ul>
            </div>";

      echo " <div class='col-sm-1 col-xs-3'>
              <button class='btn btn-default btn-sm dropdown-toggle' data-toggle='dropdown'>
                タグ一覧<span class='caret'></span>
              </button>";
               echo "<ul class='dropdown-menu'>";
                for($tag_count = 0; $tag_count <= 10; $tag_count++){
                  if($xml->thumb->tags->tag[$tag_count] == null){
                    break;
                  }
                  if($xml->thumb->tags->tag[$tag_count]['category'] == 1){
                    echo "<li class='category_tag'><a href=./nicotagsearch.php?keyword=".$xml->thumb->tags->tag[$tag_count].">".$xml->thumb->tags->tag[$tag_count] ."</button></a>";
                    continue;
                  }
                  if($xml->thumb->tags->tag[$tag_count]['lock'] == 1){
                    echo "<li class='lock_tag'><a href=./nicotagsearch.php?keyword=".$xml->thumb->tags->tag[$tag_count].">".$xml->thumb->tags->tag[$tag_count] ."</button></a>";
                    continue;
                  }
                  echo "<li class='tag'><a href=./nicotagsearch.php?keyword=".$xml->thumb->tags->tag[$tag_count].">".$xml->thumb->tags->tag[$tag_count] ."</a></li>";
                }
          echo "</ul>
            </div>";

        if($id_sort == "sm" || $id_sort == "nm"){
          if((strpos($ua,'iPhone')!==false)||(strpos($ua,'iPod')!==false)||(strpos($ua,'Android')!==false)) {
            echo " <div class='col-sm-2 col-xs-6'><center>
            <a href=https://sp.nicovideo.jp/user/".$xml->thumb->user_id." target=_blank>"
            .$xml->thumb->user_nickname."<br></a>";
          } else {
            echo " <div class='col-sm-2 col-xs-6'><center>
            <a href=https://www.nicovideo.jp/user/".$xml->thumb->user_id." target=_blank>"
            .$xml->thumb->user_nickname."<br></a>";
          }
        }
        else if($id_sort == "so"){
          echo " <div class='col-sm-2 col-xs-6'><center>
                <a href=https://ch.nicovideo.jp/ch".$xml->thumb->ch_id." target=_blank>"
                .$xml->thumb->ch_name."<br></a>";
        }
        else{
          echo "Not found";
        }

    //画像処理(画像の解像度を縦横比を維持したまま調整する)
    $user_icon_url = $xml->thumb->user_icon_url;

      if($id_sort == "sm" || $id_sort == "nm"){
        require_once("./phpQuery/phpQuery-onefile.php");
        // ユーザのサムネイル
        if((strpos($ua,'iPhone')!==false)||(strpos($ua,'iPod')!==false)||(strpos($ua,'Android')!==false)) {
          echo "<a href=https://sp.nicovideo.jp/user/".$xml->thumb->user_id." target=_blank><img src=".$user_icon_url." class='img-responsive' alt='Responsive image'></a><br>";
        } else {
          echo "<a href=https://www.nicovideo.jp/user/".$xml->thumb->user_id." target=_blank><img src=".$user_icon_url." class='img-responsive' alt='Responsive image'></a><br>";
        }
        // ユーザのフォロー・フォロワー数を出力(ユーザが退会しているなら「ユーザが存在しません」を出力)
        $html = file_get_contents("https://www.nicovideo.jp/user/".$xml->thumb->user_id."");
        // 2020/07/27更新：マイページのユーザデータをスクレイピング
        $userinfo = phpQuery::newDocument($html)
               ->find("#js-initial-userpage-data")
               ->attr('data-initial-data');
        $userinfo_json = json_decode($userinfo, true);
        $follow_user = $userinfo_json['userDetails']['userDetails']['user']['followeeCount'];
        $follower_user = $userinfo_json['userDetails']['userDetails']['user']['followerCount'];
        if($follow_user !== "" && $follower_user !== ""){
          echo "フォロー： ".$follow_user."<br>";
          echo "フォロワー： ".$follower_user."";
        }else{
            echo "<font class='waring' color='#FF4500'>フォローフォロワー取得不可</font>";
        }
      }
      else if($id_sort == "so"){
        echo "<a href=https://ch.nicovideo.jp/ch".$xml->thumb->ch_id." target=_blank><img src=".$xml->thumb->ch_icon_url." class='img-responsive' alt='Responsive image'></a>";
      }
      else{
        echo "Not found";
      }
    echo  "</center></div>";

    echo " <div class='col-sm-2 col-xs-6'>";
      echo "<button class='btn btn-default btn-sm dropdown-toggle' data-toggle='dropdown'>
            投稿者コメント<span class='caret'></span>
            </button>
              <ul class='dropdown-menu'>
                <li itemprop='description'>".$xml->thumb->description."</li>";
        echo "</ul>
          </div>";
      echo " <div class='col-sm-1 col-xs-6'>";
        echo "<button class='btn btn-default btn-sm dropdown-toggle' data-toggle='dropdown'>
              最新コメント<span class='caret'></span>
              </button>
                <ul class='dropdown-menu'>
                  <li>".$xml->thumb->last_res_body."</li>";
          echo "</ul>
            </div>";

    //firsrt_retriereは投稿された日時を取得する
    $day = substr($xml->thumb->first_retrieve, 0, 10); //first_retrieveから日付だけを抽出
    $time = substr($xml->thumb->first_retrieve, 11, 8); //first_retrieveから時間だけを抽出
    //ニコニコAPI(動画の宣伝情報:json形式) --> $publicity_id
    $publicity_id = 'http://uad-api.nicovideo.jp/sub/1.0/UadVideoService/findAdsInfoJsonp?idvideos='. $id_sort . $id;
    $base_id = preg_replace('/.+?({.+}).+/','$1',$base_id); //$json_advのJSON形式から余分なものを抜き取る --> {} の形になり、変数を読み込めるようになる
    $publicity_xml = json_decode($base_id);

      echo "<div class='col-sm-5 col-xs-12'>";
        echo "<center>";
            echo "<span itemprop='uploadDate'>".$day." ".$time."</span><br><span>".$xml->thumb->length."</span>";
            echo "<a href=./nicoinforesult.php?nicoID=".$id."><img itemprop='thumbnailUrl' src=".$xml->thumb->thumbnail_url." class='img-responsive' alt='サムネ取得不可' width='40%' height='40%'></a>";
            echo "再生数 <span itemprop='interactionStatistic'>".$xml->thumb->view_counter."</span>";
            if($id_sort != "so") echo "コメント数 ".$xml->thumb->comment_num."";
            echo "マイリスト数 ".$xml->thumb->mylist_counter."<br>";
        echo "</center>";
      echo "</div>";
    echo '</article>';
  echo "<div class='col-sm-12 col-xs-12' style='background:black;'></div>";

}

/*
    動画の種類(sm,so,nmを判断)
*/
function video_IDsearch($id){

  $base_id = 'https://ext.nicovideo.jp/api/getthumbinfo/sm' . $id; //ニコニコAPI(動画情報取得)
  $xml = simplexml_load_file($base_id); //ニコニコAPIのxml形式のデータを読み込む(動画情報取得)
  $id_sort = substr($xml->thumb->video_id, 0, 2);

    if($id_sort != "sm"){
      $base_id = 'https://ext.nicovideo.jp/api/getthumbinfo/so' . $id; //ニコニコAPI(動画情報取得)
      $xml = simplexml_load_file($base_id); //ニコニコAPIのxml形式のデータを読み込む(動画情報取得)
      $id_sort = substr($xml->thumb->video_id, 0, 2);

        if($id_sort != "so"){
          $base_id = 'https://ext.nicovideo.jp/api/getthumbinfo/nm' . $id; //ニコニコAPI(動画情報取得)
          $xml = simplexml_load_file($base_id); //ニコニコAPIのxml形式のデータを読み込む(動画情報取得)
          $id_sort = substr($xml->thumb->video_id, 0, 2);

          if($id_sort != "nm"){
            $novideo = 1;
            return $novideo;
          }
          return $id_sort;
        }
      return $id_sort;
    }
  return $id_sort;
}
/*
    一番最新の動画の動画IDを取得
*/
function video_newone(){
  $keyword_set = "アニメ OR ゲーム OR 実況プレイ動画 OR 東方 OR アイドルマスター OR ラジオ OR 描いてみた OR
              エンターテイメント OR 音楽 OR 歌ってみた OR 演奏してみた OR 踊ってみた OR VOCALOID OR ニコニコインディーズ OR
              動物 OR 料理 OR 自然 OR 旅行 OR スポーツ OR ニコニコ動画講座 OR 車載動画 OR 歴史 OR
              科学 OR ニコニコ技術部 OR ニコニコ手芸部 OR 作ってみた OR
              政治 OR
              例のアレ OR その他 OR 日記 OR
              ファッション";

  $keyword_encode = urlencode($keyword_set);
  $base_key = 'https://api.search.nicovideo.jp/api/v2/snapshot/video/contents/search?q='.$keyword_encode.'&targets=tagsExact&fields=contentId&_sort='.urlencode("-").'startTime&_limit=1';
  $json = file_get_contents($base_key);
  $arr = json_decode($json);

  $max_id = substr($arr->data[0]->contentId, 2);

  return $max_id;

}

/*
    動画IDのjson情報を取得
*/
function video_new_json($keyword_encode,$offset){

  $base_key = 'https://api.search.nicovideo.jp/api/v2/snapshot/video/contents/search?q='.$keyword_encode.'&targets=tagsExact&fields=contentId&_sort='.urlencode("-").'startTime&_offset='.$offset.'&_limit=100';

  $json = file_get_contents($base_key);
  $arr = json_decode($json);
  
  return $arr;

}

/*
    keywordからjson情報を取得
*/
function video_keyword_json($keyword_encode,$sort,$field,$offset){

  $base_key = 'https://api.search.nicovideo.jp/api/v2/snapshot/video/contents/search?q='.$keyword_encode.'&targets=title,description,tags&fields=contentId&_sort='.urlencode($sort).$field.'&_offset='.$offset.'&_limit=100';
  $json = file_get_contents($base_key);
  $arr = json_decode($json);

  return $arr;

}
/*
    tagからjson情報を取得
*/
function video_tag_json($keyword_encode,$sort,$field,$offset){

  $base_key = 'https://api.search.nicovideo.jp/api/v2/snapshot/video/contents/search?q='.$keyword_encode.'&targets=tagsExact&fields=contentId&_sort='.urlencode($sort).$field.'&_offset='.$offset.'&_limit=100';
  $json = file_get_contents($base_key);
  $arr = json_decode($json);

  return $arr;

}

/*
    コメントを出力
*/
function comment_data($xml, $id, $video_id, $sort="latest", $offset=0){
  $dom = new DOMDocument('1.0', 'UTF-8');
  $dom->preserveWhiteSpace = false;
  $dom->formatOutput = true;
  $dom->load($xml);

  echo "<center><p><style=font-size: 150%;>".date( "Y年m月d日 H時i分s秒" )."までに投稿されたコメントを出力します</p></center>";
  echo "<div class='col-sm-12 col-xs-12' style='background:black;'></div>";
  echo "<center><p><b>対象動画</b></p></center>";

  video_data($id,$video_id);
/*
      if($comment[0] != null){
        echo "<p>※以下のコメントのダウンロードが可能です。";
        echo "<a href=../nicocomedown.php?nicoID=".$id.">ダウンロードページへ</a></p></center>";
      }
*/
//コメントの範囲を新しい方または古い方の範囲を決める
if($sort === "old"){
  $offset_start = $offset;//コメントの範囲：始め(古い方)
  $offset_end = $offset + 100; //コメントの範囲：終わり(新しい方)
}
if($sort === "long" || $sort === "short" || $sort === "latest"){
  $offset_start = 0;
  $offset_end = 1000;
}

$comment_num = 0;//表示するコメント数
  for($i = $offset_start; $i <= $offset_end; $i++){
    $comment[$comment_num] = $dom->getElementsByTagName("chat")->item($i);
    if($comment[$comment_num] === null || $i === $offset_end){
      $comment_num--;
      break;
    }
    $comment_num++;
  }
  if($sort === "latest"){
    $r_comment_num = 0;
    for($j = $comment_num; $j >= 0; $j--){
      $r_comment[$r_comment_num] = $comment[$j];
      $r_comment_num++;
    }
    require_once('nicocome/comment_offset_num.php');
    $comment = offset_diff($r_comment, $comment_num, $offset);
  }
  if($sort === "long" || $sort === "short"){
    require_once('nicocome/time_sort.php');
    $comment = time_sort($comment ,$comment_num, $sort, $offset);
  }
for($i = 0; $i < 100; $i++){

  if($comment[$i] === null){
    break;
  }
  /*
      $comment_alldata[0][] = コメント投稿順
      $comment_alldata[1][] = コメント投稿再生時間
      $comment_alldata[2][] = コメントユーザーID
      $comment_alldata[3][] = コメント内容
      $comment_alldata[4][] = コメント投稿日付
  */
  $comment_alldata[0][$i] = $comment[$i]->getAttribute("no");

  $comment_alldata[1][$i] = $comment[$i]->getAttribute("vpos");
  $comment_alldata[1][$i] = $comment_alldata[1][$i] / 100; //vposは1/100秒の単位なので、秒単位に直す
  $comment_alldata[1][$i] = date("i:s", $comment_alldata[1][$i]);

  $comment_alldata[2][$i] = $comment[$i]->getAttribute("user_id");
  $comment_alldata[2][$i] = substr($comment_alldata[2][$i] , 0, 10); //ユーザーIDから最初の10文字だけを抽出

  $comment_alldata[3][$i] = $comment[$i]->nodeValue;

  $comment_alldata[4][$i] = $comment[$i]->getAttribute("date");
  $comment_alldata[4][$i] = date("Y-m-d H:i:s", $comment_alldata[4][$i]);
}

  /* pagination */
  //paginationはスマホ版の時にレイアウト変更フラグつける
  $ua = $_SERVER['HTTP_USER_AGENT'];
  if((strpos($ua,'iPhone')!==false) || (strpos($ua,'iPod')!==false) || (strpos($ua,'Android.*Mobile')!==false) || (strpos($ua,'Windows.*Phone')!==false) || (strpos($ua,'Android')!==false)) {
    $sp = true;
  }else{
    $sp = false;
  }
  comment_pagination($id, $sort, $offset, $comment_num + 1, $sp);
      echo "<div class='col-sm-3 col-xs-2'>";
            if($sort === "long" || $sort === "short"){
              echo "<a href=./nicocome.php?nicoID=".$id."&sort=old>";
              echo "投稿順 <span class='glyphicon glyphicon-sort color-sort' style='font-size: 20px'></span>";
              echo "</a>";
            }
            if($sort === "old"){
              echo "<a href=./nicocome.php?nicoID=".$id."&sort=latest>";
              echo "投稿順 <span class='glyphicon glyphicon-chevron-up color-sort' style='font-size: 20px'></span>";
              echo "</a>";
            }
            if($sort === "latest"){
              echo "<a href=./nicocome.php?nicoID=".$id."&sort=old>";
              echo "投稿順 <span class='glyphicon glyphicon-chevron-down color-sort' style='font-size: 20px'></span>";
              echo "</a>";
            }
      echo "</div>
            <div class='col-sm-3 col-xs-2'>";
            if($sort === "old" || $sort === "latest"){
              echo "<a href=./nicocome.php?nicoID=".$id."&sort=short>";
              echo "再生時間 <span class='glyphicon glyphicon-sort color-sort' style='font-size: 20px'></span>";
              echo "</a>";
            }
            if($sort === "short"){
              echo "<a href=./nicocome.php?nicoID=".$id."&sort=long>";
              echo "再生時間 <span class='glyphicon glyphicon-chevron-up color-sort' style='font-size: 20px'></span>";
              echo "</a>";
            }
            if($sort === "long"){
              echo "<a href=./nicocome.php?nicoID=".$id."&sort=short>";
              echo "再生時間 <span class='glyphicon glyphicon-chevron-down color-sort' style='font-size: 20px'></span>";
              echo "</a>";
            }
      echo "</div>
            <div class='col-sm-3 col-xs-4'>
              ユーザーID
            </div>
            <div class='col-sm-3 col-xs-4'>
              投稿日付
            </div>
            <div class='col-sm-12 col-xs-12'>
            </div>";

      echo "<div class='col-sm-12 col-xs-12' style='background:black;'></div>
            <div class='col-sm-12 col-xs-12' style='background:black;'></div>
            <div class='col-sm-12 col-xs-12' style='background:black;'></div>";



for($i = 0; $i < 100; $i++){

  if($comment[0] === null){
    echo "<center>
            <div class='col-sm-12 col-xs-12'>
              <p>コメントが投稿されていません</p>
            </div>
          </center>";
    break;
  }

  if($comment[$i] === null){
    break;
  }
        echo "<div class='col-sm-3 col-xs-2'>"
                .$comment_alldata[0][$i].
             "</div>
              <div class='col-sm-3 col-xs-2'>"
                .$comment_alldata[1][$i].
             "</div>
              <div class='col-sm-3 col-xs-4'>"
                .$comment_alldata[2][$i].
             "</div>
              <div class='col-sm-3 col-xs-4'>"
                .$comment_alldata[4][$i].
              "</div>"; 
            if($comment_alldata[3][$i] == null)  {
              echo "<div class='col-sm-12 col-xs-12'>
                      <font color='#FF4500'>
                        コメントが消去されてます
                      </font>
                    </div>";
            }
            else{
              echo "<div class='col-sm-12 col-xs-12'><p>"
                      .$comment_alldata[3][$i].
                   "</p></div>";
            }
        echo "<div class='col-sm-12 col-xs-12' style='background:gray;'></div>";
      }
      echo '</article>';

      return $comment;
}

?>


