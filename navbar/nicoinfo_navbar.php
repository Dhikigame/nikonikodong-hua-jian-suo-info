
<?php
function navbar($select){
  echo  '<div id="header" class="container" style="padding:20px"></div>
          <nav class="navbar navbar-default navbar-fixed-top">

            <div class="navbar-header">
              <button class="navbar-toggle" data-toggle="collapse" data-target=".target">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="../index.php"><p>ニコニコ動画　検索info</p></a>
            </div>

        <div class="container">
            <div class="collapse navbar-collapse target">
              <ul class="nav navbar-nav">
                <li'; if($select == 1) {echo " class='active'";} echo '><a href="../nicosearch_search.php">キーワード・タグ検索</a></li>
                <li'; if($select == 2) {echo " class='active'";} echo '><a href="../nicoinfo_search.php">動画詳細検索</a></li>
                <li'; if($select == 3) {echo " class='active'";} echo '><a href="../niconewvideo.php">新着動画検索</a></li>
                <li'; if($select == 4) {echo " class='active'";} echo '><a href="../nicocome_search.php">コメント検索</a></li>
                <li'; if($select == 5) {echo " class='active'";} echo '><a href="../nicorand.php">ランダム検索</a></li>
                <li'; if($select == 6) {echo " class='active'";} echo '><a href="../niconicokm_search.php">過去・未来検索</a></li>
                <li>
                <form class="navbar-form" action="../nicosearch.php" method="get">
                  <div class="form-group">
                    <input type="text" name="keyword" class="form-control" maxlength="30" placeholder="keyword">
                  </div>
                    <button type="submit" class="btn btn-primary">検索</button>
                </form>
                </li>
              </ul>
            </div>
          </div>
        </nav>';
}
?>
