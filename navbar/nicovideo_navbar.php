
<?php
function navbar($select){
  echo  '<div id="header" class="container" style="padding:20px"></div>
          <nav class="navbar navbar-default navbar-fixed-top">

            <div class="navbar-header">
              <button class="navbar-toggle" data-toggle="collapse" data-target=".target">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="./"><p>ニコニコ動画　検索info</p></a>
            </div>

        <div class="container">
            <div class="collapse navbar-collapse target">
              <ul class="nav navbar-nav small">
                <li'; if($select == 1) {echo " class='active nav-item'";} echo '><a class="nav-item" href="./nicokeywordtags-search.php">キーワード・タグ</a></li>
                <li'; if($select == 2) {echo " class='active nav-item'";} echo '><a class="nav-item" href="./nicoinfosearch.php">動画詳細</a></li>
                <li'; if($select == 3) {echo " class='active nav-item'";} echo '><a class="nav-item" href="./niconewvideo.php">新着動画</a></li>
                <li'; if($select == 4) {echo " class='active nav-item'";} echo '><a class="nav-item" href="./nicocomesearch.php">コメント</a></li>
                <li'; if($select == 5) {echo " class='active nav-item'";} echo '><a class="nav-item" href="./nicorand.php">ランダム</a></li>
                <li'; if($select == 6) {echo " class='active nav-item'";} echo '><a class="nav-item" href="./nicokmsearch.php">過去・未来検索</a></li>
                <li'; if($select == 7) {echo " class='active nav-item'";} echo '><a style="background-color: #e3f2fd;" class="nav-item" href="./ranking/">ランキング</a></li>
                <li>
                <form class="navbar-form" action="nicosearch.php" method="get">
                  <div class="form-group">
                    <input type="text" name="keyword" class="form-control" maxlength="30" placeholder="keyword">
                  </div>
                    <button type="submit" class="btn btn-primary">検索</button>
                </form>
                </li>
              </ul>
            </div>
          </div>
        </nav>';
}

function navbar_ranking($select){
  echo  '<div id="header" class="container" style="padding:20px"></div>
          <nav class="navbar navbar-default navbar-fixed-top">

            <div class="navbar-header">
              <button class="navbar-toggle" data-toggle="collapse" data-target=".target">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="../"><p>ニコニコ動画　検索info</p></a>
            </div>

        <div class="container">
            <div class="collapse navbar-collapse target">
              <ul class="nav navbar-nav small">
                <li'; if($select == 1) {echo " class='active nav-item'";} echo '><a class="nav-item" href="../nicokeywordtags-search.php">キーワード・タグ</a></li>
                <li'; if($select == 2) {echo " class='active nav-item'";} echo '><a class="nav-item" href="../nicoinfosearch.php">動画詳細</a></li>
                <li'; if($select == 3) {echo " class='active nav-item'";} echo '><a class="nav-item" href="../niconewvideo.php">新着動画</a></li>
                <li'; if($select == 4) {echo " class='active nav-item'";} echo '><a class="nav-item" href="../nicocomesearch.php">コメント</a></li>
                <li'; if($select == 5) {echo " class='active nav-item'";} echo '><a class="nav-item" href="../nicorand.php">ランダム</a></li>
                <li'; if($select == 6) {echo " class='active nav-item'";} echo '><a class="nav-item" href="../nicokmsearch.php">過去・未来検索</a></li>
                <li'; if($select == 7) {echo " class='active nav-item'";} echo '><a style="background-color: #e3f2fd;" class="nav-item" href="../ranking/">ランキング</a></li>
                <li>
                <form class="navbar-form" action="../nicosearch.php" method="get">
                  <div class="form-group">
                    <input type="text" name="keyword" class="form-control" maxlength="30" placeholder="keyword">
                  </div>
                    <button type="submit" class="btn btn-primary">検索</button>
                </form>
                </li>
              </ul>
            </div>
          </div>
        </nav>';
}
?>
