<?php
  $keyword = isset($_GET['keyword']) ? $_GET['keyword'] : null; //GETでキーワードを受け取る
  if($keyword == null){
    $title = "ニコニコ動画検索info タグ動画検索";
  }else{
    $title = $keyword." - ニコニコ動画検索info タグ動画検索";
  }
  $meta_url = "https://nicosearch.info/nicotagsearch.php?keyword=".$keyword;
?>
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
      <?php echo "<title>".$title."</title>"; ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <?php echo '<meta property="og:url" content="'.$meta_url.'" />'; ?>
        <meta property="og:type" content="website" />
        <?php echo '<meta property="og:title" content="'.$title.' - ニコニコ動画検索info タグ動画検索" />'; ?>
        <meta property="og:description" content="ニコニコ動画に投稿された動画のコメント,タグ,投稿者ランキング,新着動画,フォロー・フォロワー等の詳細な検索ができるサイトです。" />
        <meta property="og:site_name" content="ニコニコ動画検索info" />
        <meta property="og:image" content="https://nicosearch.info/image/site-title.png" />

        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content="@dhiki_pico" />
        <?php echo '<meta name="twitter:title" content="'.$title.' - ニコニコ動画検索info タグ動画検索" />'; ?>
        <?php echo '<meta name="twitter:url" content="'.$meta_url.'" />'; ?>
        <meta name="twitter:description" content="ニコニコ動画に投稿された動画のコメント,タグ,投稿者ランキング,新着動画,フォロー・フォロワー等の詳細な検索ができるサイトです。" />
        <meta name="twitter:image" content="https://nicosearch.info/image/site-title.png" />

          <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
          <link href="./css/style.css" rel="stylesheet" media="screen">
            <style type="text/css">
            @font-face {
                font-family: 'GenJyuuGothic';
                src: url('fonts/genjyuugothic-20150607/GenJyuuGothic-Medium.ttf');
            }
            body{
                font-family: 'GenJyuuGothic',sans-serif;
                /* table-layout: fixed; */
                /* text-align: center; */
            }
            p{
              font-size: 20px;
            }
            img {
              zoom: 150%;
            }
            button {
              -webkit-tap-highlight-color:rgba(0,0,0,0);
            }
            </style>
            <script type="application/ld+json">
            {
              "@context" : "http://schema.org",
              "@type" : "WebPage",
              "name" : "ニコニコ動画検索info",
            "url" : "https://nicosearch.info/",
            "sameAs" : [
              "https://twitter.com/dhiki_pico",
              "https://www.instagram.com/nicosearchinfo/?hl=ja"
              ]
            }
            </script>
            <script>
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

              ga('create', 'UA-85902327-1', 'auto');
              ga('send', 'pageview');

            </script>
              <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
              <script src="js/bootstrap.min.js"></script>


              <script src="js/include.js"></script>
  </head>
  <body ontouchstart="">
    <?php
    require('videoinfo_func.php');
    require('navbar/nicovideo_navbar.php');
    require('search_condition.php');
     ?>
<a name="up"></a>


    <?php $select = 1; navbar($select); ?>
    
    <ul itemscope itemtype="https://schema.org/BreadcrumbList" class="breadcrumbs">
      <!-- 1つめ -->
      <li itemprop="itemListElement" itemscope
          itemtype="https://schema.org/ListItem" class="breadcrumb_1">
        <a itemprop="item" href="https://nicosearch.info/">
            <span itemprop="name">ニコニコ動画検索info トップ</span>
        </a>
        <meta itemprop="position" content="1" />
      </li>

      <!-- 2つめ -->
      <li itemprop="itemListElement" itemscope
          itemtype="https://schema.org/ListItem" class="breadcrumb_2">
        <a itemprop="item" href="https://nicosearch.info/nicokeywordtags-search.php">
            <span itemprop="name">キーワード・タグ検索</span>
        </a>
        <meta itemprop="position" content="2" />
      </li>

      <!-- 3つめ -->
      <li itemprop="itemListElement" itemscope
          itemtype="https://schema.org/ListItem" class="breadcrumb_3">
        <?php echo '<a itemprop="item" href="https://nicosearch.info/nicotagsearch.php?keyword='.$keyword.'">'; ?>
        <?php echo "<span itemprop='name'>[".$keyword."]でタグ検索</span>"; ?>
        </a>
        <meta itemprop="position" content="3" />
      </li>
    </ul>
    <center>
      <div class="container" style="padding:20px">

          <h1><b>タグ検索</b></h1>
          <!-- <a href=./nicosearch_search.php><button type='button' class='btn btn-default' data-toggle='popover'>入力ページに戻る</button></a> -->
        <form class="navbar-form" action="nicotagsearch.php" method="get">
          <div class="form-group">
            <input type="text" name="keyword" class="form-control" maxlength="30" placeholder="tag" value="<?php if( !empty($_GET['keyword']) ){ echo $_GET['keyword']; } ?>">
          </div>
            <button type="submit" class="btn btn-primary">検索</button>
        </form>
<?php
        $field = isset($_GET['field']) ? $_GET['field'] : null; //GETでfieldを受け取る
        $sort = isset($_GET['sort']) ? $_GET['sort'] : null; //GETでsortを受け取る
        $offset = isset($_GET['offset']) ? $_GET['offset'] : null; //GETでoffsetを受け取る

        $offset = intval($offset); //小数点を整数で返す

        if($keyword == null){
          echo "<div class='container' style='padding:20px'>";
          echo "<p>タグを入力してください</p>";
          echo "</div>";
            die();
        }

        if($field != null && $field != "viewCounter" && $field != "commentCounter" && $field != "mylistCounter" && $field != "startTime"){
          echo "<div class='container' style='padding:20px'>";
          echo "<p>不正アクセスエラー</p>";
          echo "</div>";
            die();
        }

        if( $offset > 90){
          echo "<div class='container' style='padding:20px'>";
          echo "<p>offsetは90までです</p>";
          echo "</div>";
            die();
        }

        if( $offset < 0){
          echo "<div class='container' style='padding:20px'>";
          echo "<p>offsetエラー</p>";
          echo "</div>";
            die();
        }

        if($field == null){
          $field = "startTime";
        }
        if($sort == null){
          $sort = "-";
        }
        if($sort != null && $sort != "-"){
          $sort = "+";
        }
        if($offset == null){
          $offset = 0;
        }

        $keyword_encode = urlencode($keyword);

        $arr = video_tag_json($keyword_encode,$sort,$field,$offset);

        if($arr->data[0]->contentId == null){
          echo "<div class='container' style='padding:20px'>";

          echo "<p>動画が見つかりません</p>";
          echo "</div>";
            die();
        }

        search_tag_condition($keyword_encode,$field,$sort);
?>

<div class="container" style="padding:20px">

  <script type="text/javascript" >
     include("navbar/videoinfo.html");
  </script>

<?php
    for($i = 0; $i <= 9; $i++){

      if($arr->data[$i]->contentId == null){
          break;
      }

      $id[$i] = substr($arr->data[$i]->contentId, 2);
      $id_sort = substr($arr->data[$i]->contentId, 0, 2);

      video_data($id[$i],$id_sort);
    }
      $screen_video_num = $i;
?>
<?php
//pagination
require('pagination/pagination.php');
//paginationはスマホ版の時にレイアウト変更フラグつける
$ua = $_SERVER['HTTP_USER_AGENT'];
if((strpos($ua,'iPhone')!==false) || (strpos($ua,'iPod')!==false) || (strpos($ua,'Android.*Mobile')!==false) || (strpos($ua,'Windows.*Phone')!==false) || (strpos($ua,'Android')!==false)) {
  $sp = true;
}else{
  $sp = false;
}
search_pagination($offset, $keyword_encode, $field, $sort, $screen_video_num, $type="tag", $sp);
?>

<a href="#up" class="btn btn-primary btn-lg" role="button">一番上へ</a>
</center>
    </div>
</div>
<a name="down"></a>
    </body>
</html>
