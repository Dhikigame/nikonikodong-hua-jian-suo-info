<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
      <title>ニコニコ動画　検索info　更新履歴</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
            <style type="text/css">
            @font-face {
                font-family: 'GenJyuuGothic';
                src: url('fonts/genjyuugothic-20150607/GenJyuuGothic-Medium.ttf');
            }
            body{
                font-family: 'GenJyuuGothic',sans-serif;
                /* table-layout: fixed; */
                text-align: center;
            }
            p{
              font-size: 20px;
            }
            img {
              zoom: 150%;
            }
            button {
              -webkit-tap-highlight-color:rgba(0,0,0,0);
            }
            </style>
            <script>
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

              ga('create', 'UA-85902327-1', 'auto');
              ga('send', 'pageview');

            </script>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
 <script src="js/bootstrap.min.js"></script>


 <script src="js/include.js"></script>
 <body>
   <?php
   require('videoinfo_func.php');
   require('navbar/nicovideo_navbar.php');
    ?>

   <?php $select = 0; navbar($select); ?>
   <div class="container" style="padding:20px">
   <center>
     <h1>ニコニコ動画検索info</h1>
  <p>2021/4/04</p>
  コメントが出力できなくなった問題を修正<br>
  Yahooリアルタイム検索ページが開かなくなった問題を修正<br>
  <p>2020/07/29</p>
  7/27にユーザページ新しくなったことによりできなくなった<br>
  フォロー・フォロワー数の取得と投稿動画ページの改善<br> 
  <p>2020/04/22</p>
  動画情報タブに「ニコニ広告」追加<br> 
  <p>2020/02/08</p>
  投稿者ランキングページの追加(タブ上部)<br>
  レイアウトフォントの変更<br>
  <p>2019/03/10</p>
  コメント検索とキーワード・タグ検索時の入力値引き継ぎするよう改善<br>
  <p>2019/02/10</p>
  コメント検索にページング機能追加<br>
  <p>2019/01/30</p>
  動画検索におけるソート機能を修正<br>
  <p>2019/01/27</p>
  各種インターフェイスを修正<br>
　<p>2018/12/09</p>
  「コメント検索」にソート機能とページの一番上に戻るボタン追加<br>
  投稿順、再生時間をクリック・タップすればソートされます<br>
  また、広告ポイントが出力できなくなってしまったためご了承下さい<br><br>
  <p>2018/11/29</p>
  新着動画検索で新規カテゴリタグ追加(ASMR、MMD、 バーチャル、 鉄道、 TRPG)<br><br>
　<p>2018/05/06</p>
  投稿者情報にフォロワー数、フォロー数を追加<br><br>
  <p>2017/10/22</p>
  「動画詳細検索」を作り直し<br>
  動画の詳しい情報を知ることができます<br><br>
  <p>2017/4</p>
  サイトをリニューアル<br>

    </div>


    </body>

    <center>
    <div class='col-sm-12 col-xs-12' style='background:black;'></div>

    <script type="text/javascript" >
        include("copyright/copyright.html");
    </script>

    </center>

    </html>
