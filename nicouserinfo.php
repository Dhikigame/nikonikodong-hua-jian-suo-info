<?php
   require('./videoinfo_func.php');
   require('./navbar/nicovideo_navbar.php');
   $select = 2; navbar($select);
   require('./nicoinfo.php');
?>
<?php
   $userid = isset($_GET['UserId']) ? $_GET['UserId'] : null; //GETでIDを受け取る
  //  $video_id = video_IDsearch($id); //受け取ったIDの動画の種類を調べる(sm,so,nmを判断)
  //  $id_sort = $video_id;//sm,so,nmを保存
  //  $video_id = $video_id . $id;

  //  $base = 'https://ext.nicovideo.jp/api/getthumbinfo/'; //ニコニコAPI(動画情報取得)
  //  $base_id = 'https://ext.nicovideo.jp/api/getthumbinfo/' . $video_id; //ニコニコAPI(動画情報取得)
  //  $xml = simplexml_load_file($base_id); //ニコニコAPIのxml形式のデータを読み込む(動画情報取得)

  //  $meta_url = "https://nicosearch.info/nicoinforesult.php?nicoID=".$id;
?>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
      <?php echo "<title>".$userid." - ニコニコ動画検索info ユーザー情報</title>"; ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta property="og:type" content="website" />
        <?php echo '<meta property="og:title" content="'.$userid.' - ニコニコ動画検索info ユーザー情報" />'; ?>
        <meta property="og:description" content="ニコニコ動画に投稿された動画のコメント,タグ,投稿者ランキング,新着動画,フォロー・フォロワー等の詳細な検索ができるサイトです。" />
        <meta property="og:site_name" content="ニコニコ動画検索info" />
        <meta property="og:image" content="https://nicosearch.info/image/site-title.png" />

        <!-- <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content="@dhiki_pico" />
        <meta name="twitter:description" content="ニコニコ動画に投稿された動画のコメント,タグ,投稿者ランキング,新着動画,フォロー・フォロワー等の詳細な検索ができるサイトです。" />
        <meta name="twitter:image" content="https://nicosearch.info/image/site-title.png" /> -->

          <link href="./css/bootstrap.min.css" rel="stylesheet" media="screen">
          <link href="./css/style.css" rel="stylesheet" media="screen">
            <style type="text/css">
            @font-face {
                font-family: 'GenJyuuGothic';
                src: url('fonts/genjyuugothic-20150607/GenJyuuGothic-Medium.ttf');
            }
            body{
                font-family: 'GenJyuuGothic',sans-serif;
                /* table-layout: fixed; */
                /* text-align: center; */
            }
            p{
              font-size: 20px;
            }
            img {
              zoom: 150%;
            }
            button {
              -webkit-tap-highlight-color:rgba(0,0,0,0);
            }
            

            </style>
            <script type="application/ld+json">
            {
              "@context" : "http://schema.org",
              "@type" : "WebPage",
              "name" : "ニコニコ動画検索info",
            "url" : "https://nicosearch.info/",
            "sameAs" : [
              "https://twitter.com/dhiki_pico",
              "https://www.instagram.com/nicosearchinfo/?hl=ja"
              ]
            }
            </script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-85902327-1', 'auto');
  ga('send', 'pageview');

</script>
<script src = "http://code.jquery.com/jquery-1.11.3.min.js"></script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>

<script src="../js/include.js"></script>
</head>

<body>
  <a name="up"></a>

    <ul itemscope itemtype="https://schema.org/BreadcrumbList" class="breadcrumbs">
      <!-- 1つめ -->
      <li itemprop="itemListElement" itemscope
          itemtype="https://schema.org/ListItem" class="breadcrumb_1">
        <a itemprop="item" href="https://nicosearch.info/">
            <span itemprop="name">ニコニコ動画検索info トップ</span>
        </a>
        <meta itemprop="position" content="1" />
      </li>

      <!-- 2つめ -->
      <li itemprop="itemListElement" itemscope
          itemtype="https://schema.org/ListItem" class="breadcrumb_2">
        <a itemprop="item" href="https://nicosearch.info/nicoinfosearch.php">
            <span itemprop="name">動画詳細検索</span>
        </a>
        <meta itemprop="position" content="2" />
      </li>
      
    </ul>
<center>
<?php
  //  //エラー出力
  //  if( $xml->error->code == "DELETED"){
  //    echo "<div class='container' style='padding:20px'>";
  //    echo "<center><a href=../nicoinfo_search.php><button type='button' class='btn btn-default' data-toggle='popover'>
  //    入力ページに戻る</button></a></center>";
  //    echo "<p>入力値 ".$id."</p>";
  //    die ("動画が消去、または非公開です");
  //    echo "</div>";
  //   //   die();
  //  }
  //  if( $xml->error->code == "NOT_FOUND"){
  //    echo "<div class='container' style='padding:20px'>";
  //    echo "<center><a href=../nicoinfo_search.php><button type='button' class='btn btn-default' data-toggle='popover'>
  //    入力ページに戻る</button></a></center>";
  //    echo "<p>入力値 ".$id."</p>";
  //    die ("動画が消去、または存在しません");
  //    echo "</div>";
  //   //   die();
  //  }
  //  if( $id == null){
  //    echo "<div class='container' style='padding:20px'>";
  //    echo "<center><a href=../nicoinfo_search.php><button type='button' class='btn btn-default' data-toggle='popover'>
  //    入力ページに戻る</button></a></center>";
  //    echo "<p>入力値 ".$id."</p>";
  //    die ("IDが入力されてません");
  //    echo "</div>";
  //     // die();
  //  }
userdata_output($userid);
?>

  <a name="down"></a>
</div>
  </center>
 </body>
</html>
